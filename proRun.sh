#!/bin/bash

# RECUERDA QUE HAY QUE CORRER ESTO SIN sudo: ./proRun

UPDATE_COMPOSER_NODE_AND_BOWER=false
gulpPath=$DISISMY_ROOT/src/DisismyWebBundle/Resources/gulp

if [ -z ${DISISMY_ROOT+x} ]
then
echo ""
echo "*********************************************************************"
echo "* DISISMY_ROOT is UNSET... escribe                                  *"
echo '* export DISISMY_ROOT="path/to/disismy/root/in/your/computer"       *'
echo "* probablemente en                                                  *"
echo "* nano ~/.bash_profile                                              *"
echo "* o                                                                 *"
echo "* nano ~/.profile                                                   *"
echo "* necesita reiniciar el terminal                                    *"
echo "* Recuerda que tienes que correr el proRun sin sudo,                *"
echo "* si no no funciona                                                 *"
echo "*********************************************************************"
echo ""
echo ""
exit 1
fi

command -v composer >/dev/null 2>&1 ||
{
	echo ""
	echo "*********************************************************************"
	echo "* composer no está instalado globalmente"
	echo '* 1. Instálalo siguiendo estas pautas:'
	echo "* https://getcomposer.org/download/"
	echo "* 2. Haz que sea global corriendo este script:"
	echo "* mv composer.phar /usr/local/bin/composer"
	echo "*********************************************************************"
	echo ""
	echo ""
	exit 1
}

command -v gulp >/dev/null 2>&1 ||
{
	echo ""
	echo "*********************************************************************"
	echo "* gulp no está instalado globalmente"
	echo '* 1. Instálalo corriendo este script:'
	echo "* npm install -g gulp"
	echo "*********************************************************************"
	echo ""
	echo ""
	exit 1
}

if [ -z ${NODE_ENV+x} ]
then
echo ""
echo "*********************************************************************"
echo "* NODE_ENV is UNSET... escribe                                      *"
echo '* export NODE_ENV="development|production"				          *'
echo "* probablemente en                                                  *"
echo "* nano ~/.bash_profile                                              *"
echo "* o                                                                 *"
echo "* nano ~/.profile                                                   *"
echo "* necesita reiniciar el terminal                                    *"
echo "*********************************************************************"
echo ""
echo ""
exit 1
fi


echo ""
echo "*********************************************************************"
if [ $NODE_ENV = "development" ]
	then
	echo " development"
else
	if [ $NODE_ENV = "production" ]
		then
		echo " production"
	fi
fi
echo "*********************************************************************"
echo ""
echo ""




cd $DISISMY_ROOT

if [ $NODE_ENV = "production" ]
	then
	echo ""
	echo "*********************************************************************"
	echo "* Como estamos en production, hacemos un pull de git                *"
	echo "*********************************************************************"
    # pull no repone los ficheros borrados en production
    git pull origin master
    git fetch --all
    git reset --hard origin/master
	echo ""
	echo ""
fi

sudo chmod -R 777 $DISISMY_ROOT/var/cache/ && sudo chmod -R 777 $DISISMY_ROOT/var/logs/
sudo php $DISISMY_ROOT/bin/console cache:clear
sudo php $DISISMY_ROOT/bin/console cache:clear -e prod
sudo chmod -R 777 $DISISMY_ROOT/var/cache/ && sudo chmod -R 777 $DISISMY_ROOT/var/logs/

if [ $UPDATE_COMPOSER_NODE_AND_BOWER = true ]
then
    echo ""
    echo "*********************************************************************"
    echo "* Updateamos composer, node y bower!!!                              *"
    echo "*********************************************************************"
    if [ $NODE_ENV = "development" ]
    then
		# TODO: cuando quité el sudo composer self-updte y sudo composer update
		# ya teniamos la ultima version y por eso no daba pete.
		# quizás cuando sí que vuelva a necesitar falle por tema de permisos
		# en alguna carpeta.
        composer self-update
        composer update
        cd $DISISMY_ROOT/app/Resources && npm update && cd $DISISMY_ROOT/
        cd $DISISMY_ROOT/src/DisismyWebBundle/Resources/public/libs/npm && npm update && cd $DISISMY_ROOT/
        cd $DISISMY_ROOT/src/DisismyWebBundle/Resources/public/libs/bower && bower update && cd $DISISMY_ROOT/
        cd $gulpPath && npm update && cd $DISISMY_ROOT/
    fi
    if [ $NODE_ENV = "production" ]
    then
        composer self-update
        composer install
        cd $DISISMY_ROOT/app/Resources && npm install && cd $DISISMY_ROOT/
        cd $DISISMY_ROOT/src/DisismyWebBundle/Resources/public/libs/npm && npm install && cd $DISISMY_ROOT/
        cd $DISISMY_ROOT/src/DisismyWebBundle/Resources/public/libs/bower && bower install --allow-root && cd $DISISMY_ROOT/
        cd $gulpPath && npm install && cd $DISISMY_ROOT/
    fi
fi

# borramos todos los assets
cd $DISISMY_ROOT/web/ && sudo rm -rf css && sudo rm -rf js && sudo rm -rf images && cd $DISISMY_ROOT/
sudo chmod -R 777 $DISISMY_ROOT/var/cache/ && sudo chmod -R 777 $DISISMY_ROOT/var/logs/
# los añadimos
sudo php bin/console assetic:dump
# sudo php bin/console assetic:dump --env=prod --no-debug


# give files read privileges recursively
sudo find $DISISMY_ROOT/web/bundles/disismyweb/images/Users/ -type f -exec chmod 777 {} +
# give directories read & execute privileges recursively
sudo find $DISISMY_ROOT/web/bundles/disismyweb/images/Users -type d -exec chmod 777 {} +





# requirejs optimizer:
rPath=$DISISMY_ROOT/src/DisismyWebBundle/Resources/public/libs/npm/node_modules/requirejs/bin/
appBuildPath=$DISISMY_ROOT/src/DisismyWebBundle/Resources/config/
echo ""
echo "*********************************************************************"
echo "rPath: $rPath"
echo "appBuildPath: $appBuildPath"
echo "*********************************************************************"
echo ""
echo ""
node $rPath/r.js -o $appBuildPath/app.build.js


echo ""
echo "*********************************************************************"
echo "* gulp for $NODE_ENV environment                                     "
echo "*********************************************************************"
cd $gulpPath && gulp --$NODE_ENV && cd $DISISMY_ROOT/
