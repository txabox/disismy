-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 21, 2016 at 08:58 AM
-- Server version: 5.6.25
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `disismy`
--

-- --------------------------------------------------------

--
-- Table structure for table `buttons`
--

CREATE TABLE IF NOT EXISTS `buttons` (
  `id` int(11) NOT NULL,
  `panoNode_owner` int(11) DEFAULT NULL,
  `target_panoNode` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pairedButton_id` int(11) DEFAULT NULL,
  `fooName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lon` double NOT NULL,
  `lat` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `buttons`
--

INSERT INTO `buttons` (`id`, `panoNode_owner`, `target_panoNode`, `created_at`, `updated_at`, `pairedButton_id`, `fooName`, `lon`, `lat`) VALUES
(1, 1, 3, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 4, 'Salón -> Cocina', 216.7, -5),
(2, 2, 1, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 3, 'Hall -> Salón', 0, -14),
(3, 1, 2, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 2, 'Salón -> Hall', 157, -5),
(4, 3, 1, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 1, 'Cocina -> Salón', 69.4, -14),
(5, 2, 3, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 6, 'Hall -> Cocina', -68.2, -14),
(6, 3, 2, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 5, 'Cocina -> Hall', 175.8, -12),
(7, 2, 4, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 8, 'Hall -> Hab. Ppal', 256.2, -16),
(8, 4, 2, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 7, 'Hab. Ppal -> Hall', 325.8, -17),
(9, 2, 5, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 10, 'Hall -> Descansillo', 75.5, -21.7),
(10, 5, 2, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 9, 'Descansillo -> Hall', -18.3, -18.3),
(11, 5, 6, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 12, 'Descansillo -> Hab 2', 223.4, -11.5),
(12, 6, 5, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 11, 'Hab 2 -> Descansillo', -95.2, -11),
(13, 4, 7, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 14, 'Hab Ppal -> Baño Ppal', 343.7, -13.8),
(14, 7, 4, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 13, 'Baño Ppal -> Hab Ppal', 15.5, -22.7),
(15, 8, 5, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 16, 'Baño 2 -> Descansillo', 1.9, -17.8),
(16, 5, 8, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 15, 'Descansillo -> Baño 2', 145, -16.7),
(17, 9, 10, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 19, 'hall - baño', 345, -11),
(18, 9, 11, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 20, 'hall - salón', 145, -8.8),
(19, 10, 9, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 17, 'baño - hall', 299, -19),
(20, 11, 9, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 18, 'salon - hall', 311, -3.6),
(21, 11, 12, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 22, 'salón - pasillo', 122, -14),
(22, 12, 11, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 21, 'pasillo - salón', 204, -18),
(23, 12, 13, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 24, 'pasillo - despacho', 108, -15),
(24, 13, 12, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 23, 'despacho - pasillo', 169, -12),
(25, 12, 14, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 26, 'pasillo - hab ppal', 30, -15),
(26, 14, 12, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 25, 'hab ppal - pasillo', 428, -15),
(27, 15, 17, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 28, 'salón renderizado -> hall renderizado', 75, -20),
(28, 17, 15, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 27, 'Hall renderizado -> salon renderizado', 21, -22),
(29, 17, 16, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 30, 'Hall renderizado -> Cocina renderizada', 314, -17),
(30, 16, 17, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 29, 'Cocina renderizada -> Hall renderizado', 364, -15),
(31, 17, 18, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 32, 'Hall renderizado -> Hab ppal renderizada', 280, -18),
(32, 18, 17, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 31, 'Hab ppal renderizada -> Hall renderizado', -9, -22),
(33, 18, 19, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 34, 'Hab ppal renderizada -> baño ppal renderizado', 30, -20),
(34, 19, 18, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 33, 'Baño ppal renderizado -> hab ppal renderizada', 372, -21),
(35, 17, 20, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 36, 'Hall renderizado -> Pasillo renderizado', 99, -21),
(36, 20, 17, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 35, 'Pasillo renderizado -> Hall renderizado', 17, -19),
(37, 20, 21, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 38, 'Pasillo renderizado -> Hab2 renderizada', -97, -18),
(38, 21, 20, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 37, 'Hab2 renderizada -> Pasillo renderizado', 86, -15),
(39, 20, 22, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 40, 'Pasillo renderizado -> Baño2 renderizado', 185, -16),
(40, 22, 20, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 39, 'Baño2 renderizado -> Pasillo renderizado', 364, -20);

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE IF NOT EXISTS `folders` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`id`, `owner_id`, `parent_id`, `created_at`, `updated_at`, `name`) VALUES
(1, 1, NULL, '2016-04-16 00:00:00', '2016-04-16 00:00:00', 'root'),
(3, 1, 1, '2016-04-16 00:00:00', '2016-04-16 00:00:00', 'nivel1a'),
(4, 1, 1, '2016-04-16 00:00:00', '2016-04-16 00:00:00', 'nivel1b'),
(5, 1, 3, '2016-04-16 00:00:00', '2016-04-16 00:00:00', 'nivel1a_2a'),
(6, 1, 4, '2016-04-16 00:00:00', '2016-04-16 00:00:00', 'nivel1b_2a'),
(11, 16, NULL, '2016-04-20 19:05:00', '2016-04-20 19:05:00', 'root'),
(12, 17, NULL, '2016-04-20 19:15:26', '2016-04-20 19:15:26', 'root'),
(13, 18, NULL, '2016-04-20 19:24:37', '2016-04-20 19:24:37', 'root'),
(14, 19, NULL, '2016-04-20 19:28:36', '2016-04-20 19:28:36', 'root');

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE IF NOT EXISTS `houses` (
  `id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `util_sq_meters` int(11) NOT NULL,
  `built_sq_meters` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`id`, `object_id`, `created_at`, `updated_at`, `util_sq_meters`, `built_sq_meters`) VALUES
(1, 1, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 65, 65),
(2, 2, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 70, 70);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `extension` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `owner` int(11) DEFAULT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_en` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_es` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `created_at`, `updated_at`, `extension`, `owner`, `description_en`, `description_es`, `alt_en`, `alt_es`, `image_name`, `folder_id`) VALUES
(4, '2016-01-06 00:00:00', '2016-01-06 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto1', 1),
(5, '2016-01-06 00:00:00', '2016-01-06 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto2', 1),
(6, '2016-01-06 00:00:00', '2016-01-06 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto3', 1),
(7, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto4', 1),
(8, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto5', 1),
(9, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto6', 1),
(10, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto7', 1),
(11, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto8', 1),
(12, '2016-01-31 00:00:00', '2016-01-31 00:00:00', 'png', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto9', 1),
(13, '2016-01-31 00:00:00', '2016-01-31 00:00:00', 'png', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto10', 1),
(14, '2016-02-11 00:00:00', '2016-02-11 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto11', 1),
(15, '2016-02-14 00:00:00', '2016-02-14 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto12', 1),
(16, '2016-02-16 00:00:00', '2016-02-16 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto13', 1),
(17, '2016-02-16 00:00:00', '2016-02-16 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto14', 1),
(18, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto15', 1),
(19, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto16', 1),
(20, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto17', 1),
(21, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto18', 1),
(22, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto19', 1),
(23, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto20', 1),
(24, '2016-02-29 00:00:00', '2016-04-25 16:18:37', 'jpg', 1, NULL, NULL, NULL, NULL, 'foto21', 1),
(25, '2016-03-03 00:00:00', '2016-03-03 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto22', 1),
(26, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto23', 1),
(27, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto24', 1),
(28, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto25', 1),
(29, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto26', 1),
(30, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto27', 1),
(31, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto28', 1),
(32, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto29', 1),
(33, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto30', 1),
(34, '2016-03-19 00:00:00', '2016-03-19 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto31', 1),
(35, '2016-03-19 00:00:00', '2016-03-19 00:00:00', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto32', 1),
(55, '2016-04-04 14:55:57', '2016-04-04 14:55:57', 'jpg', 1, 'description_en', 'description_es', 'alt_en', 'alt_es', 'foto33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` int(11) NOT NULL,
  `name` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail_id` int(11) DEFAULT NULL,
  `locale` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `thumbnail_id`, `locale`) VALUES
(1, 'english', 35, 'en'),
(2, 'spanish', 34, 'es');

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_on_sale` tinyint(1) NOT NULL,
  `is_on_rent` tinyint(1) NOT NULL,
  `name_en` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `on_sale_price` decimal(10,2) NOT NULL,
  `on_rent_price` decimal(10,2) NOT NULL,
  `renting_period_units` smallint(6) NOT NULL,
  `thumbnail_id` int(11) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `renting_period` int(11) NOT NULL,
  `owner` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `objects`
--

INSERT INTO `objects` (`id`, `created_at`, `updated_at`, `is_on_sale`, `is_on_rent`, `name_en`, `name_es`, `description_en`, `description_es`, `on_sale_price`, `on_rent_price`, `renting_period_units`, `thumbnail_id`, `is_public`, `type_id`, `renting_period`, `owner`) VALUES
(1, '2016-01-05 00:00:00', '2016-06-28 20:16:25', 1, 1, 'Show house', 'Casa de Muestra cambiada', 'This is a show house of about 80 m2 in the center of Bilbao. It consists of lounge, 2 bedrooms , 2 bathrooms, kitchen and Hall. Central heating. Very bright.', 'Esta es una casa de muestra de unos 80 m2 en el centro de Bilbao. Consta de Salón, 2 habitaciónes, 2 baños, cocina y Hall. Calefacción central. Muy luminosa.', '12.00', '12.00', 1, 17, 0, 1, 4, 1),
(2, '2016-02-15 00:00:00', '2016-06-20 08:30:45', 1, 1, 'Vivienda en Alda. Urquijo (Bilbao)', 'Vivienda en Alda. Urquijo (Bilbao)', 'Piso en venta en Alda. Urquijo, de Bilbao. Muy soleado.', 'Piso en venta en Alda. Urquijo, de Bilbao. Muy soleado.', '295000.00', '0.00', 0, 24, 1, 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `objects_languages`
--

CREATE TABLE IF NOT EXISTS `objects_languages` (
  `object_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `objects_languages`
--

INSERT INTO `objects_languages` (`object_id`, `language_id`) VALUES
(1, 2),
(2, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `object_types`
--

CREATE TABLE IF NOT EXISTS `object_types` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `object_types`
--

INSERT INTO `object_types` (`id`, `name`) VALUES
(1, 'house'),
(2, 'hotel');

-- --------------------------------------------------------

--
-- Table structure for table `object_viewers`
--

CREATE TABLE IF NOT EXISTS `object_viewers` (
  `user_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `object_viewers`
--

INSERT INTO `object_viewers` (`user_id`, `object_id`) VALUES
(16, 1),
(18, 1),
(19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `panoNodes`
--

CREATE TABLE IF NOT EXISTS `panoNodes` (
  `id` int(11) NOT NULL,
  `pano_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `name_en` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_es` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `panoNodes`
--

INSERT INTO `panoNodes` (`id`, `pano_id`, `room_id`, `created_at`, `updated_at`, `image_id`, `name_en`, `name_es`, `description_en`, `description_es`) VALUES
(1, 1, 1, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 4, 'Living room', 'Salón', NULL, NULL),
(2, 1, 3, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 5, 'Hall', 'Hall', NULL, NULL),
(3, 1, 2, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 6, 'Kitchen', 'Cocina', NULL, NULL),
(4, 1, 4, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 7, 'Main room', 'Habitación principal', NULL, NULL),
(5, 1, 5, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 8, 'Corridor', 'Pasillo', NULL, NULL),
(6, 1, 6, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 9, 'Room 2', 'Habitación 2', NULL, NULL),
(7, 1, 7, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 11, 'Main Bathroom', 'Baño principal', NULL, NULL),
(8, 1, 8, '2016-01-24 00:00:00', '2016-01-24 00:00:00', 10, 'Bathroom 2', 'Baño 2', NULL, NULL),
(9, 2, 9, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 18, 'Hall-Kitchen', 'Hall-Cocina', NULL, NULL),
(10, 2, 10, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 19, 'Bathroom', 'Baño', NULL, NULL),
(11, 2, 11, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 20, 'Living room', 'Salón', NULL, NULL),
(12, 2, 12, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 21, 'Corridor', 'Pasillo', NULL, NULL),
(13, 2, 13, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 22, 'Office', 'Despacho', NULL, NULL),
(14, 2, 14, '2016-02-29 00:00:00', '2016-02-29 00:00:00', 23, 'Main room', 'Habitación principal', NULL, NULL),
(15, 3, 1, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 25, 'Living room', 'Salón', 'Rendered', 'Renderizado'),
(16, 3, 2, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 26, 'Kitchen', 'Cocina', 'Rendered', 'Renderizado'),
(17, 3, 3, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 27, 'Hall', 'Hall', 'Rendered', 'Renderizado'),
(18, 3, 4, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 28, 'Main room', 'Habitación principal', 'Rendered', 'Renderizado'),
(19, 3, 10, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 29, 'Main bathroom', 'Baño principal', 'Rendered', 'Renderizado'),
(20, 3, 5, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 30, 'Corridor', 'Pasillo', 'Rendered', 'Renderizado'),
(21, 3, 6, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 31, 'Room 2', 'Habitación 2', 'Rendered', 'Renderizado'),
(22, 3, 8, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 32, 'Bathroom 2', 'Baño 2', 'Rendered', 'Renderizado');

-- --------------------------------------------------------

--
-- Table structure for table `panos`
--

CREATE TABLE IF NOT EXISTS `panos` (
  `id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `initial_panoNode` int(11) DEFAULT NULL,
  `initialPanoNodeLon` double NOT NULL,
  `initialPanoNodeLat` double NOT NULL,
  `name_en` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `panos`
--

INSERT INTO `panos` (`id`, `object_id`, `created_at`, `updated_at`, `initial_panoNode`, `initialPanoNodeLon`, `initialPanoNodeLat`, `name_en`, `name_es`, `description_en`, `description_es`, `thumbnail_id`) VALUES
(1, 1, '2016-01-06 00:00:00', '2016-01-06 00:00:00', 1, 136, 0, '''Pano'' example', 'Ejemplo de ''Pano''', '''Pano'' example (Interactive and Virtual 360º Panoramic Tour ) for a typical house in the center of Bilbao. If you want yours, do not hesitate to contact us! ;)', 'Un "Pano" no es más que un Tour Virtual Panorámico Fotográfico e Interactivo de 360º.<br />Si quieres el tuyo personalizado, ¡No dudes en ponerte en contacto con nosotros! ;)', NULL),
(2, 2, '2016-01-07 00:00:00', '2016-01-07 00:00:00', 9, 136, 0, 'Vivienda en Alda. Urquijo (Bilbao)', 'Vivienda en Alda. Urquijo (Bilbao)', 'Piso en venta en Alda. Urquijo, de Bilbao. Muy soleado.', 'Piso en venta en Alda. Urquijo, de Bilbao. Muy soleado.', NULL),
(3, 1, '2016-03-04 00:00:00', '2016-03-04 00:00:00', 15, 180, -3.07, '''Render Pano'' example', 'Ejemplo de ''Render Pano''', '''Render Pano'' example (Interactive and Virtual 360º Panoramic Tour ) for a typical house in the center of Bilbao. If you want yours, do not hesitate to contact us! ;)', 'Un ''Render Pano'' no es más que un Tour Virtual Panorámico  e Interactivo de 360º renderizado en 3D.<br />Si quieres el tuyo personalizado, ¡No dudes en ponerte en contacto con nosotros! ;)', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name_en` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_id` int(11) DEFAULT NULL,
  `util_sq_meters` decimal(10,2) NOT NULL,
  `built_sq_meters` decimal(10,2) NOT NULL,
  `name_es` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `created_at`, `updated_at`, `name_en`, `house_id`, `util_sq_meters`, `built_sq_meters`, `name_es`, `description_en`, `description_es`) VALUES
(1, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 'Living room', 1, '25.00', '25.00', 'Salón', 'Lorem ipsum in Enlgish', 'Lorem ipsum en Español'),
(2, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 'Kitchen', 1, '9.00', '9.00', 'Cocina', '', ''),
(3, '2016-01-05 00:00:00', '2016-01-05 00:00:00', 'Hall', 1, '7.00', '7.00', 'Hall', '', ''),
(4, '2016-01-23 00:00:00', '2016-01-23 00:00:00', 'Main room', 1, '14.00', '14.00', 'Habitación Principal', '', ''),
(5, '2016-02-16 00:00:00', '2016-02-16 00:00:00', 'Landing', 1, '2.00', '2.00', 'Pasillo', '', ''),
(6, '2016-02-16 00:00:00', '2016-02-16 00:00:00', 'Room #2', 1, '10.00', '10.00', 'Habitación 2', '', ''),
(7, '2016-02-16 00:00:00', '2016-02-16 00:00:00', 'Main bathroom', 1, '5.00', '5.00', 'Baño Principal', '', ''),
(8, '2016-02-16 00:00:00', '2016-02-16 00:00:00', 'Bathroom #2', 1, '4.00', '4.00', 'Baño 2', '', ''),
(9, '2016-02-29 00:00:00', '2016-03-13 16:21:14', 'Hall-Kitchen', 2, '0.00', '0.00', 'Hall-Cocina', NULL, NULL),
(10, '2016-02-29 00:00:00', '2016-03-13 16:21:14', 'Bathroom', 2, '0.00', '0.00', 'Baño', NULL, NULL),
(11, '2016-02-29 00:00:00', '2016-03-13 16:21:14', 'Living room', 2, '0.00', '0.00', 'Salón', NULL, NULL),
(12, '2016-02-29 00:00:00', '2016-03-13 16:21:14', 'Corridor', 2, '0.00', '0.00', 'Pasillo', NULL, NULL),
(13, '2016-02-29 00:00:00', '2016-03-13 16:21:14', 'Office', 2, '0.00', '0.00', 'Despacho', NULL, NULL),
(14, '2016-02-29 00:00:00', '2016-03-13 16:21:14', 'Main room', 2, '0.00', '0.00', 'Habitación principal', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_images`
--

CREATE TABLE IF NOT EXISTS `room_images` (
  `image_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `storage_plans`
--

CREATE TABLE IF NOT EXISTS `storage_plans` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name_en` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  `bytes` bigint(20) NOT NULL,
  `annual_price` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `storage_plans`
--

INSERT INTO `storage_plans` (`id`, `created_at`, `updated_at`, `name_en`, `name_es`, `description_en`, `description_es`, `is_public`, `bytes`, `annual_price`) VALUES
(1, '2016-04-02 00:00:00', '2016-04-02 00:00:00', 'BASIC', 'BASIC', 'Basic Storage Plan subscription', 'Suscripción básico Plan de Almacenamiento', 1, 209715200, '50.00'),
(2, '2016-04-03 00:00:00', '2016-04-03 00:00:00', 'PRO 5', 'PRO 5', '', '', 1, 524288000, '180.00'),
(3, '2016-04-03 00:00:00', '2016-04-03 00:00:00', 'PRO 10', 'PRO 10', '', '', 1, 2097152000, '360.00'),
(4, '2016-04-03 00:00:00', '2016-04-03 00:00:00', 'PRO 20', 'PRO 20', '', '', 1, 10485760000, '720.00');

-- --------------------------------------------------------

--
-- Table structure for table `time_periods`
--

CREATE TABLE IF NOT EXISTS `time_periods` (
  `id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `time_periods`
--

INSERT INTO `time_periods` (`id`, `name`) VALUES
(2, 'day(s)'),
(1, 'hour(s)'),
(4, 'month(s)'),
(3, 'week(s)'),
(5, 'year(s)');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `storage_plan` int(11) DEFAULT NULL,
  `consumedBytes` int(11) NOT NULL,
  `root_folder_id` int(11) DEFAULT NULL,
  `encodedId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `description_en`, `description_es`, `is_public`, `storage_plan`, `consumedBytes`, `root_folder_id`, `encodedId`) VALUES
(1, 'disismy', 'disismy', 'lauteilatu@hotmail.com', 'lauteilatu@hotmail.com', 1, '68qyh57po34sgwc80g4cko8ogw8soks', '$2y$13$dEpfYz0HvFW2SaShRhQfk.W6bwwOyfk4K/HKiUVgdr8SW0jizid.S', '2016-06-29 10:44:39', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2016-02-15 18:42:46', '2016-07-06 09:10:13', '', '', 1, 1, 2690230, 1, 1126440043),
(16, 'amparo2', 'amparo2', 'ima13524@laoeq.com', 'ima13524@laoeq.com', 1, 'obuv0qjk56sgsgksgwswo0w48sgcwg4', '$2y$13$c6QN/dY3Sy1ZcvnAyFJ5LO5kRAgiC./ZgBbRBvuuTPn0T2Mxos1Ze', '2016-04-20 19:06:01', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2016-04-20 19:05:00', '2016-04-20 19:06:44', NULL, NULL, 1, 1, 26624, 11, 0),
(17, 'amparo', 'amparo', 'sip06291@laoeq.com', 'sip06291@laoeq.com', 0, 'n1cv2ul7jisss0k0owcgck04000c0cc', '$2y$13$B2sbKfhiqTP/uH8FHe7vIu6nXjI7ECYuGy.GLC/x0yAIq67MQbXmW', NULL, 0, 0, NULL, 'wSWRx49z73uabHhkNyNBIQ5csMSSgla8oZGjIvM0FA4', NULL, 'a:0:{}', 0, NULL, '2016-04-20 19:15:26', '2016-04-20 19:15:26', NULL, NULL, 1, 1, 0, 12, 0),
(18, 'amparo3', 'amparo3', 'hti70322@laoeq.com', 'hti70322@laoeq.com', 0, '1vbiptcq9uboo84ss80osc84gosocw8', '$2y$13$qzCm6BIXj/khE6YLRz6qeugqtJ4fMQzQ7O3Kx6DbGUBMJ9Iq4j1mi', NULL, 0, 0, NULL, 'm9trqbC1gvIHIq3V0dDtXBF0NeUWjHTshZ8qQkWDGG0', NULL, 'a:0:{}', 0, NULL, '2016-04-20 19:24:37', '2016-04-20 19:24:37', NULL, NULL, 1, 1, 0, 13, 1173573452),
(19, 'amparo_ribelles', 'amparo_ribelles', 'wia67891@laoeq.com', 'wia67891@laoeq.com', 1, 'dz0cc81utqo8w4ocwcsossws80ssc04', '$2y$13$Q/5CbRwVS5OaiN0p2f38D.MfambL9Tq2hY9DQQVbxqqe0xYziU8mq', '2016-04-20 19:29:25', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2016-04-20 19:28:36', '2016-04-20 19:29:49', NULL, NULL, 1, 1, 3337, 14, 779621777);

-- --------------------------------------------------------

--
-- Table structure for table `user_objects`
--

CREATE TABLE IF NOT EXISTS `user_objects` (
  `user_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_objects`
--

INSERT INTO `user_objects` (`user_id`, `object_id`) VALUES
(1, 1),
(1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buttons`
--
ALTER TABLE `buttons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_435C85B6BD1E95AD` (`pairedButton_id`),
  ADD KEY `IDX_435C85B690E20B97` (`panoNode_owner`),
  ADD KEY `IDX_435C85B6138DCF10` (`target_panoNode`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FE37D30F727ACA70` (`parent_id`),
  ADD KEY `IDX_FE37D30F7E3C61F9` (`owner_id`);

--
-- Indexes for table `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_95D7F5CB232D562B` (`object_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E01FBE6ACF60E67C` (`owner`),
  ADD KEY `IDX_E01FBE6A162CB942` (`folder_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_D4DB71B55E237E06` (`name`),
  ADD UNIQUE KEY `UNIQ_D4DB71B54180C698` (`locale`),
  ADD KEY `IDX_D4DB71B5FDFF2E92` (`thumbnail_id`);

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B21ACCF3FDFF2E92` (`thumbnail_id`),
  ADD KEY `IDX_B21ACCF3C54C8C93` (`type_id`),
  ADD KEY `IDX_B21ACCF384640318` (`renting_period`),
  ADD KEY `IDX_B21ACCF3CF60E67C` (`owner`);

--
-- Indexes for table `objects_languages`
--
ALTER TABLE `objects_languages`
  ADD PRIMARY KEY (`object_id`,`language_id`),
  ADD KEY `IDX_125446C8232D562B` (`object_id`),
  ADD KEY `IDX_125446C882F1BAF4` (`language_id`);

--
-- Indexes for table `object_types`
--
ALTER TABLE `object_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `object_viewers`
--
ALTER TABLE `object_viewers`
  ADD PRIMARY KEY (`object_id`,`user_id`),
  ADD KEY `IDX_42B67CA76ED395` (`user_id`),
  ADD KEY `IDX_42B67C232D562B` (`object_id`);

--
-- Indexes for table `panoNodes`
--
ALTER TABLE `panoNodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_321E208C3DA5256D` (`image_id`),
  ADD KEY `IDX_321E208C6EEBE1F1` (`pano_id`),
  ADD KEY `IDX_321E208C54177093` (`room_id`);

--
-- Indexes for table `panos`
--
ALTER TABLE `panos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_D54A3670119F46C9` (`initial_panoNode`),
  ADD KEY `IDX_D54A3670232D562B` (`object_id`),
  ADD KEY `IDX_D54A3670FDFF2E92` (`thumbnail_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7CA11A966BB74515` (`house_id`);

--
-- Indexes for table `room_images`
--
ALTER TABLE `room_images`
  ADD PRIMARY KEY (`room_id`,`image_id`),
  ADD UNIQUE KEY `UNIQ_A15178AB3DA5256D` (`image_id`),
  ADD KEY `IDX_A15178AB54177093` (`room_id`);

--
-- Indexes for table `storage_plans`
--
ALTER TABLE `storage_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_periods`
--
ALTER TABLE `time_periods`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_339039C45E237E06` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_1483A5E9A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_1483A5E95F3EA365` (`root_folder_id`),
  ADD KEY `IDX_1483A5E9D4779068` (`storage_plan`);

--
-- Indexes for table `user_objects`
--
ALTER TABLE `user_objects`
  ADD PRIMARY KEY (`user_id`,`object_id`),
  ADD KEY `IDX_EA17D2F9A76ED395` (`user_id`),
  ADD KEY `IDX_EA17D2F9232D562B` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buttons`
--
ALTER TABLE `buttons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `houses`
--
ALTER TABLE `houses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `objects`
--
ALTER TABLE `objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `object_types`
--
ALTER TABLE `object_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `panoNodes`
--
ALTER TABLE `panoNodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `panos`
--
ALTER TABLE `panos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `storage_plans`
--
ALTER TABLE `storage_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `time_periods`
--
ALTER TABLE `time_periods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `buttons`
--
ALTER TABLE `buttons`
  ADD CONSTRAINT `FK_435C85B671E896E1` FOREIGN KEY (`target_panoNode`) REFERENCES `panoNodes` (`id`),
  ADD CONSTRAINT `FK_435C85B690E20B97` FOREIGN KEY (`panoNode_owner`) REFERENCES `panoNodes` (`id`),
  ADD CONSTRAINT `FK_435C85B6BD1E95AD` FOREIGN KEY (`pairedButton_id`) REFERENCES `buttons` (`id`);

--
-- Constraints for table `folders`
--
ALTER TABLE `folders`
  ADD CONSTRAINT `FK_FE37D30F727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `folders` (`id`),
  ADD CONSTRAINT `FK_FE37D30F7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `houses`
--
ALTER TABLE `houses`
  ADD CONSTRAINT `FK_95D7F5CB232D562B` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`);

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK_E01FBE6A162CB942` FOREIGN KEY (`folder_id`) REFERENCES `folders` (`id`),
  ADD CONSTRAINT `FK_E01FBE6ACF60E67C` FOREIGN KEY (`owner`) REFERENCES `users` (`id`);

--
-- Constraints for table `language`
--
ALTER TABLE `language`
  ADD CONSTRAINT `FK_D4DB71B5FDFF2E92` FOREIGN KEY (`thumbnail_id`) REFERENCES `images` (`id`);

--
-- Constraints for table `objects`
--
ALTER TABLE `objects`
  ADD CONSTRAINT `FK_B21ACCF384640318` FOREIGN KEY (`renting_period`) REFERENCES `time_periods` (`id`),
  ADD CONSTRAINT `FK_B21ACCF3C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `object_types` (`id`),
  ADD CONSTRAINT `FK_B21ACCF3CF60E67C` FOREIGN KEY (`owner`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_B21ACCF3FDFF2E92` FOREIGN KEY (`thumbnail_id`) REFERENCES `images` (`id`);

--
-- Constraints for table `objects_languages`
--
ALTER TABLE `objects_languages`
  ADD CONSTRAINT `FK_125446C8232D562B` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_125446C882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `object_viewers`
--
ALTER TABLE `object_viewers`
  ADD CONSTRAINT `FK_42B67C232D562B` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_42B67CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `panoNodes`
--
ALTER TABLE `panoNodes`
  ADD CONSTRAINT `FK_321E208C3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`),
  ADD CONSTRAINT `FK_321E208C54177093` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `FK_321E208C6EEBE1F1` FOREIGN KEY (`pano_id`) REFERENCES `panos` (`id`);

--
-- Constraints for table `panos`
--
ALTER TABLE `panos`
  ADD CONSTRAINT `FK_D54A3670119F46C9` FOREIGN KEY (`initial_panoNode`) REFERENCES `panoNodes` (`id`),
  ADD CONSTRAINT `FK_D54A3670232D562B` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`),
  ADD CONSTRAINT `FK_D54A3670FDFF2E92` FOREIGN KEY (`thumbnail_id`) REFERENCES `images` (`id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `FK_7CA11A966BB74515` FOREIGN KEY (`house_id`) REFERENCES `houses` (`id`);

--
-- Constraints for table `room_images`
--
ALTER TABLE `room_images`
  ADD CONSTRAINT `FK_A15178AB3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`),
  ADD CONSTRAINT `FK_A15178AB54177093` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_1483A5E95F3EA365` FOREIGN KEY (`root_folder_id`) REFERENCES `folders` (`id`),
  ADD CONSTRAINT `FK_1483A5E9D4779068` FOREIGN KEY (`storage_plan`) REFERENCES `storage_plans` (`id`);

--
-- Constraints for table `user_objects`
--
ALTER TABLE `user_objects`
  ADD CONSTRAINT `FK_EA17D2F9232D562B` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_EA17D2F9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
