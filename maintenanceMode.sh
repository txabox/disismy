#!/bin/bash
echo ""
echo "*********************************************************************"
echo "************************  mainenanceMode.sh  ************************"
echo "*********************************************************************"
echo ""
echo ""

if [ -z ${DISISMY_ROOT+x} ]
then
    echo ""
    echo "*********************************************************************"
    echo "* DISISMY_ROOT is UNSET... escribe                                  *"
    echo '* export DISISMY_ROOT="path/to/disismy/root/in/your/computer"       *'
    echo "* probablemente en                                                  *"
    echo "* nano ~/.bash_profile                                              *"
    echo "* o                                                                 *"
    echo "* nano ~/.profile                                                   *"
    echo "* necesita reiniciar el terminal                                    *"
    echo "*********************************************************************"
    echo ""
    echo ""
    exit 1
fi

if [ -z ${NODE_ENV+x} ]
then
    echo ""
    echo "*********************************************************************"
    echo "* NODE_ENV is UNSET... escribe                                      *"
    echo '* export NODE_ENV="development|production"				          *'
    echo "* probablemente en                                                  *"
    echo "* nano ~/.bash_profile                                              *"
    echo "* o                                                                 *"
    echo "* nano ~/.profile                                                   *"
    echo "* necesita reiniciar el terminal                                    *"
    echo "*********************************************************************"
    echo ""
    echo ""
    exit 1
fi

if [ -z ${1+x} ]
then
    echo ""
    echo "*********************************************************************"
    echo "*                                                                   *"
    echo "* Tienes que pasar true para levantar el modo mantenimiento,        *"
    echo "* o false para quitar el modo mantenimiento.                        *"
    echo "* Por ejemplo:                                                      *"
    echo "* ./maintenanceMode.sh true|false                                   *"
    echo "*                                                                   *"
    echo "*********************************************************************"
    echo ""
    echo ""
    exit 1
else
    isMaintenanceMode=$1
    echo ""
    echo "*********************************************************************"
    echo "isMaintenanceMode: $isMaintenanceMode"
    echo "*********************************************************************"
    echo ""
fi

cd $DISISMY_ROOT


sudo chmod -R 777 $DISISMY_ROOT/var/cache/ && sudo chmod -R 777 $DISISMY_ROOT/var/logs/

if [ $isMaintenanceMode = "true" ]
then

    if [ $NODE_ENV = "production" ]
    then
        #quitamos la ultima linea de parameters.yml
        sed -i '$ d' app/config/parameters.yml
    elif [ $NODE_ENV = "development" ]
    then
        #quitamos la ultima linea de parameters.yml
        sed -i '' -e '$ d'  app/config/parameters.yml
    fi

    #y añadimos el nuevo valor
    echo "    maintenance: true" >> app/config/parameters.yml


elif [ $isMaintenanceMode = "false" ]
then
    if [ $NODE_ENV = "production" ]
    then
        #quitamos la ultima linea de parameters.yml
        sed -i '$ d' app/config/parameters.yml
    elif [ $NODE_ENV = "development" ]
    then
        #quitamos la ultima linea de parameters.yml
        sed -i '' -e '$ d'  app/config/parameters.yml
    fi

    #y añadimos el nuevo valor
    echo "    maintenance: false" >> app/config/parameters.yml

fi

sudo php $DISISMY_ROOT/bin/console cache:clear
sudo php $DISISMY_ROOT/bin/console cache:clear --env=prod

sudo chmod -R 777 $DISISMY_ROOT/var/cache/ && sudo chmod -R 777 $DISISMY_ROOT/var/logs/
