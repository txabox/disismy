<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StoragePlan
 *
 * @ORM\Table(name="storage_plans")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\StoragePlanRepository")
 * @ORM\HasLifecycleCallbacks
 */
class StoragePlan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;

    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string", length=10)
     */
    private $name_en;

    /**
     * @var string
     *
     * @ORM\Column(name="name_es", type="string", length=10)
     */
    private $name_es;

    /**
     * @var string
     *
     * @ORM\Column(name="description_en", type="string", length=255)
     */
    private $description_en;

    /**
     * @var string
     *
     * @ORM\Column(name="description_es", type="string", length=255)
     */
    private $description_es;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_public", type="boolean")
     */
    private $isPublic;

    /**
     * @var int
     *
     * @ORM\Column(name="bytes", type="bigint")
     */
    private $bytes;


    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    protected $annualPrice;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="storagePlan")
     */
    private $users;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }





    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->name_en;
                break;

            case 'es':
                return $this->name_es;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return House
     */
    public function setName( $name, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->name_en = $name;
                break;

            case 'es':
                $this->name_es = $name;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return House
     */
    public function setDescription( $description, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->description_en = $description;
                break;

            case 'es':
                $this->description_es = $description;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->description_en;
                break;

            case 'es':
                return $this->description_es;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     *
     * @return House
     */
    public function setNameEn($nameEn)
    {
        $this->name_en = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * Set nameEs
     *
     * @param string $nameEs
     *
     * @return House
     */
    public function setNameEs($nameEs)
    {
        $this->name_es = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->name_es;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return House
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->description_en = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->description_en;
    }

    /**
     * Set descriptionEs
     *
     * @param string $descriptionEs
     *
     * @return House
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->description_es = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->description_es;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return StoragePlan
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Add user
     *
     * @param \DisismyWebBundle\Entity\User $user
     *
     * @return StoragePlan
     */
    public function addUser(\DisismyWebBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \DisismyWebBundle\Entity\User $user
     */
    public function removeUser(\DisismyWebBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set bytes
     *
     * @param integer $bytes
     *
     * @return StoragePlan
     */
    public function setBytes($bytes)
    {
        $this->bytes = $bytes;

        return $this;
    }

    /**
     * Get bytes
     *
     * @return integer
     */
    public function getBytes()
    {
        return $this->bytes;
    }


    /**
     * Set annualPrice
     *
     * @param string $annualPrice
     *
     * @return StoragePlan
     */
    public function setAnnualPrice($annualPrice)
    {
        $this->annualPrice = $annualPrice;

        return $this;
    }

    /**
     * Get annualPrice
     *
     * @return string
     */
    public function getAnnualPrice()
    {
        return $this->annualPrice;
    }
}
