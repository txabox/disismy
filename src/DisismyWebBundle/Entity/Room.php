<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Room
 *
 * @ORM\Table(name="rooms")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\RoomRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Room
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;



    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string", length=80, nullable=true)
     */
    private $name_en = "";

    /**
     * @var string
     *
     * @ORM\Column(name="name_es", type="string", length=80, nullable=true)
     */
    private $name_es = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description_en", type="string", length=255, nullable=true)
     */
    private $description_en = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description_es", type="string", length=255, nullable=true)
     */
    private $description_es = "";


     /**
      * @ORM\Column(type="decimal", precision=10, scale=2, name="util_sq_meters")
      */
    private $utilSqMeters;


    /**
    * @ORM\Column(type="decimal", precision=10, scale=2, name="built_sq_meters")
    */
    private $builtSqMeters;

    /**
     * Txabox: en realidad esto es una relacion One-To-Many unidireccional de 1 room a N imagenes
     * xq las imagenes también pueden pertenecer a otros tipos de objetos
     * aunque parezca raro, las relaciones One-To-Many unidireccionales se mapean con ManyToMany
     * con una Join Table ( http://doctrine-orm.readthedocs.org/projects/doctrine-orm/en/latest/reference/association-mapping.html )
     *
     * @ORM\ManyToMany(targetEntity="Image")
     * @ORM\JoinTable(name="room_images",
     *      joinColumns={@ORM\JoinColumn(name="room_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $images;


    /**
     *
     * @ORM\OneToMany(targetEntity="PanoNode", mappedBy="room")
     */
    private $panoNodes;


    /**
     * @ORM\ManyToOne(targetEntity="House", inversedBy="rooms")
     * @ORM\JoinColumn(name="house_id", referencedColumnName="id")
     */
    private $house;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->panoNodes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getName( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->name_en;
                break;

            case 'es':
                return $this->name_es;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Room
     */
    public function setName( $name, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->name_en = $name;
                break;

            case 'es':
                $this->name_es = $name;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Room
     */
    public function setDescription( $description, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->description_en = $description;
                break;

            case 'es':
                $this->description_es = $description;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->description_en;
                break;

            case 'es':
                return $this->description_es;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     *
     * @return Room
     */
    public function setNameEn($nameEn)
    {
        $this->name_en = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * Set nameEs
     *
     * @param string $nameEs
     *
     * @return Room
     */
    public function setNameEs($nameEs)
    {
        $this->name_es = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->name_es;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return Room
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->description_en = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->description_en;
    }

    /**
     * Set descriptionEs
     *
     * @param string $descriptionEs
     *
     * @return Room
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->description_es = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->description_es;
    }

    /**
     * Add image
     *
     * @param \DisismyWebBundle\Entity\Image $image
     *
     * @return Room
     */
    public function addImage(\DisismyWebBundle\Entity\Image $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \DisismyWebBundle\Entity\Image $image
     */
    public function removeImage(\DisismyWebBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add panoNode
     *
     * @param \DisismyWebBundle\Entity\PanoNode $panoNode
     *
     * @return Room
     */
    public function addPanoNode(\DisismyWebBundle\Entity\PanoNode $panoNode)
    {
        $this->panoNodes[] = $panoNode;

        return $this;
    }

    /**
     * Remove panoNode
     *
     * @param \DisismyWebBundle\Entity\PanoNode $panoNode
     */
    public function removePanoNode(\DisismyWebBundle\Entity\PanoNode $panoNode)
    {
        $this->panoNodes->removeElement($panoNode);
    }

    /**
     * Get panoNodes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPanoNodes()
    {
        return $this->panoNodes;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Room
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Room
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set house
     *
     * @param \DisismyWebBundle\Entity\House $house
     *
     * @return Room
     */
    public function setHouse(\DisismyWebBundle\Entity\House $house = null)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return \DisismyWebBundle\Entity\House
     */
    public function getHouse()
    {
        return $this->house;
    }




    /**
     * Set utilSqMeters
     *
     * @param string $utilSqMeters
     *
     * @return Room
     */
    public function setUtilSqMeters($utilSqMeters)
    {
        $this->utilSqMeters = $utilSqMeters;

        return $this;
    }

    /**
     * Get utilSqMeters
     *
     * @return string
     */
    public function getUtilSqMeters()
    {
        return $this->utilSqMeters;
    }

    /**
     * Set builtSqMeters
     *
     * @param string $builtSqMeters
     *
     * @return Room
     */
    public function setBuiltSqMeters($builtSqMeters)
    {
        $this->builtSqMeters = $builtSqMeters;

        return $this;
    }

    /**
     * Get builtSqMeters
     *
     * @return string
     */
    public function getBuiltSqMeters()
    {
        return $this->builtSqMeters;
    }
}
