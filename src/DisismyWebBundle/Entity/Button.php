<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Button
 *
 * @ORM\Table(name="buttons")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\ButtonRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Button
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;


    /**
     * @var string
     *
     * @ORM\Column(name="fooName", type="string", length=255)
     */
    private $fooName;



    /**
     * @ORM\ManyToOne(targetEntity="PanoNode", inversedBy="buttons")
     * @ORM\JoinColumn(name="panoNode_owner", referencedColumnName="id")
     */
    private $panoNode_owner;


    /**
     * @ORM\ManyToOne(targetEntity="PanoNode")
     * @ORM\JoinColumn(name="target_panoNode", referencedColumnName="id")
     */
    private $targetPanoNode;

    /**
     * @ORM\OneToOne(targetEntity="Button")
     * @ORM\JoinColumn(name="pairedButton_id", referencedColumnName="id")
     */
    private $pairedButton;



    // POSITION


    /**
     * @ORM\Column(name="lon", type="float")
     */
    private $lon;

    /**
     * @ORM\Column(name="lat", type="float")
     */
    private $lat;



    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }

    /**
     * Set panoNode_owner
     *
     * @param \DisismyWebBundle\Entity\PanoNode $panoNode_owner
     *
     * @return Button
     */
    public function setPanoNode_owner(\DisismyWebBundle\Entity\PanoNode $panoNode_owner = null)
    {
        $this->panoNode_owner = $panoNode_owner;

        return $this;
    }

    /**
     * Get panoNode_owner
     *
     * @return \DisismyWebBundle\Entity\PanoNode
     */
    public function getPanoNode_owner()
    {
        return $this->panoNode_owner;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     *
     * @return Button
     */
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     *
     * @return Button
     */
    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Button
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Button
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set panoNodeOwner
     *
     * @param \DisismyWebBundle\Entity\PanoNode $panoNodeOwner
     *
     * @return Button
     */
    public function setPanoNodeOwner(\DisismyWebBundle\Entity\PanoNode $panoNodeOwner = null)
    {
        $this->panoNode_owner = $panoNodeOwner;

        return $this;
    }

    /**
     * Get panoNodeOwner
     *
     * @return \DisismyWebBundle\Entity\PanoNode
     */
    public function getPanoNodeOwner()
    {
        return $this->panoNode_owner;
    }



    /**
     * Set targetPanoNode
     *
     * @param \DisismyWebBundle\Entity\PanoNode $targetPanoNode
     *
     * @return Button
     */
    public function setTargetPanoNode(\DisismyWebBundle\Entity\PanoNode $targetPanoNode = null)
    {
        $this->targetPanoNode = $targetPanoNode;

        return $this;
    }

    /**
     * Get targetPanoNode
     *
     * @return \DisismyWebBundle\Entity\PanoNode
     */
    public function getTargetPanoNode()
    {
        return $this->targetPanoNode;
    }

    /**
     * Set pairedButton
     *
     * @param \DisismyWebBundle\Entity\Button $pairedButton
     *
     * @return Button
     */
    public function setPairedButton(\DisismyWebBundle\Entity\Button $pairedButton = null)
    {
        $this->pairedButton = $pairedButton;

        return $this;
    }

    /**
     * Get pairedButton
     *
     * @return \DisismyWebBundle\Entity\Button
     */
    public function getPairedButton()
    {
        return $this->pairedButton;
    }

    /**
     * Set fooName
     *
     * @param string $fooName
     *
     * @return Button
     */
    public function setFooName($fooName)
    {
        $this->fooName = $fooName;

        return $this;
    }

    /**
     * Get fooName
     *
     * @return string
     */
    public function getFooName()
    {
        return $this->fooName;
    }

    /**
     * Set lon
     *
     * @param float $lon
     *
     * @return Button
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return Button
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }
}
