<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PanoNode
 *
 * @ORM\Table(name="panoNodes")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\PanoNodeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PanoNode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;



    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string", length=80, nullable=true)
     */
    private $name_en = "";

    /**
     * @var string
     *
     * @ORM\Column(name="name_es", type="string", length=80, nullable=true)
     */
    private $name_es = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description_en", type="string", length=255, nullable=true)
     */
    private $description_en = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description_es", type="string", length=255, nullable=true)
     */
    private $description_es = "";



    /**
     * @ORM\ManyToOne(targetEntity="Pano", inversedBy="panoNodes")
     * @ORM\JoinColumn(name="pano_id", referencedColumnName="id")
     */
    private $pano;


    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="panoNodes")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    private $room;


    /**
     * @ORM\OneToMany(targetEntity="Button", mappedBy="panoNode_owner")
     */
    private $buttons;

    /**
     * @ORM\OneToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $texture;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->buttons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }



    /**
     * Set pano
     *
     * @param \DisismyWebBundle\Entity\Pano $pano
     *
     * @return PanoNode
     */
    public function setPano(\DisismyWebBundle\Entity\Pano $pano = null)
    {
        $this->pano = $pano;

        return $this;
    }

    /**
     * Get pano
     *
     * @return \DisismyWebBundle\Entity\Pano
     */
    public function getPano()
    {
        return $this->pano;
    }

    /**
     * Set room
     *
     * @param \DisismyWebBundle\Entity\Room $room
     *
     * @return PanoNode
     */
    public function setRoom(\DisismyWebBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \DisismyWebBundle\Entity\Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Add button
     *
     * @param \DisismyWebBundle\Entity\Button $button
     *
     * @return PanoNode
     */
    public function addButton(\DisismyWebBundle\Entity\Button $button)
    {
        $this->buttons[] = $button;

        return $this;
    }

    /**
     * Remove button
     *
     * @param \DisismyWebBundle\Entity\Button $button
     */
    public function removeButton(\DisismyWebBundle\Entity\Button $button)
    {
        $this->buttons->removeElement($button);
    }

    /**
     * Get buttons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getButtons()
    {
        return $this->buttons;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PanoNode
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return PanoNode
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


    /**
     * Set texture
     *
     * @param \DisismyWebBundle\Entity\Image $texture
     *
     * @return PanoNode
     */
    public function setTexture(\DisismyWebBundle\Entity\Image $texture = null)
    {
        $this->texture = $texture;

        return $this;
    }

    /**
     * Get texture
     *
     * @return \DisismyWebBundle\Entity\Image
     */
    public function getTexture()
    {
        return $this->texture;
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getName( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->name_en;
                break;

            case 'es':
                return $this->name_es;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Room
     */
    public function setName( $name, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->name_en = $name;
                break;

            case 'es':
                $this->name_es = $name;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Room
     */
    public function setDescription( $description, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->description_en = $description;
                break;

            case 'es':
                $this->description_es = $description;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->description_en;
                break;

            case 'es':
                return $this->description_es;
                break;
            default:
                die( "No tenemos el idioma $locale en Room" );
                break;
        }
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     *
     * @return Room
     */
    public function setNameEn($nameEn)
    {
        $this->name_en = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * Set nameEs
     *
     * @param string $nameEs
     *
     * @return Room
     */
    public function setNameEs($nameEs)
    {
        $this->name_es = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->name_es;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return Room
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->description_en = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->description_en;
    }

    /**
     * Set descriptionEs
     *
     * @param string $descriptionEs
     *
     * @return Room
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->description_es = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->description_es;
    }


}
