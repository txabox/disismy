<?php
// src/DisismyWebBundle/Entity/User.php

namespace DisismyWebBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="encodedId", type="integer", nullable=true)
     */
    private $encodedId;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;


    /**
     * @var bool
     *
     * @ORM\Column(name="is_public", type="boolean")
     */
    private $isPublic;


    /**
     * @var string
     *
     * @ORM\Column(name="description_en", type="string", length=255, nullable=true)
     */
    private $description_en;

    /**
     * @var string
     *
     * @ORM\Column(name="description_es", type="string", length=255, nullable=true)
     */
    private $description_es;


    /**
     * @ORM\OneToMany(targetEntity="Object", mappedBy="owner")
     */
    private $objects;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="owner")
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="Object", mappedBy="viewers")
     */
    private $viewableObjects;


    /**
     * @ORM\ManyToOne(targetEntity="StoragePlan", inversedBy="users")
     * @ORM\JoinColumn(name="storage_plan", referencedColumnName="id")
     */
    private $storagePlan;


    /**
     * @var int
     *
     * @ORM\Column(name="consumedBytes", type="integer")
     */
    private $consumedBytes;


    /**
     * @ORM\OneToOne(targetEntity="Folder")
     * @ORM\JoinColumn(name="root_folder_id", referencedColumnName="id")
     */
    private $rootFolder;


    // ...
    /**
     * @ORM\OneToMany(targetEntity="Folder", mappedBy="owner")
     */
    private $folders;






    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }





    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->folders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->objects = new \Doctrine\Common\Collections\ArrayCollection();
        $this->viewableObjects = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isPublic = true;
        $this->consumedBytes = 0;
    }

    /**
     * Add image
     *
     * @param \DisismyWebBundle\Entity\Image $image
     *
     * @return User
     */
    public function addImage(\DisismyWebBundle\Entity\Image $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \DisismyWebBundle\Entity\Image $image
     */
    public function removeImage(\DisismyWebBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add object
     *
     * @param \DisismyWebBundle\Entity\Object $object
     *
     * @return User
     */
    public function addObject(\DisismyWebBundle\Entity\Object $object)
    {
        $this->objects[] = $object;

        return $this;
    }

    /**
     * Remove object
     *
     * @param \DisismyWebBundle\Entity\Object $object
     */
    public function removeObject(\DisismyWebBundle\Entity\Object $object)
    {
        $this->objects->removeElement($object);
    }

    /**
     * Get objects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjects()
    {
        return $this->objects;
    }



    /**
     * Set description
     *
     * @param string $description
     *
     * @return User
     */
    public function setDescription( $description, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->description_en = $description;
                break;

            case 'es':
                $this->description_es = $description;
                break;
            default:
                die( "No tenemos el idioma $locale en User" );
                break;
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->description_en;
                break;

            case 'es':
                return $this->description_es;
                break;
            default:
                die( "No tenemos el idioma $locale en User" );
                break;
        }
    }

    /**
     *
     * @return array
     */
    public function toArray( $globals, $locale )
    {
        $thisArray = array(
            "encodedId" => $globals->optimus->encode( $this->id ),
            "name" => $this->getUsername(),
            "desc" => $this->getDescription( $locale )
        );
        return $thisArray;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return User
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->description_en = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->description_en;
    }

    /**
     * Set descriptionEs
     *
     * @param string $descriptionEs
     *
     * @return User
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->description_es = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->description_es;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return User
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }



    /**
     * Add viewableObject
     *
     * @param \DisismyWebBundle\Entity\Object $viewableObject
     *
     * @return User
     */
    public function addViewableObject(\DisismyWebBundle\Entity\Object $viewableObject)
    {
        $this->viewableObjects[] = $viewableObject;

        return $this;
    }

    /**
     * Remove viewableObject
     *
     * @param \DisismyWebBundle\Entity\Object $viewableObject
     */
    public function removeViewableObject(\DisismyWebBundle\Entity\Object $viewableObject)
    {
        $this->viewableObjects->removeElement($viewableObject);
    }

    /**
     * Get viewableObjects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewableObjects()
    {
        return $this->viewableObjects;
    }

    /**
     * Set storagePlan
     *
     * @param \DisismyWebBundle\Entity\storagePlan $storagePlan
     *
     * @return User
     */
    public function setStoragePlan(\DisismyWebBundle\Entity\storagePlan $storagePlan = null)
    {
        $this->storagePlan = $storagePlan;

        return $this;
    }

    /**
     * Get storagePlan
     *
     * @return \DisismyWebBundle\Entity\storagePlan
     */
    public function getStoragePlan()
    {
        return $this->storagePlan;
    }

    /**
     * Set consumedBytes
     *
     * @param integer $consumedBytes
     *
     * @return User
     */
    public function setConsumedBytes($consumedBytes)
    {
        $this->consumedBytes = $consumedBytes;

        return $this;
    }

    /**
     * Get consumedBytes
     *
     * @return integer
     */
    public function getConsumedBytes()
    {
        return $this->consumedBytes;
    }

    /**
     * Add folder
     *
     * @param \DisismyWebBundle\Entity\Folder $folder
     *
     * @return User
     */
    public function addFolder(\DisismyWebBundle\Entity\Folder $folder)
    {
        $this->folders[] = $folder;

        return $this;
    }

    /**
     * Remove folder
     *
     * @param \DisismyWebBundle\Entity\Folder $folder
     */
    public function removeFolder(\DisismyWebBundle\Entity\Folder $folder)
    {
        $this->folders->removeElement($folder);
    }

    /**
     * Get folders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFolders()
    {
        return $this->folders;
    }



    /**
     * Set rootFolder
     *
     * @param \DisismyWebBundle\Entity\Folder $rootFolder
     *
     * @return User
     */
    public function setRootFolder(\DisismyWebBundle\Entity\Folder $rootFolder = null)
    {
        $this->rootFolder = $rootFolder;

        return $this;
    }

    /**
     * Get rootFolder
     *
     * @return \DisismyWebBundle\Entity\Folder
     */
    public function getRootFolder()
    {
        return $this->rootFolder;
    }

    /**
     * Set encodedId
     *
     * @param integer $encodedId
     *
     * @return User
     */
    public function setEncodedId($encodedId)
    {
        $this->encodedId = $encodedId;

        return $this;
    }

    /**
     * Get encodedId
     *
     * @return integer
     */
    public function getEncodedId()
    {
        return $this->encodedId;
    }
}
