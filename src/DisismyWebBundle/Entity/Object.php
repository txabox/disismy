<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Object
 *
 * @ORM\Table(name="objects")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\ObjectRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Object
{

    // rentingPeriod
    // rentingPeriodUnits


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;

    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string", length=80)
     */
    private $name_en;

    /**
     * @var string
     *
     * @ORM\Column(name="name_es", type="string", length=80)
     */
    private $name_es;

    /**
     * @var string
     *
     * @ORM\Column(name="description_en", type="string", length=255)
     */
    private $description_en;

    /**
     * @var string
     *
     * @ORM\Column(name="description_es", type="string", length=255)
     */
    private $description_es;

     /**
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="thumbnail_id", referencedColumnName="id")
     */
    private $thumbnail;



    /**
     * @var bool
     *
     * @ORM\Column(name="is_public", type="boolean")
     */
    private $isPublic;


    /**
     * @ORM\ManyToMany(targetEntity="Language", inversedBy="objects")
     * @ORM\JoinTable(name="objects_languages")
     */
    private $languages;



    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="objects")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     */
    private $owner;


    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="viewableObjects")
     * @ORM\JoinTable(name="object_viewers")
     */
    private $viewers;

    /**
     * @ORM\OneToOne(targetEntity="House", mappedBy="object")
     */
    private $house;

    /**
     * @ORM\ManyToOne(targetEntity="ObjectType", inversedBy="objects")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;


    /**
     * @ORM\OneToMany(targetEntity="Pano", mappedBy="object")
     */
    private $panos;


    /**
     * @var bool
     *
     * @ORM\Column(name="is_on_sale", type="boolean")
     */
    private $isOnSale;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    protected $onSalePrice;


    /**
     * @var bool
     *
     * @ORM\Column(name="is_on_rent", type="boolean")
     */
    private $isOnRent;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    protected $onRentPrice;


    /**
     * @ORM\ManyToOne(targetEntity="TimePeriods")
     * @ORM\JoinColumn(name="renting_period", referencedColumnName="id", nullable=false)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $rentingPeriod;

    /**
     * @ORM\Column(type="smallint", name="renting_period_units")
     */
    private $rentingPeriodUnits;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }


    /**
     *
     * @return integer
     */
    public function getThumbnailId()
    {
        $defaultThumbId = 16;
        $thumbnailId = $this->getThumbnail() ? $this->getThumbnail()->getId() : $defaultThumbId;

        return $thumbnailId;

    }



    /**
     *
     * @return array
     */
    public function toArray( $globals, $locale, $curUser = false )
    {
        $thisArray = array(
            "encodedId" => $globals->optimus->encode( $this->id ),
            "type" => $this->getType()->toArray( $globals, $locale ),
            // para saber el typedObject hay que hacerlo desde el Controller
            // mira en showObjectAction en ObjectsController.php
            // "typedObject" => $this->getHouse() ? $this->getHouse()->toArray( $globals, $locale ) : null,
            "name" => $this->getName( $locale ),
            "name_en" => $this->getNameEn(),
            "name_es" => $this->getNameEs(),
            "desc" => $this->getDescription( $locale ),
            "desc_en" => $this->getDescriptionEn(),
            "desc_es" => $this->getDescriptionEs(),
            "isOnSale" => $this->getIsOnSale(),
            "onSalePrice" => $this->getOnSalePrice(),
            "isOnRent" => $this->getIsOnRent(),
            "onRentPrice" => $this->getOnRentPrice(),
            "rentingPeriodUnits" => $this->getRentingPeriodUnits(),
            "thumbId" => $globals->optimus->encode( $this->getThumbnailId() ),
            "isPublic" => $this->getIsPublic(),
            "ownerEncodedId" => $globals->optimus->encode( $this->getOwner()->getId() ),
        );

        // if curUser is the owner of the object
        if( $curUser &&
        $curUser->getId() === $this->getOwner()->getId() )
        {
            $thisArray[ "languages" ] = [];
            $languages = $this->getLanguages();
            foreach( $languages as $language )
            {
                array_push( $thisArray[ "languages" ], $language->toArray( $globals, $locale ) );
            }

            $thisArray[ "viewers" ] = [];
            $viewers = $this->getViewers();
            foreach( $viewers as $viewer )
            {
                array_push( $thisArray[ "viewers" ], $viewer->toArray( $globals, $locale ) );
            }
        }

        return $thisArray;
    }






    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->panos = new \Doctrine\Common\Collections\ArrayCollection();
    }




    /**
     * Add pano
     *
     * @param \DisismyWebBundle\Entity\Pano $pano
     *
     * @return Object
     */
    public function addPano(\DisismyWebBundle\Entity\Pano $pano)
    {
        $this->panos[] = $pano;

        return $this;
    }

    /**
     * Remove pano
     *
     * @param \DisismyWebBundle\Entity\Pano $pano
     */
    public function removePano(\DisismyWebBundle\Entity\Pano $pano)
    {
        $this->panos->removeElement($pano);
    }

    /**
     * Get panos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPanos()
    {
        return $this->panos;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Object
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Object
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set isOnSale
     *
     * @param boolean $isOnSale
     *
     * @return Object
     */
    public function setIsOnSale($isOnSale)
    {
        $this->isOnSale = $isOnSale;

        return $this;
    }

    /**
     * Get isOnSale
     *
     * @return boolean
     */
    public function getIsOnSale()
    {
        return $this->isOnSale;
    }

    /**
     * Set isOnRent
     *
     * @param boolean $isOnRent
     *
     * @return Object
     */
    public function setIsOnRent($isOnRent)
    {
        $this->isOnRent = $isOnRent;

        return $this;
    }

    /**
     * Get isOnRent
     *
     * @return boolean
     */
    public function getIsOnRent()
    {
        return $this->isOnRent;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->name_en;
                break;

            case 'es':
                return $this->name_es;
                break;
            default:
                die( "No tenemos el idioma $locale en Object" );
                break;
        }
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return House
     */
    public function setName( $name, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->name_en = $name;
                break;

            case 'es':
                $this->name_es = $name;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return House
     */
    public function setDescription( $description, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->description_en = $description;
                break;

            case 'es':
                $this->description_es = $description;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->description_en;
                break;

            case 'es':
                return $this->description_es;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     *
     * @return House
     */
    public function setNameEn($nameEn)
    {
        $this->name_en = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * Set nameEs
     *
     * @param string $nameEs
     *
     * @return House
     */
    public function setNameEs($nameEs)
    {
        $this->name_es = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->name_es;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return House
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->description_en = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->description_en;
    }

    /**
     * Set descriptionEs
     *
     * @param string $descriptionEs
     *
     * @return House
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->description_es = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->description_es;
    }

    /**
     * Set house
     *
     * @param \DisismyWebBundle\Entity\House $house
     *
     * @return Object
     */
    public function setHouse(\DisismyWebBundle\Entity\House $house = null)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return \DisismyWebBundle\Entity\House
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set onSalePrice
     *
     * @param string $onSalePrice
     *
     * @return Object
     */
    public function setOnSalePrice($onSalePrice)
    {
        $this->onSalePrice = $onSalePrice;

        return $this;
    }

    /**
     * Get onSalePrice
     *
     * @return string
     */
    public function getOnSalePrice()
    {
        return $this->onSalePrice;
    }

    /**
     * Set onRentPrice
     *
     * @param string $onRentPrice
     *
     * @return Object
     */
    public function setOnRentPrice($onRentPrice)
    {
        $this->onRentPrice = $onRentPrice;

        return $this;
    }

    /**
     * Get onRentPrice
     *
     * @return string
     */
    public function getOnRentPrice()
    {
        return $this->onRentPrice;
    }

    /**
     * Set rentingPeriodUnits
     *
     * @param integer $rentingPeriodUnits
     *
     * @return Object
     */
    public function setRentingPeriodUnits($rentingPeriodUnits)
    {
        $this->rentingPeriodUnits = $rentingPeriodUnits;

        return $this;
    }

    /**
     * Get rentingPeriodUnits
     *
     * @return integer
     */
    public function getRentingPeriodUnits()
    {
        return $this->rentingPeriodUnits;
    }

    /**
     * Set thumbnail
     *
     * @param \DisismyWebBundle\Entity\Image $thumbnail
     *
     * @return Object
     */
    public function setThumbnail(\DisismyWebBundle\Entity\Image $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return \DisismyWebBundle\Entity\Image
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return Object
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set type
     *
     * @param \DisismyWebBundle\Entity\ObjectType $type
     *
     * @return Object
     */
    public function setType(\DisismyWebBundle\Entity\ObjectType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \DisismyWebBundle\Entity\ObjectType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set rentingPeriod
     *
     * @param \DisismyWebBundle\Entity\TimePeriods $rentingPeriod
     *
     * @return Object
     */
    public function setRentingPeriod(\DisismyWebBundle\Entity\TimePeriods $rentingPeriod = null)
    {
        $this->rentingPeriod = $rentingPeriod;

        return $this;
    }

    /**
     * Get rentingPeriod
     *
     * @return \DisismyWebBundle\Entity\TimePeriods
     */
    public function getRentingPeriod()
    {
        return $this->rentingPeriod;
    }

    /**
     * Set owner
     *
     * @param \DisismyWebBundle\Entity\User $owner
     *
     * @return Object
     */
    public function setOwner(\DisismyWebBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \DisismyWebBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add language
     *
     * @param \DisismyWebBundle\Entity\Language $language
     *
     * @return Object
     */
    public function addLanguage(\DisismyWebBundle\Entity\Language $language)
    {
        $this->languages[] = $language;

        return $this;
    }

    /**
     * Remove language
     *
     * @param \DisismyWebBundle\Entity\Language $language
     */
    public function removeLanguage(\DisismyWebBundle\Entity\Language $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }


    /**
     * Add viewer
     *
     * @param \DisismyWebBundle\Entity\User $viewer
     *
     * @return Object
     */
    public function addViewer(\DisismyWebBundle\Entity\User $viewer)
    {
        $this->viewers[] = $viewer;

        return $this;
    }

    /**
     * Remove viewer
     *
     * @param \DisismyWebBundle\Entity\User $viewer
     */
    public function removeViewer(\DisismyWebBundle\Entity\User $viewer)
    {
        $this->viewers->removeElement($viewer);
    }

    /**
     * Get viewers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewers()
    {
        return $this->viewers;
    }
}
