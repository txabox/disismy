<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * House
 *
 * @ORM\Table(name="houses")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\HouseRepository")
 * @ORM\HasLifecycleCallbacks
 */
class House
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;


    /**
     * @ORM\OneToOne(targetEntity="Object", inversedBy="house")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id")
     */
    private $object;



    /**
     * @ORM\OneToMany(targetEntity="Room", mappedBy="house", cascade={"persist"})
     */
    private $rooms;

    /**
     * @ORM\Column(type="integer", name="util_sq_meters")
     */
    private $utilSqMeters;

    /**
     * @ORM\Column(type="integer", name="built_sq_meters")
     */
    private $builtSqMeters;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->rooms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     *
     * @return array
     */
    public function toArray( $globals, $locale )
    {
        $thisArray = array(
            "encodedId" => $globals->optimus->decode( $this->id ),
            "objectId" => $globals->optimus->decode( $this->getObject()->getId() ),
            "utilSqMeters" => $this->getUtilSqMeters(),
            "builtSqMeters" => $this->getBuiltSqMeters()
        );
        return $thisArray;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }


    /**
     * Set object
     *
     * @param \DisismyWebBundle\Entity\Object $object
     *
     * @return House
     */
    public function setObject(\DisismyWebBundle\Entity\Object $object = null)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return \DisismyWebBundle\Entity\Object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Add room
     *
     * @param \DisismyWebBundle\Entity\Room $room
     *
     * @return House
     */
    public function addRoom(\DisismyWebBundle\Entity\Room $room)
    {
        $room->setHouse($this);
        $this->rooms[] = $room;

        return $this;
    }

    /**
     * Remove room
     *
     * @param \DisismyWebBundle\Entity\Room $room
     */
    public function removeRoom(\DisismyWebBundle\Entity\Room $room)
    {
        $this->rooms->removeElement($room);
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return House
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return House
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set utilSqMeters
     *
     * @param integer $utilSqMeters
     *
     * @return House
     */
    public function setUtilSqMeters($utilSqMeters)
    {
        $this->utilSqMeters = $utilSqMeters;

        return $this;
    }

    /**
     * Get utilSqMeters
     *
     * @return integer
     */
    public function getUtilSqMeters()
    {
        return $this->utilSqMeters;
    }

    /**
     * Set builtSqMeters
     *
     * @param integer $builtSqMeters
     *
     * @return House
     */
    public function setBuiltSqMeters($builtSqMeters)
    {
        $this->builtSqMeters = $builtSqMeters;

        return $this;
    }

    /**
     * Get builtSqMeters
     *
     * @return integer
     */
    public function getBuiltSqMeters()
    {
        return $this->builtSqMeters;
    }

}
