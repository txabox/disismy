<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Folder
 *
 * @ORM\Table(name="folders")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\FolderRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Folder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, nullable=true)
     */
    private $name;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="folders")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;


    /**
     * @ORM\OneToMany(targetEntity="Folder", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="folder")
     */
    private $images;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Folder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Folder
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Folder
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set owner
     *
     * @param \DisismyWebBundle\Entity\User $owner
     *
     * @return Folder
     */
    public function setOwner(\DisismyWebBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \DisismyWebBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add child
     *
     * @param \DisismyWebBundle\Entity\Folder $child
     *
     * @return Folder
     */
    public function addChild(\DisismyWebBundle\Entity\Folder $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \DisismyWebBundle\Entity\Folder $child
     */
    public function removeChild(\DisismyWebBundle\Entity\Folder $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \DisismyWebBundle\Entity\Folder $parent
     *
     * @return Folder
     */
    public function setParent(\DisismyWebBundle\Entity\Folder $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \DisismyWebBundle\Entity\Folder
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add image
     *
     * @param \DisismyWebBundle\Entity\Image $image
     *
     * @return Folder
     */
    public function addImage(\DisismyWebBundle\Entity\Image $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \DisismyWebBundle\Entity\Image $image
     */
    public function removeImage(\DisismyWebBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     *
     * @return array
     */
    public function toArray( $globals, $locale )
    {

        $children   = array();
        $images     = array();

        foreach( $this->getChildren() as $value )
        {

            $childFolder = array(
                "encodedId" => $globals->optimus->encode( $value->id ),
                "name" => $value->getName(),
                // "encodedOwnerId" => $globals->optimus->encode( $value->getOwner()->getId() ),
                "parent" => $globals->optimus->encode( $this->id ),
            );

            array_push( $children, $childFolder );

        }

        foreach( $this->getImages() as $image )
        {
            array_push( $images, $image->toArray( $globals, $locale ) );
        }


        $thisArray = array(
            "encodedId"         => $globals->optimus->encode( $this->id ),
            "name"              => $this->getName(),
            // "encodedOwnerId"    => $globals->optimus->encode( $this->getOwner()->getId() ),
            "parent"            => $this->getParent() ? $parent->toArray( $globals, $locale ) : null,
            "children"          => $children,
            "images"            => $images
        );

        return $thisArray;
    }
}
