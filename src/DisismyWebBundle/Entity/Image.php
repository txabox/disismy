<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Image
 *
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;



    // VichUploaderBundle
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    // VichUploaderBundle
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;



    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="images")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="alt_en", type="string", length=80, nullable = true)
     */
    private $alt_en;

    /**
     * @var string
     *
     * @ORM\Column(name="alt_es", type="string", length=80, nullable = true)
     */
    private $alt_es;

    /**
     * @var string
     *
     * @ORM\Column(name="description_en", type="string", length=255, nullable=true, nullable = true)
     */
    private $description_en;

    /**
     * @var string
     *
     * @ORM\Column(name="description_es", type="string", length=255, nullable=true, nullable = true)
     */
    private $description_es;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=4, nullable=false)
     */
    private $extension;


    /**
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="images")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    private $folder;







    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->alt_en;
                break;

            case 'es':
                return $this->alt_es;
                break;
            default:
                die( "No tenemos el idioma $locale en Image" );
                break;
        }
    }


    /**
     * Set alt
     *
     * @param string $alt
     *
     * @return House
     */
    public function setAlt( $alt, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->alt_en = $alt;
                break;

            case 'es':
                $this->alt_es = $alt;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return House
     */
    public function setDescription( $description, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->description_en = $description;
                break;

            case 'es':
                $this->description_es = $description;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->description_en;
                break;

            case 'es':
                return $this->description_es;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
    }

    /**
     * Set altEn
     *
     * @param string $altEn
     *
     * @return House
     */
    public function setAltEn($altEn)
    {
        $this->alt_en = $altEn;

        return $this;
    }

    /**
     * Get altEn
     *
     * @return string
     */
    public function getAltEn()
    {
        return $this->alt_en;
    }

    /**
     * Set altEs
     *
     * @param string $altEs
     *
     * @return House
     */
    public function setAltEs($altEs)
    {
        $this->alt_es = $altEs;

        return $this;
    }

    /**
     * Get altEs
     *
     * @return string
     */
    public function getAltEs()
    {
        return $this->alt_es;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return House
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->description_en = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->description_en;
    }

    /**
     * Set descriptionEs
     *
     * @param string $descriptionEs
     *
     * @return House
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->description_es = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->description_es;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Image
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Image
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }



    /**
     *
     * @return array
     */
    public function toArray( $globals, $locale )
    {

        $thisArray = array(
            "encodedId"         => $globals->optimus->encode( $this->id ),
            "name"              => $this->imageName,
            "alt"               => $this->getAlt( $locale ),
            "desc"              => $this->getDescription( $locale ),
            "folderEncodedId"   => $globals->optimus->encode( $this->getFolder()->getId() ),
        );

        return $thisArray;
    }


    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return Image
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set owner
     *
     * @param \DisismyWebBundle\Entity\User $owner
     *
     * @return Image
     */
    public function setOwner(\DisismyWebBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \DisismyWebBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Image
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set folder
     *
     * @param \DisismyWebBundle\Entity\Folder $folder
     *
     * @return Image
     */
    public function setFolder(\DisismyWebBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return \DisismyWebBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }
}
