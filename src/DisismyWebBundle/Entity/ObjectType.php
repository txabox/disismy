<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ObjectType
 *
 * @ORM\Table(name="object_types")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\ObjectTypeRepository")
 */
class ObjectType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Object", mappedBy="type")
     */
    private $objects;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->objects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ObjectType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add object
     *
     * @param \DisismyWebBundle\Entity\Object $object
     *
     * @return ObjectType
     */
    public function addObject(\DisismyWebBundle\Entity\Object $object)
    {
        $this->objects[] = $object;

        return $this;
    }

    /**
     * Remove object
     *
     * @param \DisismyWebBundle\Entity\Object $object
     */
    public function removeObject(\DisismyWebBundle\Entity\Object $object)
    {
        $this->objects->removeElement($object);
    }

    /**
     * Get objects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjects()
    {
        return $this->objects;
    }

    public function __toString()
    {
        return "$this->name";
    }

    /**
     *
     * @return array
     */
    public function toArray( $globals, $locale )
    {
        $thisArray = array(
            "encodedId" => $globals->optimus->encode( $this->id ),
            "name" => $this->getName( $locale ),
        );
        return $thisArray;
    }
}
