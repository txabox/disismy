<?php

namespace DisismyWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pano
 *
 * @ORM\Table(name="panos")
 * @ORM\Entity(repositoryClass="DisismyWebBundle\Repository\PanoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Pano
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="thumbnail_id", referencedColumnName="id")
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string", length=80)
     */
    private $name_en;

    /**
     * @var string
     *
     * @ORM\Column(name="name_es", type="string", length=80)
     */
    private $name_es;

    /**
     * @var string
     *
     * @ORM\Column(name="description_en", type="string", length=255)
     */
    private $description_en;

    /**
     * @var string
     *
     * @ORM\Column(name="description_es", type="string", length=255)
     */
    private $description_es;


    /**
     * @ORM\ManyToOne(targetEntity="Object", inversedBy="panos")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id")
     */
    private $object;


    /**
     *
     * @ORM\OneToMany(targetEntity="PanoNode", mappedBy="pano")
     */
    private $panoNodes;


    /**
     * @ORM\OneToOne(targetEntity="PanoNOde")
     * @ORM\JoinColumn(name="initial_panoNode", referencedColumnName="id")
     */
    private $initialPanoNode;


    /**
     * @ORM\Column(name="initialPanoNodeLon", type="float")
     */
     // giro de izda a derecha
    private $initialPanoNodeLon;


    /**
     * @ORM\Column(name="initialPanoNodeLat", type="float")
     */
     // giro de izda a derecha
    private $initialPanoNodeLat;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->panoNodes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }




    /**
     * Get name
     *
     * @return string
     */
    public function getName( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->name_en;
                break;

            case 'es':
                return $this->name_es;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return House
     */
    public function setName( $name, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->name_en = $name;
                break;

            case 'es':
                $this->name_es = $name;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return House
     */
    public function setDescription( $description, $locale )
    {
        switch( $locale )
        {
            case 'en':
                $this->description_en = $description;
                break;

            case 'es':
                $this->description_es = $description;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription( $locale )
    {
        switch( $locale )
        {
            case 'en':
                return $this->description_en;
                break;

            case 'es':
                return $this->description_es;
                break;
            default:
                die( "No tenemos el idioma $locale en House" );
                break;
        }
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     *
     * @return House
     */
    public function setNameEn($nameEn)
    {
        $this->name_en = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * Set nameEs
     *
     * @param string $nameEs
     *
     * @return House
     */
    public function setNameEs($nameEs)
    {
        $this->name_es = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->name_es;
    }

    /**
     * Set descriptionEn
     *
     * @param string $descriptionEn
     *
     * @return House
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->description_en = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->description_en;
    }

    /**
     * Set descriptionEs
     *
     * @param string $descriptionEs
     *
     * @return House
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->description_es = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->description_es;
    }

    /**
     *
     * @return array
     */
    public function toArray( $globals, $locale )
    {


        $thisArray = array(
            "encodedId" => $globals->optimus->encode( $this->id ),
            "name" => $this->getName( $locale ),
            "desc" => $this->getDescription( $locale ),
            "initialPanoNodeEncodedId" => $globals->optimus->encode( $this->getInitialPanoNode()->getId() ),
            "initialPanoNodeLon" => $this->getInitialPanoNodeLon(),
            "initialPanoNodeLat" => $this->getInitialPanoNodeLat()
        );

        return $thisArray;

    }

    /**
     *
     * @return array
     */
    public function toArrayWithObject( $globals, $locale )
    {


        $thisArray = array(
            "encodedId" => $globals->optimus->encode( $this->id ),
            "name" => $this->getName( $locale ),
            "desc" => $this->getDescription( $locale ),
            "initialPanoNodeEncodedId" => $globals->optimus->encode( $this->getInitialPanoNode()->getId() ),
            "initialPanoNodeLon" => $this->getInitialPanoNodeLon(),
            "initialPanoNodeLat" => $this->getInitialPanoNodeLat(),
            "object" => $this->getObject()->toArray( $globals, $locale )
        );

        return $thisArray;

    }

    /**
     * Set object
     *
     * @param \DisismyWebBundle\Entity\Object $object
     *
     * @return Pano
     */
    public function setObject(\DisismyWebBundle\Entity\Object $object = null)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return \DisismyWebBundle\Entity\Object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Add panoNode
     *
     * @param \DisismyWebBundle\Entity\PanoNode $panoNode
     *
     * @return Pano
     */
    public function addPanoNode(\DisismyWebBundle\Entity\PanoNode $panoNode)
    {
        $this->panoNodes[] = $panoNode;

        return $this;
    }

    /**
     * Remove panoNode
     *
     * @param \DisismyWebBundle\Entity\PanoNode $panoNode
     */
    public function removePanoNode(\DisismyWebBundle\Entity\PanoNode $panoNode)
    {
        $this->panoNodes->removeElement($panoNode);
    }

    /**
     * Get panoNodes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPanoNodes()
    {
        return $this->panoNodes;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pano
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pano
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set initialPanoNode
     *
     * @param \DisismyWebBundle\Entity\PanoNOde $initialPanoNode
     *
     * @return Pano
     */
    public function setInitialPanoNode(\DisismyWebBundle\Entity\PanoNOde $initialPanoNode = null)
    {
        $this->initialPanoNode = $initialPanoNode;

        return $this;
    }

    /**
     * Get initialPanoNode
     *
     * @return \DisismyWebBundle\Entity\PanoNOde
     */
    public function getInitialPanoNode()
    {
        return $this->initialPanoNode;
    }


    /**
     * Set initialPanoNodeLon
     *
     * @param float $initialPanoNodeLon
     *
     * @return Pano
     */
    public function setInitialPanoNodeLon($initialPanoNodeLon)
    {
        $this->initialPanoNodeLon = $initialPanoNodeLon;

        return $this;
    }

    /**
     * Get initialPanoNodeLon
     *
     * @return float
     */
    public function getInitialPanoNodeLon()
    {
        return $this->initialPanoNodeLon;
    }

    /**
     * Set initialPanoNodeLat
     *
     * @param float $initialPanoNodeLat
     *
     * @return Pano
     */
    public function setInitialPanoNodeLat($initialPanoNodeLat)
    {
        $this->initialPanoNodeLat = $initialPanoNodeLat;

        return $this;
    }

    /**
     * Get initialPanoNodeLat
     *
     * @return float
     */
    public function getInitialPanoNodeLat()
    {
        return $this->initialPanoNodeLat;
    }


    /**
     * Set thumbnail
     *
     * @param \DisismyWebBundle\Entity\Image $thumbnail
     *
     * @return Pano
     */
    public function setThumbnail(\DisismyWebBundle\Entity\Image $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return \DisismyWebBundle\Entity\Image
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }
}
