<?php

namespace DisismyWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class RoomType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add( 'name_en',
                    TextType::class,
                    array(
                        'label' => "entities.name_en",
                        'required' => false
            ))
            ->add( 'name_es',
                    TextType::class,
                    array(
                        'label' => "entities.name_es",
                        'required' => false
            ))
            ->add( 'description_en',
                    TextareaType::class,
                    array(
                        'label' => "entities.description_en",
                        'required' => false
            ))
            ->add( 'description_es',
                    TextareaType::class,
                    array(
                        'label' => "entities.description_es",
                        'required' => false
            ))
            ->add( 'utilSqMeters',
                    IntegerType::class,
                    array(
                        'label' => "entities.utilSqMeters",
                        'required' => false
            ))
            ->add( 'builtSqMeters',
                    IntegerType::class,
                    array(
                        'label' => "entities.builtSqMeters",
                        'required' => false
            ))
            // ->add('images')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DisismyWebBundle\Entity\Room'
        ));
    }
}
