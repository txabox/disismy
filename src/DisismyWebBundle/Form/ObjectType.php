<?php

namespace DisismyWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use \Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


use DisismyWebBundle\Form\ImageType;


use Symfony\Component\Form\CallbackTransformer;

class ObjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $curUser = $options[ "curUser" ];

        $builder
            ->add( 'name_en',
                    TextType::class,
                    array(
                        'label' => "entities.name",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'name_es',
                    TextType::class,
                    array(
                        'label' => "entities.name",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'description_en',
                    TextareaType::class,
                    array(
                        'label' => "entities.description",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'description_es',
                    TextareaType::class,
                    array(
                        'label' => "entities.description",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'isOnSale',
                    CheckboxType::class,
                    array(
                        'label' => "object.entity.isOnSale",
                        'required' => false,
                        'attr' => array( 'class' => 'checkbox' ),
            ))
            ->add( 'onSalePrice',
                    IntegerType::class,
                    array(
                        'label' => "object.entity.onSalePrice",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'isOnRent',
                    CheckboxType::class,
                    array(
                        'label' => "object.entity.isOnRent",
                        'required' => false,
                        'attr' => array( 'class' => 'checkbox' ),
            ))
            ->add( 'onRentPrice',
                    IntegerType::class,
                    array(
                        'label' => "object.entity.onRentPrice",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'rentingPeriodUnits',
                    IntegerType::class,
                    array(
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            // para que estén ordenadas
            ->add( 'rentingPeriod',
                    EntityType::class,
                    array(
                        'class' => 'DisismyWebBundle:TimePeriods',
                        'query_builder' => function( EntityRepository $er )
                        {
                            return $er->createQueryBuilder( 'tp' )
                            ->orderBy( 'tp.id', 'ASC' );
                        },
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'thumbnail',
                    ImageType::class,
                    array(
                        'label' => "entities.thumbnail"
            ))
            /*->add( 'viewers',
                    null,
                    array(
                        'label' => "object.entity.viewers",
                        'attr' => array( 'class' => 'form-control' ),
            ))*/
            ->add( 'isPublic',
                    CheckboxType::class,
                    array(
                        'label' => "entities.isPublic",
                        'required' => false,
                        'attr' => array( 'class' => 'checkbox' ),
            ))
            ->add( 'saveBtn',
                    SubmitType::class,
                    array(
                        'label' => "entities.save",
                        'attr' => array( 'class' => 'save btn-block btn btn-lg btn-success' ),
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults(array(
            'data_class' => 'DisismyWebBundle\Entity\Object',
            'curUser' => null,
        ));
    }
}
