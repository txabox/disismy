<?php

namespace DisismyWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use DisismyWebBundle\Form\RoomType;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class HouseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add( 'utilSqMeters',
                    IntegerType::class,
                    array(
                        'label' => "entities.utilSqMeters",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'builtSqMeters',
                    IntegerType::class,
                    array(
                        'label' => "entities.builtSqMeters",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            /*
            ->add( 'rooms', CollectionType::class, array(
                'entry_type'    => RoomType::class,
                'allow_add'     => true,
                'allow_delete'  => true,
                'by_reference'  => false
            ))
            */
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DisismyWebBundle\Entity\House'
        ));
    }
}
