<?php
// src/DisismyWebBundle/Form/RegistrationType.php

namespace DisismyWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;

use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add( 'recaptcha', EWZRecaptchaType::class, array(
            'attr'        => array(
                'options' => array(
                    'theme' => 'light',
                    'type'  => 'image',
                    'size'  => 'normal'
                )
            ),
            'mapped'      => false,
            'constraints' => array(
                new RecaptchaTrue()
            )
        ));

        /*
        $builder
            ->add(  'privacyPolicy',
                    CheckboxType::class,
                    array(
                        'label'     => 'Privacy Policy',
                        'required'  => true,
                        'mapped'    => false,
                        'attr' => array( 'class' => 'checkbox' ),
                    ))
           ->add(   'customerAgreement',
                    CheckboxType::class,
                    array(
                        'label'     => 'Customer Agreement',
                        'required'  => false,
                        'mapped' => false,
                        'attr' => array( 'class' => 'checkbox' ),
                        'constraints' => new Assert\IsTrue(
                            array(
                                'message' => 'object.entity.onSalePrice',
                                'groups' => 'Registration' )
                        )
                    ));
        */
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'disismy_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
