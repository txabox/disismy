<?php

namespace DisismyWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Vich\UploaderBundle\Form\Type\VichFileType;


class ImageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add( 'imageFile',
                    VichFileType::class,
                    array(
                        'label'         => "image.entity.file",
                        'required'      => false,
                        'allow_delete'  => true, // not mandatory, default is true
                        'download_link' => true, // not mandatory, default is true
            ))
            ->add( 'name',
                    TextType::class,
                    array(
                        'label' => "image.entity.name",
                        'required' => false,
                        'mapped' => false,
                        'attr' => array(
                            'class' => 'form-control',
                            'placeholder' => "image.entity.name" ),
            ))
            ->add( 'alt_en',
                    TextType::class,
                    array(
                        'label' => "image.entity.alt",
                        'required' => false,
                        'attr' => array(
                            'class' => 'form-control',
                            'placeholder' => "image.entity.placeholder" ),
            ))
            ->add( 'alt_es',
                    TextType::class,
                    array(
                        'label' => "image.entity.alt",
                        'required' => false,
                        'attr' => array(
                            'class' => 'form-control',
                            'placeholder' => "image.entity.placeholder" ),
            ))
            ->add( 'description_en',
                    TextareaType::class,
                    array(
                        'label' => "entities.description",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'description_es',
                    TextareaType::class,
                    array(
                        'label' => "entities.description",
                        'required' => false,
                        'attr' => array( 'class' => 'form-control' ),
            ))
            ->add( 'saveBtn',
                    SubmitType::class,
                    array(
                        'label' => "entities.upload",
                        'attr' => array( 'class' => 'save btn-block btn btn-lg btn-success' ),
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DisismyWebBundle\Entity\Image',
            'curUser' => null,
        ));
    }
}
