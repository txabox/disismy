<?php

namespace DisismyWebBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
// use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Jenssegers\Optimus\Optimus;



class GlobalsService
{

    protected $container = null;

    private $doctrine = null;
    private $em = null;
    private $request = null;
    private $encriptador = null;
    /**
     * @var TokenStorage
     */
    protected $tokenStorage;




    /*
    public $encoders = array( new JsonEncoder() );
    public $normalizers = array( new ObjectNormalizer() );
    public $serializer = new Serializer( $normalizers, $encoders );
    */
    public $optimus = null;
    public $webPath = null;
    public $imagesPath = null;
    public $IS_DEBUG_MODE = null;

    //put your code here
    public function __construct( Container $container, Registry $doctrine, EntityManager $em, RequestStack $request, TokenStorage $tokenStorage )
    {
        $this->container = $container;
        $this->doctrine = $doctrine;
        $this->em = $em;
        $this->request = $request;
        $this->tokenStorage = $tokenStorage;
        $this->optimus = new Optimus( $this->container->getParameter( 'optimusPrime' ),
                                $this->container->getParameter( 'optimusInverse' ),
                                $this->container->getParameter( 'optimusRandom' ) );

        $this->webPath = $this->container->get( 'kernel' )->getRootDir() . '/../web/';
        $this->imagesPath = $this->container->get( 'kernel' )->getRootDir() . '/../web/bundles/disismyweb/images/Users/';
        $this->IS_DEBUG_MODE = true;

        // $this->encriptador = $this->container->get('encriptador');

//        \Doctrine\Common\Util\Debug::dump( $this->container->get('encriptador') );
//        die("");
    }

    public function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function isUserLogged()
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
        {
            return array(
                "code"          => 0,
                "isOk"          => true,
                "data"          => array(
                    "msg"       => "logged",
                    "curUser"   => $this->getUser(),
                )
             );
        }
        else
        {
            return array(
                "code"      => 1,
                "isOk"      => false,
                "data"      => array(
                    "msg"   => "not_logged"
                )
             );
        }
    }

    public function isUserLoggedAndOwnsThisObject( $encodedObjectId )
    {

        $tempResponse = $this->isUserLogged();

        // el usuario no esta logueado
        if( ! $tempResponse[ "isOk" ] )
        {
            return $tempResponse;
        }
        // el usuario está logueado
        else
        {

            $curUser = $this->getUser();

            // 1. Seleccionamos el objeto
            $objectId = $this->optimus->decode( $encodedObjectId );

            $object = $this->doctrine
            ->getRepository( 'DisismyWebBundle:Object' )
            ->findOneById( $objectId );

            if( ! $object )
            {
                return array(
                    "code"      => 0,
                    "isOk"      => false,
                    "data"      => array(
                        "msg"   => "object_not_found"
                    )
                 );
            }
            // el objeto existe
            else
            {
                // 2. Miramos a ver si curUser es el owner del object
                if( $object->getOwner()->getId() != $curUser->getId() )
                {

                    return array(
                        "code"          => 1,
                        "isOk"          => false,
                        "data"          => array(
                            "msg"       => "not_object_owner"
                        )
                     );
                }
                // curUser es el dueño del objeto
                else
                {
                    return array(
                        "code"          => 2,
                        "isOk"          => true,
                        "data"          => array(
                            "msg"       => "user_logged_and_object_owner",
                            "curUser"   => $curUser,
                            "object"    => $object
                        )
                     );
                }
            }
        }
    }

    public function isUserLoggedAndOwnsThisImage( $encodedImageId )
    {

        $tempResponse = $this->isUserLogged();

        // el usuario no esta logueado
        if( ! $tempResponse[ "isOk" ] )
        {
            return $tempResponse;
        }
        // el usuario está logueado
        else
        {

            $curUser = $this->getUser();

            // 1. Seleccionamos el objeto
            $imageId = $this->optimus->decode( $encodedImageId );

            $image = $this->doctrine
            ->getRepository( 'DisismyWebBundle:Image' )
            ->findOneById( $imageId );

            if( ! $image )
            {
                return array(
                    "code"      => 0,
                    "isOk"      => false,
                    "data"      => array(
                        "msg"   => "image_not_found"
                    )
                 );
            }
            // el objeto existe
            else
            {
                // 2. Miramos a ver si curUser es el owner del image
                if( $image->getOwner()->getId() != $curUser->getId() )
                {

                    return array(
                        "code"          => 1,
                        "isOk"          => false,
                        "data"          => array(
                            "msg"       => "not_image_owner"
                        )
                     );
                }
                // curUser es el dueño del objeto
                else
                {
                    return array(
                        "code"          => 2,
                        "isOk"          => true,
                        "data"          => array(
                            "msg"       => "user_logged_and_image_owner",
                            "curUser"   => $curUser,
                            "image"    => $image
                        )
                     );
                }
            }
        }
    }


    public function isUserLoggedAndOwnsThisFolder( $encodedFolderId )
    {

        $tempResponse = $this->isUserLogged();

        // el usuario no esta logueado
        if( ! $tempResponse[ "isOk" ] )
        {
            return $tempResponse;
        }
        // el usuario está logueado
        else
        {

            $curUser = $this->getUser();

            // 1. Seleccionamos el objeto
            $folderId = $this->optimus->decode( $encodedFolderId );

            $folder = $this->doctrine
            ->getRepository( 'DisismyWebBundle:Folder' )
            ->findOneById( $folderId );

            if( ! $folder )
            {
                return array(
                    "code"      => 0,
                    "isOk"      => false,
                    "data"      => array(
                        "msg"   => "folder_not_found"
                    )
                 );
            }
            // el objeto existe
            else
            {
                // 2. Miramos a ver si curUser es el owner del folder
                if( $folder->getOwner()->getId() != $curUser->getId() )
                {

                    return array(
                        "code"          => 1,
                        "isOk"          => false,
                        "data"          => array(
                            "msg"       => "not_folder_owner"
                        )
                     );
                }
                // curUser es el dueño del objeto
                else
                {
                    return array(
                        "code"          => 2,
                        "isOk"          => true,
                        "data"          => array(
                            "msg"       => "user_logged_and_folder_owner",
                            "curUser"   => $curUser,
                            "folder"    => $folder
                        )
                     );
                }
            }
        }
    }


}
