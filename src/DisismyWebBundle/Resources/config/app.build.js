// http://renderedtext.com/blog/2013/01/23/compile-requirejs-project-to-single-file/
// https://github.com/requirejs/r.js/blob/master/build/example.build.js
({
    baseUrl: "../public/",
    mainConfigFile: '../public/js/main.js',
    optimize: 'uglify2',
    uglify2: {
        mangle: true
    },
    findNestedDependencies: true,
    inlineText : true,
    stubModules: [ 'text' ],
    optimizeAllPluginResources: true,
    /* Specifying the out parameter tells r.js that you want everything in one file. The alternative is specifying dir in which case the contents of your app folder are copied into that dir.
    dir: "../appdirectory-build",
    */
    /* A few options like appDir, dir and modules are incompatible with out aka compiling to a single file so don’t use those.
    appDir: "../public/",
    modules: [
        {
            name: "main"
        }
    ],*/
    /*
    Usually, you would use the name parameter to specify your main module but we’re using include here because we’re bundling the requirejs loader as well.
    name: 'main',
    The include parameter is an array specifying which other modules to include in the build. When we specify the “main” one, r.js traces all other modules that main depends on and includes them
    */
    // name: 'js/requirejs/main',
    include: [ 'libs/npm/node_modules/requirejs/require', 'js/main' ],
    /*
    Wrap, unsurprisingly wraps module requires into a closure so that only what you export gets into the global environment. To be honest, I don’t really need this but if you’re bundling a widget or something someone will use with a lot of other stuff I guess it’s a good idea.
    */
    wrap: true,
    out: "../public/js/main.min.js",

})
