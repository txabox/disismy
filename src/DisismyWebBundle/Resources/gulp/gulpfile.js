const gulp = require( 'gulp' ),
    argv = require('yargs').argv,
    runSequence = require( 'run-sequence' ),
    del = require( 'del' );
const CONFIG = argv.production ? require( './config/production.json' ) : require( './config/development.json' );

gulp.task( 'default', function( cb_ )
{
    console.log( "ENVIRONMENT: " + CONFIG.ENVIRONMENT );

    if( argv.production )
    {
        runSequence(
                'cleanDevJs',
                cb_ );
    }

});

gulp.task( 'cleanDevJs', function( cb )
{
    if( argv.production )
    {

        del([ '../public/js/**/*', '!../public/js/main.min.js' ], { force: true } )
        .then( function( paths )
        {
            console.log( 'Deleted files and folders:\n', paths.join( '\n' ) );
        });

    }
});
