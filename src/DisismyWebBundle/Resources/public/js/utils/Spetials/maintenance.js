$(function()
{
    $( "#indexCarousel" ).swiperight( function()
    {
        $( this ).carousel( 'prev' );
    });

    $( "#indexCarousel" ).swipeleft( function()
    {
        $( this ).carousel( 'next' );
    });

});
