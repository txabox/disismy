"use strict";

define( [ 'jquery' ], function ( $ )
{

    // Vertical centered modals
    // you can give custom class like this // var modalVerticalCenterClass = ".modal.modal-vcenter";
    var modalVerticalCenterClass = ".centeredModal";
    function centerModals( $element )
    {
        var $modals;
        if( $element.length )
        {
            $modals = $element;
        }
        else
        {
            $modals = $( modalVerticalCenterClass + ':visible' );
        }
        $modals.each( function(i)
        {
            var $clone = $( this ).clone().css( 'display', 'block' ).appendTo( 'body' );
            var top = Math.round( ( $clone.height() - $clone.find( '.modal-content' ).height() ) / 2 );
            top = top > 0 ? top : 0;
            $clone.remove();
            $( this ).find( '.modal-content' ).css( "margin-top", top );
        });
    }
    $( modalVerticalCenterClass ).on( 'show.bs.modal', function( e )
    {
        centerModals( $( this ) );
    });

    $( window ).on( 'resize', centerModals );







    // permite abrir multiples modales
    $( document ).on( 'show.bs.modal', '.modal', function ()
    {
        var zIndex = 1040 + ( 10 * $( '.modal:visible' ).length );
        $( this ).css( 'z-index', zIndex );
        setTimeout( function()
        {
            $( '.modal-backdrop' ).not( '.modal-stack' ).css( 'z-index', zIndex - 1 ).addClass( 'modal-stack' );
        }, 0);
    });



    // para evitar que el fondo scrollee con el modal:
    $('body').on('touchmove', function(e){
    if($('.scroll-disable').has($(e.target)).length) e.preventDefault();
    });
    $('body').on('shown.bs.modal', function(){
    $(this).addClass('scroll-disable');
    });
    $('body').on('hidden.bs.modal', function(){
    $(this).removeClass('scroll-disable');
    });
    
});
