"use strict";

define( [
    'jquery',
    'backbone',
    'ShowIndexView',
    'ShowUserView',
    'ShowGalleryView',
    'ShowObjectView',
    'EditObjectView',
], function ( $, Backbone, IndexView, ShowUserView, GalleryView, ShowObjectView,
EditObjectView )
{

    var cssNames = [];
    var curView = null;

    function getParameterByNameFromQueryString( name, url )
    {
        if( ! url ) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");

        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function requiresToBeLogged( cb )
    {

        $.ajax({
            method: "GET",
            url: Disismy.Symfony.BASE_URL + "/isLogged/"
        })
        .done( function( data, textStatus, jqXHR )
        {
            if( data.isOk )
            {
                cb();
            }
            else
            {
                window.location = Disismy.Symfony.BASE_URL + "/login";
            }
        })
        .fail( function( jqXHR, textStatus, errorThrown )
        {
            var errorMsg = "Data not retreived: Unknown error. Please, check your browser console error log and try again later.";
            console.error( errorMsg, jqXHR, textStatus, errorThrown );
            alert( errorMsg );
        });
    }

    var Router = Backbone.Router.extend({
        routes:
        {
            "": "index",
            "profile/:encodedUserId(/)": "showUser",
            ":objectType/:encodedObjectId(/)": "showObject",
            ":objectType/gallery/:galleryOwnerEncodedId(/)": "showGallery",
            "edit/:objectType/:encodedObjectId(/)": "editObject",
        },
        initialize: function( options )
        {
            var self = this;

            this.on( "route", function( route, params )
            {
                // console.log( "onRoute: route: ", route, ", params: ", params );
                var isEmbeded = getParameterByNameFromQueryString( 'isEmbeded' );
                isEmbeded = ( true == isEmbeded );
                if( isEmbeded )
                {
                    $( 'nav' ).hide();
                    $( 'footer' ).hide();
                    $( '#mainContentContainer' ).css( 'padding-top', '0px' );
                }
                else
                {
                    $( 'nav' ).show( 300 );
                    $( 'footer' ).show( 300 );
                    $( '#mainContentContainer' ).css( 'padding-top', '50px' );
                }
            });

        },

        index: function()
        {
            console.log( 'Router: index' );
            curView && curView.unload();
            curView = new IndexView({
                'parentContainerId': '#mainContentContainer',
            });
        },

        showUser: function( encodedUserId )
        {
            console.log( 'Router: showUser' );
            curView && curView.unload();
            curView = new ShowUserView({
                'parentContainerId': '#mainContentContainer',
                'encodedUserId': encodedUserId
            });
        },

        showObject: function( objectType, encodedObjectId )
        {
            console.log( 'Router: showObject' );
            curView && curView.unload();
            curView = new ShowObjectView({
                'parentContainerId': '#mainContentContainer',
                'objectType': objectType,
                'encodedObjectId': encodedObjectId,
                'pano': getParameterByNameFromQueryString( 'pano' ),
                'isEmbeded': getParameterByNameFromQueryString( 'isEmbeded' ),
            });
        },

        showGallery: function( objectType, galleryOwnerEncodedId )
        {
            console.log( 'Router: gallery' );
            curView && curView.unload();
            curView = new GalleryView({
                'parentContainerId': '#mainContentContainer',
                'objectType': objectType,
                'galleryOwnerEncodedId': galleryOwnerEncodedId,
            });
        },


        editObject: function( objectType, encodedObjectId )
        {
            requiresToBeLogged( function()
            {
                console.log( 'Router: editObject' );
                curView && curView.unload();
                curView = new EditObjectView({
                    'parentContainerId': '#mainContentContainer',
                    'objectType': objectType,
                    'encodedObjectId': encodedObjectId,
                });
            });
        },

    });

    return Router;

});
