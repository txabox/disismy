"use strict";

define( [
    'jquery',
    'backbone',
    'ObjectModel',
], function ( $, Backbone, ObjectModel )
{

    var ObjectsCollection =  Backbone.Collection.extend({
            url: '/objects/',
            model: ObjectModel,
            initialize: function( options )
            {
                this._meta = {};
                this.meta( "ownerEncodedId", options.ownerEncodedId );
                // console.log( "A new ObjectCollection has been initialized. ", options, this, this.model );
            },
            meta: function( prop, value )
            {
                if( value === undefined )
                {
                    return this._meta[ prop ]
                }
                else
                {
                    this._meta[ prop ] = value;
                }
            },
            parse: function( response )
            {
                var self = this;
                self.meta( 'isOwnerCurUser', response.data.isOwnerCurUser );
                self.meta( 'owner', response.data.owner );

                return response.data.objects;
            }
        });




    return ObjectsCollection;

});
