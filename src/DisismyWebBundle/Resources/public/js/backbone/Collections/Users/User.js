"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'UserModel',
], function ( $, _, Backbone, UserModel )
{

    var UsersCollection =  Backbone.Collection.extend({
            url: '/users/',
            model: UserModel,
            initialize: function( options )
            {
                this._meta = {};
                // console.log( "A new UserCollection has been initialized. ", options, this, this.model );
            },
            meta: function( prop, value )
            {
                if( value === undefined )
                {
                    return this._meta[ prop ]
                }
                else
                {
                    this._meta[ prop ] = value;
                }
            },
            parse: function( response )
            {
                var self = this;

                return response.data.users;
            },

            getNamesArray: function()
            {
                var self = this;
                var namesArray = [];

                _.each( self.models, function( user )
                {
                    namesArray.push( user.get( 'name' ) );
                });

                return namesArray;
            }
        });




    return UsersCollection;

});
