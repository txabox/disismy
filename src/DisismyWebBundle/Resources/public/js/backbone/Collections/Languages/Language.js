"use strict";

define( [
    'jquery',
    'backbone',
    'LanguageModel',
], function ( $, Backbone, LanguageModel )
{

    var LanguagesCollection =  Backbone.Collection.extend({
            url: '/languages/',
            model: LanguageModel,
            initialize: function( options )
            {
                this._meta = {};
                // console.log( "A new LanguageCollection has been initialized. ", options, this, this.model );
            },
            meta: function( prop, value )
            {
                if( value === undefined )
                {
                    return this._meta[ prop ]
                }
                else
                {
                    this._meta[ prop ] = value;
                }
            },
            parse: function( response )
            {
                var self = this;

                return response.data.languages;
            }
        });




    return LanguagesCollection;

});
