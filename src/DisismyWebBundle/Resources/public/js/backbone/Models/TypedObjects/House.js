"use strict";

define( [
    'jquery',
    'backbone'
], function ( $, Backbone )
{
    /*
    ejemplo de variables y funciones privadas
    var privateVar = "privateVar";
    function privateFunction(arg)
    {
        console.log("Esto es una funcion privada cuyo argumento es: ", arg);
    }
    */
    var HouseModel = Backbone.Model.extend({
            defaults:{
                "id"            : -1,
                "encodedId"     : -1,
            },

            url: function()
            {
                var self = this;
                return "house/" + self.get( 'encodedId' ) + "/";
            },

            parse: function( response, options )
            {
                var self = this;
                if( response.data )
                {
                    return response.data.house;
                }
                else
                {
                    return response;
                }
            },

            initialize: function( options )
            {
                this.on( 'change:encodedId', this.setId );
                if( options.encodedId )
                {
                    this.set({ 'encodedId': options.encodedId });
                }
                this.set({ id: this.get( "encodedId" ) });

                // console.log( "A new HouseModel has been initialized", this );
            },

            setId: function()
            {
                this.set({ id: this.get( "encodedId" ) });
            },

            // para sacar el mensaje de error
            // myHouse.on('invalid', function (model, error) {
            // console.log(error);
            //});
            validate: function( attributes )
            {
                if ( attributes.encodedId < 0 )
                {
                    return 'Invalid encodedId.';
                }
            },

            render: function()
            {
            }

        });

    return HouseModel;

});
