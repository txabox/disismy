"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'HouseModel',
    'LanguagesCollection',
    'UsersCollection',
], function ( $, _, Backbone, HouseModel, LanguagesCollection, UsersCollection )
{
    /*
    ejemplo de variables y funciones privadas
    var privateVar = "privateVar";
    function privateFunction(arg)
    {
        console.log("Esto es una funcion privada cuyo argumento es: ", arg);
    }
    */
    var ObjectModel = Backbone.Model.extend({
            defaults:{
                "id"            : -1,
                "encodedId"     : -1,
                "ownerEncodedId": -1,
                "type"          : "",
                "typedObject"   : "",
                "name"          : "",
                "desc"          : "",
                "isOnSale"      : false,
                "isOnRent"      : false,
                "thumbId"       : -1,
                "isPublic"      : false,
                "panos"         : [],
                "viewers"       : undefined,
            },

            url: function()
            {
                var self = this;
                return "object/" + self.get( 'encodedId' ) + "/";
            },

            parse: function( response, options )
            {
                var self = this;
                if( response.data )
                {
                    var newObj = response.data.object;
                    if( !newObj )
                    {
                        console.error( "Server is not sending a propper object. data: ", response.data );
                        alert( "Server is not sending a propper object" );
                        return response;
                    }

                    // transformamos el typedObject de un json a un backbone model
                    if( newObj.type.name === "house" )
                    {
                        if( newObj.typedObject )
                        {
                            newObj.typedObject = new HouseModel( newObj.typedObject );
                        }
                        else
                        {
                            // TODO: Aqui tendríamos que llamar por ajax
                            // TODO: ACTUALMENTE ESTÁ MAL!!!!!!!!!!!!!!!
                            debugger;
                        }
                    }
                    else
                    {
                        alert( "wrong_object_type" );
                    }

                    // creamos los idiomas del objeto como una collección
                    var languages = new LanguagesCollection();
                    languages.reset( response.data.object.languages );
                    newObj.languages = languages;

                    // creamos los viewers del objeto como una collección de Users
                    var viewers = new UsersCollection();
                    viewers.reset( response.data.object.viewers );
                    newObj.viewers = viewers;

                    return newObj;
                }
                else
                {
                    console.warn( "Está bien que llegue aquí? Estudiar la razón de por qué llegas aquí" );
                    return response;
                }
            },

            initialize: function( options )
            {

                this.on( 'change:encodedId', this.setId );
                if( options.encodedId )
                {
                    this.set({ 'encodedId': options.encodedId });
                }
                this.set({ id: this.get( "encodedId" ) });

                // public
                _.bindAll( this, 'addViewer', 'removeViewer' );

                this.bindEvents();
                // console.log( "A new ObjectModel has been initialized", this );

            },

            bindEvents: function()
            {
                // this.on( 'change:isPublic', this.isPublicChanged );
            },

            setId: function()
            {
                this.set({ id: this.get( "encodedId" ) });
            },

            // para sacar el mensaje de error
            // myObject.on('invalid', function (model, error) {
            // console.log(error);
            //});
            validate: function( attributes )
            {
                if ( attributes.encodedId < 0 )
                {
                    return 'Invalid encodedId.';
                }
            },

            render: function()
            {
            },

            addLanguage: function( lang, cb )
            {
                $.ajax({
                    method: "PATCH",
                    url: Disismy.Symfony.BASE_URL + "/add/object/" + this.get( 'encodedId' ) + "/language/" + lang.get( 'encodedId' ) + "/",
                    context: this
                })
                .done( function( data )
                {
                    if( data.isOk )
                    {
                        // TODO: Mirar a ver si la colleción de languages recibe
                        // esta modificacion
                        // debugger;
                        cb && cb();
                    }
                    else
                    {
                        $.notify( "Data not saved: " + data.data.msg + "." );
                    }
                })
                .fail( function( data )
                {
                    $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
                });
            },

            removeLanguage: function( lang, cb )
            {
                debugger;
                $.ajax({
                    method: "PATCH",
                    url: Disismy.Symfony.BASE_URL + "/remove/object/" + this.get( 'encodedId' ) + "/language/" + lang.get( 'encodedId' ) + "/"
                })
                .done( function( data )
                {
                    if( data.isOk )
                    {

                        cb && cb();

                    }
                    else
                    {
                        $.notify( "Data not saved: " + data.data.msg + "." );
                    }
                })
                .fail( function( data )
                {
                    $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
                });
            },

            addViewer: function( viewerName, options )
            {
                var self = this;

                $.ajax({
                    method: "PATCH",
                    url: Disismy.Symfony.BASE_URL + "/add/object/" + self.get( 'encodedId' ) + "/viewer/" + viewerName + "/",
                    context: self
                })
                .done( function( data )
                {
                    if( data.isOk )
                    {
                        this.get( 'viewers' ).add( data.data.viewer );
                        options.success && options.success( self );
                    }
                    else
                    {
                        console.warn( "Data not saved: " + data.data.msg + "." );
                        options.error && options.error( self );
                    }
                })
                .fail( function( data )
                {
                    $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
                });
            },

            removeViewer: function( viewer, cb )
            {
                var self = this;

                $.ajax({
                    method: "PATCH",
                    url: Disismy.Symfony.BASE_URL + "/remove/object/" + self.get( 'encodedId' ) + "/viewer/" + viewer.get( 'encodedId' ) + "/"
                })
                .done( function( data )
                {
                    if( data.isOk )
                    {
                        cb && cb();
                    }
                    else
                    {
                        $.notify( "Data not saved: " + data.data.msg + "." );
                    }
                })
                .fail( function( data )
                {
                    $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
                });


            },

        });

    return ObjectModel;

});
