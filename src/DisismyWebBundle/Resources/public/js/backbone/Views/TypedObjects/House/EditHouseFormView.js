"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'text!templates/TypedObjects/House/editHouseFormTmpl.html',
    'ObjectModel',
    'LanguagesCollection'
], function ( $, _, Backbone, editHouseFormTmpl, ObjectModel, LanguagesCollection )
{
    var house, parentContainerId;
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var EditHouseFormView =  Backbone.View.extend({
        template:   _.template( editHouseFormTmpl ),

        initialize: function( options )
        {
            var self = this;
            self._views = [];

            // private
            _.bindAll( this, '_onInitialized' );
            // public
            _.bindAll( this, 'render', 'postRender', 'unload' );

            $( 'body' ).addClass( 'editHouseFormCss' );

            house = options.typedObject;
            parentContainerId = options.parentContainerId;
            self._parent = options.parent;



            self._onInitialized( options.cb );


        },

        _onInitialized: function( cb )
        {
            // decimos al padre que este elemento se ha inicializado correctamente
            cb && cb( this );
        },

        events:
        {
        },

        render: function()
        {

            var self = this;

            if( this.$el.html() === "" )
            {
                // hacer esto permite separar la logica del init al render
                // y poder meter un div contenedor en el template padre.
                self.setElement( $( parentContainerId ) );
            }

            // primero renderizamos la vista del padre
            this.$el.append(
                this.template({
                    'house': house,
                })
            );

            // y luego renderizamos las vista de sus hijos

            _.each( self._views, function( view )
            {
                // por ahora sacamos al PanoView del renderizado en forma de arbol
                // porque no todos sus elementos son views jerarquizados
                if( view.name != 'PanoView' )
                {
                    view.render();
                }
            });

            return this;
        },

        postRender: function()
        {

            var self = this;

            // y luego postRenderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                view.postRender && view.postRender();
            });

            return self;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });

            $( 'body' ).removeClass( 'editHouseFormCss' );

            this.$el.empty();

            this.undelegateEvents();
        },

    });

    return EditHouseFormView;

});
