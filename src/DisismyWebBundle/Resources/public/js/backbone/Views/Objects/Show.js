"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'text!templates/Objects/showObjectTmpl.html',
    'ObjectModel',
    'PanoView',
    'SharingModView'
], function ( $, _, Backbone, showObjectTmpl, ObjectModel, PanoView, SharingModView )
{
    var objectType, visiblePanoEncodedId, isEmbeded, parentContainerId;
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var ShowObjectView =  Backbone.View.extend({
        template:   _.template( showObjectTmpl ),

        initialize: function( options )
        {
            var self = this;
            self._views = [];


            _.bindAll( this, '_onInitialized', '_getModel', '_createPanoView',
            '_createSharingModView' );

            _.bindAll( this, 'render' );

            $( 'body' ).addClass( 'showObjectCss' );

            objectType = options.objectType;
            visiblePanoEncodedId = options.pano;
            isEmbeded = ( true == options.isEmbeded );
            parentContainerId = options.parentContainerId;

            self._getModel( options.encodedObjectId, function( model )
            {
                self._createPanoView( model, function( panoView )
                {
                    self._createSharingModView( model, panoView, function()
                    {
                        self._onInitialized();
                    });
                });
            });


        },

        _onInitialized: function()
        {
            var self = this;

            self.render();

            self.postRender();
        },

        _getModel: function( encodedObjectId, cb )
        {
            var self = this;

            self.model = new ObjectModel({ encodedId: encodedObjectId });

            self.model.fetch({
                success: function( model, response, options_ )
                {
                    if( response.isOk )
                    {
                        cb( model );
                    }
                    else
                    {
                        console.error( "Error fetching objects model: ", model, response, options_ );
                        alert( response.data.msg );
                    }
                },
                error: function( model, response, options_ )
                {
                    console.error( "Error fetching objects model: ", model, response, options_ );
                }
            });
        },

        _createPanoView: function( model, cb )
        {

            var self = this;

            // si el modelo ( objeto ) tiene pano, creamos su view
            if( model.get( 'panos' ).length > 0 )
            {
                if( visiblePanoEncodedId )
                {
                    Disismy.Pano.view = new PanoView({
                        'encodedPanoId': visiblePanoEncodedId,
                        'parent': self,
                        'parentContainerId': "#mainContentContainer",
                        'isEmbeded': isEmbeded,
                        'cb': function( panoView )
                        {
                            // añadimos esta nueva vista al array de vistas de este view
                            self._views.push( panoView );
                            cb( panoView );
                        }
                    });
                }
                else
                {
                    Disismy.Pano.view = new PanoView({
                        'encodedPanoId': model.get( 'panos' )[ 0 ].encodedId,
                        'parent': self,
                        'parentContainerId': "#mainContentContainer",
                        'isEmbeded': isEmbeded,
                        'cb': function( panoView )
                        {
                            // añadimos esta nueva vista al array de vistas de este view
                            self._views.push( panoView );

                            cb( panoView );
                        }
                    });
                }

            }
            else
            {
                cb();
            }

        },

        _createSharingModView: function( model, panoView, cb )
        {

            var self = this;
            var sharingModView = new SharingModView({
                'parent':   self,
                'parentContainerId': '#sharingMod',
                'model':    model,
                'panoView': panoView,
                'cb': function( sharingModView_ )
                {
                    self._views.push( sharingModView_ );

                    cb();
                }
            });
        },

        events:
        {
        },

        render: function()
        {

            if( isEmbeded ){ return this; }

            var self = this;

            if( this.$el.html() === "" )
            {
                // hacer esto permite separar la logica del init al render
                // y poder meter un div contenedor en el template padre.
                self.setElement( $( parentContainerId ) );
            }

            // primero renderizamos la vista del padre
            this.$el.append(
                this.template({
                    'objectType': objectType,
                    'object': self.model,
                    'pano': Disismy.Pano.view.model,
                })
            );

            // y luego renderizamos las vista de sus hijos

            _.each( self._views, function( view )
            {
                // por ahora sacamos al PanoView del renderizado en forma de arbol
                // porque no todos sus elementos son views jerarquizados
                if( view.name != 'PanoView' )
                {
                    view.render();
                }
            });

            return this;
        },

        postRender: function()
        {

            if( isEmbeded ){ return this;}

            var self = this;

            // y luego postRenderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                view.postRender && view.postRender();
            });

            return self;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });

            $( 'body' ).removeClass( 'showObjectCss' );

            this.$el.empty();

            this.undelegateEvents();
        },

    });

    return ShowObjectView;

});
