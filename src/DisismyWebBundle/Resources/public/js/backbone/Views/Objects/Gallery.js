"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'text!templates/Objects/galleryTmpl.html',
    'ObjectsCollection'
], function ( $, _, Backbone, galleryTmpl, ObjectsCollection )
{
    var objectType, parentContainerId;
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var GalleryView =  Backbone.View.extend({

        template:   _.template( galleryTmpl ),

        initialize: function( options )
        {
            var self = this;
            this._views = [];

            $( 'body' ).addClass( 'galleryCss' );

            objectType = options.objectType;
            parentContainerId = options.parentContainerId;


            this.collection = new ObjectsCollection({ ownerEncodedId: options.galleryOwnerEncodedId });
            this.collection.fetch({
                data:
                {
                    ownerEncodedId: this.collection.meta( "ownerEncodedId" )
                },
                success: function( collection, response, options_ )
                {
                    if( response.isOk )
                    {
                        self.render();
                    }
                    else
                    {
                        console.error( "Error fetching objects collection: ", collection, response, options_ );
                        alert( response.data.msg );
                    }
                },
                error: function( collection, response, options_ )
                {
                    console.error( "Error fetching objects collection: ", collection, response, options_ );
                }
            });

        },

        events:
        {
        },

        render: function()
        {
            var self = this;

            if( this.$el.html() === "" )
            {
                // hacer esto permite separar la logica del init al render
                // y poder meter un div contenedor en el template padre.
                self.setElement( $( parentContainerId ) );
            }

            self.$el.append(
                self.template({
                    'objectType': objectType,
                    'isOwnerCurUser': self.collection.meta( 'isOwnerCurUser' ),
                    'galleryOwner': self.collection.meta( 'owner' ),
                    'objects': self.collection
                })
            );

            return self;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });

            $( 'body' ).removeClass( 'galleryCss' );

            this.$el.empty();

            this.undelegateEvents();
        },

    });

    return GalleryView;

});
