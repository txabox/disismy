"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'text!templates/Objects/editObjectTmpl.html',
    'ObjectModel',
    'LanguagesCollection',
    'EditHouseFormView',
    'UserModel',
    'UsersCollection',
    'ImageManagerView',
    'jquery.tag-editor'
], function ( $, _, Backbone, editObjectTmpl, ObjectModel, LanguagesCollection,
    EditHouseFormView, UserModel, UsersCollection, ImageManagerView )
{
    var objectType, parentContainerId;
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var EditObjectView =  Backbone.View.extend({
        template:   _.template( editObjectTmpl ),

        initialize: function( options )
        {
            var self = this;
            self._views = [];

            // private
            _.bindAll( this, '_onInitialized', '_getLanguages', '_getObject',
            '_createTypedObjectView', '_initializeTagManager', '_onFlagBtnClicked',
            '_setBoolProperty', '_setIntProperty', '_setTextProperty', '_showImageManager',
            '_thumbnailChanged' );
            // public
            _.bindAll( this, 'render' );

            $( 'body' ).addClass( 'editObjectCss' );

            objectType = options.objectType;
            parentContainerId = options.parentContainerId;



            self._getLanguages( function()
            {
                self._getObject( options.encodedObjectId, function()
                {
                    self._createTypedObjectView( function()
                    {
                        self.imageManagerView = new ImageManagerView({
                            parent: self,
                            cb: function( imageManagerView )
                            {
                                self._views.push( imageManagerView );
                                self._onInitialized();
                            }
                        });
                    });
                });
            });


        },

        _onInitialized: function()
        {
            var self = this;
            self.render();
            self.postRender();
        },

        _getLanguages: function( cb )
        {
            var self = this;
            self._languages = new LanguagesCollection();
            self._languages.fetch({
                success: function( models, response, options_ )
                {
                    if( response.isOk )
                    {
                        cb();
                    }
                    else
                    {
                        console.error( "Error fetching languages: ", models, response, options_ );
                        alert( response.data.msg );
                    }
                },
                error: function( models, response, options_ )
                {
                    console.error( "Error fetching languages: ", models, response, options_ );
                }
            });
        },

        _getObject: function( encodedObjectId, cb )
        {
            var self = this;

            self.model = new ObjectModel({ encodedId: encodedObjectId });

            self.model.fetch({
                success: function( model, response, options_ )
                {
                    if( response.isOk )
                    {
                        cb();
                    }
                    else
                    {
                        console.error( "Error fetching objects model: ", model, response, options_ );
                        alert( response.data.msg );
                    }
                },
                error: function( model, response, options_ )
                {
                    console.error( "Error fetching objects model: ", model, response, options_ );
                }
            });
        },

        _createTypedObjectView: function( cb )
        {
            var self = this;

            if( self.model.get( 'type' ).name === "house" )
            {
                new EditHouseFormView({
                    'parentContainerId':  '#typedObjectProperties',
                    'typedObject':  self.model.get( 'typedObject' ),
                    'parent':       self,
                    'cb': function( editHouseFormView )
                    {
                        self._views.push( editHouseFormView );
                        cb();
                    }
                });
            }
            else
            {
                console.error( "wrong_object_type" );
                alert( "wrong_object_type" );
            }
        },

        _initializeTagManager: function( cb )
        {
            var self = this;

            self.$el.find( '#viewersTagContainer' ).tagEditor(
            {
                autocomplete: {
                    delay: 0, // show suggestions immediately
                    position: { collision: 'flip' }, // automatic menu position up/down
                    source: Disismy.Symfony.BASE_URL + "/get/public/users/names/"
                },
                initialTags: self.model.get( 'viewers' ).getNamesArray(),
                placeholder: Translator.trans( "object.entity.viewers_placeholder" ),
                onChange: function( field, editor, tags ){},
                beforeTagSave: function( field, editor, tags, tag, val )
                {
                    // por alguna estupida razón, cuando pones una coma (","),
                    // trata de enviar un string vacío.
                    if( val === "" )
                    {
                        return false;
                    }

                    self.model.addViewer( val,
                    {
                        success: function( model )
                        {
                        },
                        error: function( model )
                        {
                            $( 'li', editor ).each( function()
                            {
                                var $li = $( this );
                                if( $li.find( '.tag-editor-tag' ).html() === val )
                                {
                                    $li.addClass( 'red-tag' );
                                }
                            });
                        }
                    });
                },
                beforeTagDelete: function( field, editor, tags, val )
                {
                    var viewerToDelete = self.model.get( 'viewers' ).findWhere({ name: val });
                    // si el usuario existe ( puede no existir si es un viewer en rojo )
                    if( viewerToDelete )
                    {
                        self.model.removeViewer( viewerToDelete, function()
                        {
                            self.model.get( 'viewers' ).remove( viewerToDelete );
                        });
                    }
                }
            });

            cb && cb();

        },

        events:
        {
            'click .flag': "_onFlagBtnClicked",
            'change .boolModifier': "_setBoolProperty",
            'change .intModifier': "_setIntProperty",
            'change .textModifier': "_setTextProperty",
            'click #openImageManagerBtn': '_showImageManager'
        },

        _showImageManager: function( e )
        {
            var self = this;
            self.imageManagerView.show( onImageManagerClosed );

            function onImageManagerClosed( selectedObjects )
            {
                if( selectedObjects.imageViews.length > 1 )
                {
                    // TODO: Traducir esto y ponerlo en un modal chulo
                    alert( "No puede haber más de 1 imagen seleccionada" );
                }
                else
                {
                    self.imageManagerView.hide();
                    if( selectedObjects.imageViews.length === 1 )
                    {
                        // debugger;
                        self.$el.find( "#thumbImgLoading" ).removeClass( 'hidden' );

                        var newThumbId = selectedObjects.imageViews[ 0 ].model.get( 'encodedId' );

                        var data = {};
                        data[ "thumbId" ] = newThumbId;
                        self.model.save(
                            data,
                            {
                                patch: true,
                                wait: true,
                                success: self._thumbnailChanged,
                                error: function()
                                {
                                    alert( "Error saving the new thumbnail value in DDBB" );
                                }
                            }
                        )
                    }
                }
            }

        },

        _thumbnailChanged: function( model, response, options )
        {
            var self = this;
            this.$el.find( "#thumbImg" )
            .load( function()
            {
                // loaded!
                self.$el.find( "#thumbImgLoading" ).addClass( 'hidden' );
            })
            .attr( "src", Disismy.Symfony.BASE_URL + "/rawImage/" + model.get( "thumbId" ) );
        },

        _onFlagBtnClicked: function( e )
        {

            var self = this;

            // por alguna razón que no entiend, e.target me da la imagen en
            // vez del botón, que es el que tiene el listener al evento click
            var $buttonImg = $( e.target );
            var $btn = $buttonImg.parent();
            var curLang = self._languages.get( $btn.data( 'encodedlanguageid' ) );

            if( $buttonImg.hasClass( "desaturate" ) )
            {
                // console.log( "add " + curLang.get( 'name' ) + " language" );
                self.model.addLanguage( curLang, function()
                {
                    // console.log( curLang.get( 'name' ) + " language added" );
                    $buttonImg.removeClass( "desaturate" );
                    self.$el.find( "." + curLang.get( 'name' ) + "Content" ).slideDown( 300 );
                });
            }
            else
            {
                // console.log( "remove " + curLang.get( 'name' ) + " language" );
                self.model.removeLanguage( curLang, function()
                {
                    // console.log( curLang.get( 'name' ) + " language removed" );
                    $buttonImg.addClass( "desaturate" );
                    self.$el.find( "." + curLang.get( 'name' ) + "Content" ).slideUp( 300 );
                });
            }
        },

        _setBoolProperty: function( e )
        {
            var self = this;
            var newVal = $( e.target ).is( ":checked" );
            var propertyName = e.target.id.replace( 'object_', '' );
            var $propertyContent = self.$el.find( "#" + propertyName + "ContentRow" );

            var data = {};
            data[ propertyName ] = newVal;

            this.model.save(
                data,
                {
                    patch: true,
                    wait: true,
                    success: function( model, response, options )
                    {
                        if( $propertyContent.is( ":visible" ) )
                        {
                            $propertyContent.fadeOut( 300 );
                        }
                        else
                        {
                            $propertyContent.fadeIn( 300 );
                        }
                    },
                    error: function()
                    {
                        alert( "Error saving boolean value in DDBB" );
                    }
                }
            )
        },

        _setIntProperty: function( e )
        {
            var self = this;
            var newVal = $( e.target ).val();
            var propertyName = e.target.id.replace( 'object_', '' );

            var data = {};
            data[ propertyName ] = newVal;

            this.model.save(
                data,
                {
                    patch: true,
                    wait: true,
                    success: function( model, response, options )
                    {
                    },
                    error: function()
                    {
                        alert( "Error saving integer value in DDBB" );
                    }
                }
            )
        },

        _setTextProperty: function( e )
        {
            var self = this;
            var newVal = $( e.target ).val();
            var propertyName = e.target.id.replace( 'object_', '' );

            var data = {};
            data[ propertyName ] = newVal;

            this.model.save(
                data,
                {
                    patch: true,
                    wait: true,
                    success: function( model, response, options )
                    {
                    },
                    error: function()
                    {
                        alert( "Error saving string value in DDBB" );
                    }
                }
            )
        },

        render: function()
        {
            var self = this;
            if( self.$el.html() === "" )
            {
                // hacer esto permite separar la logica del init al render
                // y poder meter un div contenedor en el template padre.
                // setElement es un método de backbone
                self.setElement( $( parentContainerId ) );
            }


            // primero renderizamos la vista de este elemento
            this.$el.append(
                this.template({
                    'objectType': objectType,
                    'languages': self._languages.models,
                    'object': self.model,
                })
            );

            // aplicamos la lógica necesaria a la vista de este elemento:
            self._showHideLanguageDependantContent();
            self._initializeTagManager();

            // DEFAULT VALUES:
            /*              isPublic              */
            if( ! self.$el.find( '#object_isPublic' ).is( ":checked" ) )
            {
                self.$el.find( '#isPublicContentRow' ).slideDown( 300 );
            }
            /*              isOnSale              */
            if( self.$el.find( '#object_isOnSale' ).is( ":checked" ) )
            {
                self.$el.find( '#isOnSaleContentRow' ).slideDown( 300 );
            }
            /*              isOnRent              */
            if( self.$el.find( '#object_isOnRent' ).is( ":checked" ) )
            {
                self.$el.find( '#isOnRentContentRow' ).slideDown( 300 );
            }



            // y luego renderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                // por ahora sacamos al PanoView del renderizado en forma de arbol
                // porque no todos sus elementos son views jerarquizados
                if( view.name != 'PanoView' )
                {
                    view.render();
                }
            });

            return this;
        },

        _showHideLanguageDependantContent: function()
        {

            var self = this;

            // recorremos todos los idiomas
            _.each( this._languages.models, function( disismyLanguage )
            {
                // el idioma está siendo usado por el objeto
                if( this.model.get( 'languages' ).get( disismyLanguage.get( 'id' ) ) )
                {
                    // console.log( "el idioma " + disismyLanguage.get( 'name' ) + " está siendo usado por el objeto" );
                        // activamos la bandera
                    this.$el.find( "#" + disismyLanguage.get( 'name' ) + "Btn>img" ).removeClass( "desaturate" );
                        // mostramos los contenidos de este idioma
                    this.$el.find( "." + disismyLanguage.get( 'name' ) + "Content" ).slideDown( 300 );
                }

            }, this );


        },

        postRender: function()
        {

            var self = this;

            // y luego postRenderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                view.postRender && view.postRender();
            });

            return self;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });



            $( 'body' ).removeClass( 'editObjectCss' );
            this.$el.empty();

            this.undelegateEvents();
        },

    });

    return EditObjectView;

});
