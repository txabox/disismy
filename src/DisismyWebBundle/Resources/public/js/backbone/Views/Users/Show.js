"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'text!templates/Users/showUserTmpl.html',
    'UserModel'
], function ( $, _, Backbone, showUserTmpl, UserModel )
{
    var isCurUser, parentContainerId;
    // los views tienen acceso al Model así:
    // 'this.model' si se pasa cuando se crea como parametro
    var ShowUserView =  Backbone.View.extend({

        template:   _.template( showUserTmpl ),

        initialize: function( options )
        {
            var self = this;
            self._views = [];

            _.bindAll( this, 'render' );

            $( 'body' ).addClass( 'showUserCss' );
            parentContainerId = options.parentContainerId;

            self._getModel( options.encodedUserId, function( model )
            {
                self._onInitialized();
            });


        },

        _onInitialized: function()
        {
            var self = this;
            self.render();
            self.postRender();
        },

        _getModel: function( encodedUserId, cb )
        {
            var self = this;

            self.model = new UserModel({ "encodedId": encodedUserId });

            self.model.fetch({
                success: function( model, response, options_ )
                {
                    if( response.isOk )
                    {
                        isCurUser = response.data.isCurUser;
                        cb( model );
                    }
                    else
                    {
                        console.error( "Error fetching users model: ", model, response, options_ );
                        alert( response.data.msg );
                    }
                },
                error: function( model, response, options_ )
                {
                    console.error( "Error fetching users model: ", model, response, options_ );
                }
            });
        },

        events:
        {
        },

        render: function()
        {

            var self = this;

            if( this.$el.html() === "" )
            {
                // hacer esto permite separar la logica del init al render
                // y poder meter un div contenedor en el template padre.
                self.setElement( $( parentContainerId ) );
            }

            // primero renderizamos la vista del padre
            this.$el.append(
                this.template({
                    "user": self.model,
                    "isCurUser": isCurUser
                })
            );

            // y luego renderizamos las vista de sus hijos

            _.each( self._views, function( view )
            {
                // por ahora sacamos al PanoView del renderizado en forma de arbol
                // porque no todos sus elementos son views jerarquizados
                if( view.name != 'PanoView' )
                {
                    view.render();
                }
            });

            return this;
        },

        postRender: function()
        {

            var self = this;

            // y luego postRenderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                view.postRender && view.postRender();
            });

            return self;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });

            $( 'body' ).removeClass( 'showUserCss' );

            this.$el.empty();

            this.undelegateEvents();
        },

    });

    return ShowUserView;

});
