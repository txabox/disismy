"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'globals',
    'PanoView',
    'text!templates/Default/indexTmpl.html'
], function ( $, _, Backbone, globals, PanoView, indexTmpl )
{
    var parentContainerId;
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var IndexView =  Backbone.View.extend({
        template:   _.template( indexTmpl ),

        initialize: function( options )
        {
            var self = this;
            this._views = [];

            _.bindAll( this, '_onInitialized', '_createPanoView' );

            $( 'body' ).addClass( 'indexCss' );
            parentContainerId = options.parentContainerId;

            self._createPanoView( 1126440043, function()
            {
                self._onInitialized();
            });

        },

        _onInitialized: function()
        {
            var self = this;
            self.render();
        },

        _createPanoView: function( panoEncodedId, cb )
        {
            var self = this;

            Disismy.Pano.view = new PanoView({
                'encodedPanoId': panoEncodedId,
                'parent': self,
                'parentContainerId': "#mainContentContainer",
                'isEmbeded': false,
                'cb': function( panoView )
                {
                    self._views.push( panoView );
                    cb();
                }
            });
            /*
            Disismy.Pano.view = new PanoView( panoEncodedId, "#mainContentContainer", false, function( panoView )
            {
                // añadimos esta nueva vista al array de vistas de este view
                self._views.push( panoView );
                cb();
            });
            */
        },

        events:
        {
        },

        render: function()
        {
            var self = this;

            if( this.$el.html() === "" )
            {
                // hacer esto permite separar la logica del init al render
                // y poder meter un div contenedor en el template padre.
                self.setElement( $( parentContainerId ) );
            }

            // primero renderizamos la vista del padre
            this.$el.append(
                this.template({
                })
            );

            // y luego renderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                // por ahora sacamos al PanoView del renderizado en forma de arbol
                // porque no todos sus elementos son views jerarquizados
                if( view.name != 'PanoView' )
                {
                    view.render();
                }
            });

            return this;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });

            $( 'body' ).removeClass( 'indexCss' );

            this.$el.empty();

            this.undelegateEvents();

        },

    });

    return IndexView;

});
