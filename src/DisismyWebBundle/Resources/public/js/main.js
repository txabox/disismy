"use strict";

requirejs.config({
    baseUrl: '/bundles/disismyweb/',
    locale: 'en',
    paths:
    {
        'text':             'libs/bower/bower_components/text/text',
        'jquery':           'libs/bower/bower_components/jquery/dist/jquery',
        'underscore':       'libs/bower/bower_components/underscore-amd/underscore-min',
        'bootstrap':        'libs/bower/bower_components/bootstrap/dist/js/bootstrap.min',
        'hammerjs':         'libs/bower/bower_components/hammerjs/hammer.min',
        'jquery-hammerjs':  'libs/bower/bower_components/jquery-hammerjs/jquery.hammer',
        'jquerymobile':     'libs/jquery.mobile.custom',
        'jquery-ui':        'libs/bower/bower_components/jquery-ui/jquery-ui.min',
        'jquery.caret':     'libs/bower/bower_components/caret/jquery.caret',
        'jquery.tag-editor':'libs/bower/bower_components/jquery-tag-editor/jquery.tag-editor.min',
        'modernizr':        'libs/Modernizr/modernizr-custom', // fullscreen o no-fullscreen
        'whatsapp':         'libs/Modernizr/whatsapp', // saber si tiene whatsapp instalado
        'notify':           'libs/bower/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify',
        'contextMenu':      'libs/bower/bower_components/jQuery-contextMenu/dist/jquery.contextMenu.min', // menus contextuales al dar al boton derecho del ratón
        'contextMenu_UI_Position': 'libs/bower/bower_components/jQuery-contextMenu/dist/jquery.ui.position.min',
        'backbone':         'libs/bower/bower_components/backbone-amd/backbone-min',
        'socialShareKit':   'libs/bower/bower_components/social-share-kit/dist/js/social-share-kit.min',
        'tweenable':        'libs/bower/bower_components/shifty/dist/shifty.min',
        'three':            'libs/bower/bower_components/three.js/build/three.min',
        'threeDeviceOrientationControls':   'libs/ThreeJS/DeviceOrientationControls',
        'threeCanvasRenderer':              'libs/ThreeJS/CanvasRenderer',
        'threeProjector':                   'libs/ThreeJS/Projector',
        'threeWebGLDetector':               'libs/ThreeJS/WebGLDetector',

        'globals':                          'js/globals',
        'extendedModals':                   'js/utils/extendedModals',
        'preventGhostClick':                'js/utils/PreventGhostClick',
        // Routers:
        'defaultRouter':                    'js/backbone/Routers/Default',
        // Models:
        'HouseModel':                       'js/backbone/Models/TypedObjects/House',
        'ObjectModel':                      'js/backbone/Models/Objects/Object',
        'UserModel':                        'js/backbone/Models/Users/User',
        'LanguageModel':                    'js/backbone/Models/Languages/Language',
        // Collections:
        'UsersCollection':                  'js/backbone/Collections/Users/User',
        'ObjectsCollection':                'js/backbone/Collections/Objects/Object',
        'LanguagesCollection':              'js/backbone/Collections/Languages/Language',
        // views:
        'ShowIndexView':                    'js/backbone/Views/Default/Index',
        'ShowUserView':                     'js/backbone/Views/Users/Show',
        'ShowGalleryView':                  'js/backbone/Views/Objects/Gallery',
        'ShowObjectView':                   'js/backbone/Views/Objects/Show',
        'EditObjectView':                   'js/backbone/Views/Objects/EditObjectView',
        'EditHouseFormView':                'js/backbone/Views/TypedObjects/House/EditHouseFormView'
    },
    shim:
    {
        'jquery-hammerjs':
        {
            deps: [ 'preventGhostClick' ],
        },
        'bootstrap':
        {
            deps: [ 'jquery' ],
            exports: 'bootstrap'
        },
        'contextMenu':
        {
            deps: [ 'jquery' ]
        },
        'contextMenu_UI_Position':
        {
            deps: [ 'jquery', 'contextMenu' ]
        },
        'whatsapp':
        {
            deps: [ 'modernizr' ]
        },
        'socialShareKit':
        {
            deps: [ 'whatsapp' ],
            exports: 'SocialShareKit'
        },
        'jquery.caret':
        {
            deps: [ 'jquery', 'jquery-ui' ],
        },
        'jquery.tag-editor':
        {
            deps: [ 'jquery', 'jquery-ui', 'jquery.caret' ],
        },

        'threeCanvasRenderer':
        {
            deps: [ 'three' ]
        },
        'threeProjector':
        {
            deps: [ 'three' ]
        },
        'threeWebGLDetector':
        {
            deps: [ 'three' ]
        },
        'threeDeviceOrientationControls':
        {
            deps: [ 'three' ]
        },
    },
    packages:
    [
        {
            name:       'PanoView',
            location:   'js/packages/Pano',  // default 'packagename'
            main:       'main',
        },
        {
            name:       'ImageManagerView',
            location:   'js/packages/ImageManager',  // default 'packagename'
            main:       'main',
        },
        {
            name:       'SharingModView',
            location:   'js/packages/SharingMod',  // default 'packagename'
            main:       'main',
        }
    ],
});

// r.js, el optimizador de requirejs usa el primer requirejs que encuentra
// y este no admite variables dinámicas. Es por esta razón que hay que crear
// 2 requirejs.config
requirejs.config({
    // Prevent caching during dev
    urlArgs: "bust=" + ( new Date() ).getTime(),
    locale: Disismy.Symfony.locale,
});



// Load the main app module to start the app
requirejs( [
    'jquery',
    'backbone',
    'notify'
], function( $, Backbone, notify )
{
    'use strict';

    $.notifyDefaults({
    	type: 'danger',
        showProgressbar: true,
    });



    requirejs( [
        'defaultRouter',
        'modernizr',
        'bootstrap',
    ], function( Router )
    {

        var router = new Router();
        Backbone.history.start();


        // hacks varios:
        $( "#disismyProfileBtn, #disismyMyObjectsBtn" ).click( function()
        {
            $( ".navbar-collapse" ).removeClass( "in" );
        });

        if( ! Disismy.Symfony.DEBUG )
        {
            $( "#debugTxt" ).hide();
        }

        if( Disismy.Symfony.requireJScurAppPath )
        {
            // requirejs( [ Disismy.Symfony.requireJScurAppPath ] );
            alert( "Esto ya no vale Txabox: " + Disismy.Symfony.requireJScurAppPath );
        }

    });

});
