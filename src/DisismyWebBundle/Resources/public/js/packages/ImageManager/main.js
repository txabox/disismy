"use strict";
/*
requirejs.config({
    paths:
    {
        'ImageModel':           'js/backbone/Models/ImageManager/Image',
        'ImagesCollection':     'js/backbone/Collections/ImageManager/Image',
        'ImagesCollectionView': './Views/ImagesCollection',
        'ImageManagerView':     './Views/ImageManager',
        'AddNewImageModalView': './Views/AddNewImageModal',
        'DeleteImageModalView': './Views/DeleteImageModal',
        'ImageView':            './Views/Image',
        'gammaGallery':                 'libs/gammaGallery/gamma',
        'gammaGallery.modernizr':       'libs/gammaGallery/modernizr.custom.70736',
        'gammaGallery.jquery.masonry':  'libs/gammaGallery/jquery.masonry',
        'gammaGallery.jquery.history':  'libs/gammaGallery/jquery.history',
        'gammaGallery.js-url':          'libs/gammaGallery/js-url',
        'gammaGallery.jquerypp':        'libs/gammaGallery/jquerypp.custom',
    },
    shim:
    {
        'gammaGallery':
        {
            deps: [ 'jquery', 'gammaGallery.modernizr', 'gammaGallery.jquery.masonry', 'gammaGallery.jquery.history', 'gammaGallery.js-url', 'gammaGallery.jquerypp' ],
            exports: 'Gamma'
        },
    },
});
*/

define( [
    './Views/ImageManager',
    'extendedModals',
], function ( ImageManagerView )
{
    var ImageManager = function( options )
    {
        return new ImageManagerView( options );
    }

    return ImageManager;

});

/*
define( [ 'ImageManagerView', 'extendedModals' ], function ( ImageManagerView )
{
    var ImageManager = function()
    {
        var self = this;

        var imageManagerView = new ImageManagerView();



        this.show = function( onImageManagerClosed_ )
        {
            imageManagerView.show( onImageManagerClosed_ );
        };

        return this;

    };

    return ImageManager;

});
*/
