"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    '../Collections/Image',
    '../Views/ImagesCollection',
    '../Views/AddNewImageModal',
    '../Views/DeleteImageModal',
    '../Models/Image',
    'text!../Templates/imageManagerModalTmpl.html',
], function (
    $,
    _,
    Backbone,
    ImagesCollection,
    ImagesCollectionView,
    AddNewImageModalView,
    DeleteImageModalView,
    ImageModel,
    imageManagerModalTmpl )
{
    /*
    $.notify({
    	// options
    	icon: 'glyphicon glyphicon-warning-sign',
    	message: 'Hello World'
    },{
    	// settings
    	type: 'info',
    });
    */

    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var ImageManagerView = Backbone.View.extend({
        tagName:    'div',
        className:  'modal fade',
        id:         'imageManagerModal',
        template:   _.template( imageManagerModalTmpl ),

        attributes: function()
        {
            return {
                tabindex : -1,
                role: "dialog",
                "aria-labelledby": "imageManagerModalLabel",
                "aria-hidden": true
            };
        },

        initialize: function( options )
        {
            if( $( "#imageManagerModal" ).length )
            {
                alert( 'there_cant_be_more_than_one_imageManager_instance' );
                return;
            }

            var self = this;
            self.parent = options.parent;

            self._views = [];

            // private
            _.bindAll( this, '_onInitialized' );
            // public
            _.bindAll( this, 'render' );

            $( 'body' ).addClass( 'imageManagerCss' );

            // lo renderizamos la primera vez, como este elemento padre no es dinamico
            // se puede hacer al inicializarse
            // this.$el.html( this.template({}) );
            // $( 'body' ).append( this.el );

            // la metemos aquí para que sea consumible desde todos sus hijos
            this.collection = new ImagesCollection();
            this.listenTo( this.collection, 'remove', function()
            {
                // alert( "Txabox. Se quita en el frontend esta imagen de la colección cuando se hace destroy del model!" );
                // debugger;
            });

            // TODO: Aún no hacemos nada con los folders
            this._selectedObjects = {
                folderViews: [],
                imageViews: []
            };
            this.onClosedCb;
            this.isGammaInitializing = false;
            this.isGammaInitialized = false;

            // EVENTS:
            $( window ).on( "resize", _.bind( this.resetBodyHeight, this ) );

            // 1. Creamos imagesColView ( el ul de la colección de imagenes )
            this.imagesColView = new ImagesCollectionView({
                collection: this.collection,
                parent: this,
                cb: function( imagesColView )
                {

                    self._views.push( imagesColView );

            // 2. Creamos addNewImageModalView
                    self.addNewImageModalView = new AddNewImageModalView({
                        collection: self.collection,
                        parent: self,
                        cb: function( addNewImageModalView )
                        {

                            self._views.push( addNewImageModalView );

            // 3. Creamos deleteImageModalView
                            self.deleteImageModalView = new DeleteImageModalView({
                                collection: self.collection,
                                parent: self,
                                cb: function( deleteImageModalView )
                                {

                                    self._views.push( deleteImageModalView );

            // 4. Seleccionamos root folder
                                    self._getRootFolder( function()
                                    {
            // 5. Añadimos las imagenes de este folder al ul
                                        self._populateGammaGallery( function()
                                        {
                                            self._onInitialized( options.cb );
                                        });
                                    });
                                }
                            })
                        }
                    });
                }
            });
        },

        _onInitialized: function( parentViewCb )
        {
            var self = this;

            // y llamamos al cb del parent
            parentViewCb && parentViewCb( self );
        },

        events:
        {
            "shown.bs.modal": "onShown",
            "hidden.bs.modal" : "onHidden",
            'click #selectImagesBtn': "_selectImagesBtnClicked",
            'click #deleteImagesBtn': "_deleteImagesBtnClicked",
        },

        _deleteImagesBtnClicked: function( e )
        {
            this.showDeleteImageModal( this._selectedObjects.imageViews );
        },

        showDeleteImageModal: function( imageViewsToDelete )
        {
            this.deleteImageModalView.show( imageViewsToDelete );
        },

        render: function()
        {

            var self = this;

            // si no tiene el template del modal metido en el body:
            if( ! $( "#imageManagerModal" ).length )
            {
                // lo añadimos
                this.$el.html( this.template({}) );
                $( 'body' ).append( this.el );
            }
            // como ya tiene el template metido en el body
            else
            {
                console.error( "Yo creo que aquí no tendría que llegar nunca." );
                alert( "Error al renderizar el ImageManager." );
                // simplemente renderizamos la vista de este elemento
                // this.$el.append(
                //     this.template({})
                // );
            }


            // aplicamos la lógica necesaria a la vista de este elemento:

            // y luego renderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                view.render();
            });

            return self;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });



            $( 'body' ).removeClass( 'imageManagerCss' );

            this.isGammaInitializing = false;
            Gamma.unload();

            this.$el.empty();
            this.undelegateEvents();

            // en este caso, como queremos que el modal esté al final del body
            // ( no en el template de este view porque queda más guarro ),
            // lo borramos también completamente y se creará de nuevo
            // en el próximo render.
            this.$el.remove();


        },

        postRender: function()
        {

            var self = this;

            // y luego postRenderizamos las vista de sus hijos
            _.each( self._views, function( view )
            {
                view.postRender && view.postRender();
            });

            return self;
        },

        add: function( image )
        {
            this.collection.add( image );
            // this.imagesColView.addOne( image );
        },

        show: function( onClosedCb_ )
        {
            this.onClosedCb = onClosedCb_;

            this._deselectAllImages();

            // cuando se cargue la libreria Gamma cambiarán los valores de
            // backdrop y keyboard
            this.$el.modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
        },

        onShown: function()
        {
            var self = this;

            this.resetBodyHeight();

            if( this.isGammaInitialized )
            {
                // console.log( "Como la galería ya estaba inicializada, simplemente forzamos un resize para evitar los petes cuando hacemos windows rezizes con la galería no visible" );
                Gamma.forceResize();
            }
            // la galeria aun no se ha inicializado
            else
            {
                // si la galeria no se ha inicializado aún, pero lo está haciendo
                if( this.isGammaInitializing )
                {
                    // simplemente no hacemos nada
                    // console.log( "La galería se está inicializando. No hacemos nada." );
                }
                // si la galería no se ha inicializado y aún no se ha mandado a inicializar
                else
                {
                    // lo inicializamos
                    // console.log( "Como aún no habíamos inicializado la galería. Inicializamos la galería sólo una vez." );
                    this.isGammaInitializing = true;

                    var GammaSettings = {
                            // order is important!
                            viewport : [ {
                                width : 1200,
                                columns : 5
                            }, {
                                width : 769,
                                columns : 3
                            }, {
                                width : 500,
                                columns : 2
                            }, {
                                width : 320,
                                columns : 1
                            }, {
                                width : 0,
                                columns : 1
                            } ]
                    };

                    Gamma.init( GammaSettings, onGammaGalleryLoaded );

                }
            }

            function onGammaGalleryLoaded()
            {
                self.isGammaInitializing = false;
                self.isGammaInitialized = true;

                // volvemos a poder cerrar el modal
                self.$el.find( ".closeImageManagerModal" ).prop( "disabled", false );
                self.$el.data( 'bs.modal' ).options.backdrop = true;
                self.$el.data( 'bs.modal' ).options.keyboard = true;

            }
        },

        _preHide: function()
        {
            this.onClosedCb && this.onClosedCb( this._selectedObjects );
        },

        hide: function()
        {
            this.$el.modal( "hide" );
        },

        onHidden: function()
        {
            this.$el.find( ".modal-backdrop" ).remove();
        },

        resetBodyHeight: function()
        {
            if( this.$el.is( ":visible" ) )
            {
                this.$el.find( ".scrollable" ).height( $( window ).height() - 205 );
            }
            else
            {
                console.log( "No hacemos nada a su height porque el modal no esta visible" );
            }
        },

        _getRootFolder: function( cb )
        {

            var self = this;

            // cogemos el rootFolder de este usuario
            $.ajax({
                url: Disismy.Symfony.BASE_URL + "/rootFolder/",
                method: "POST"
            })
            .done( function( data )
            {
                if( data.isOk )
                {
                    // console.log( "Data saved: ", data );
                    self.curFolder = data.data.folder;
                    self.curFolderImages = self.curFolder.images;
                    cb();
                }
                else
                {
                    $.notify( "Data not saved: " + data.data.msg + ". Please, try again later." );
                }
            })
            .fail( function( data )
            {
                $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
            });
        },

        _populateGammaGallery: function( cb )
        {

            var self = this;
            for( var i = 0; i < self.curFolderImages.length; ++i )
            {
                var image = new ImageModel( self.curFolderImages[ i ] );
                self.add( image );
            }

            cb();
        },

        _selectImagesBtnClicked: function( e )
        {
            this._preHide();
        },

        // an image was selected from the image model view
        imageSelected: function( imageView, cb )
        {
            this._selectedObjects.imageViews.push( imageView );
            this._selectedObjects.imageViews = _.uniq( this._selectedObjects.imageViews );
            this.$el.find( ".imageSelectedNum" ).html( this._selectedObjects.imageViews.length );
            if( this._selectedObjects.imageViews.length > 0 )
            {
                this.$el.find( "#selectImagesBtn, #deleteImagesBtn" ).prop( "disabled", false );
            }
            cb && cb();
        },

        imageDeselected: function( imageView, cb )
        {
            var index = this._selectedObjects.imageViews.indexOf( imageView );
            if( index > -1 )
            {
                this._selectedObjects.imageViews.splice( index, 1 );
            }
            this.$el.find( ".imageSelectedNum" ).html( this._selectedObjects.imageViews.length );

            if( this._selectedObjects.imageViews.length === 0 )
            {
                this.$el.find( "#selectImagesBtn, #deleteImagesBtn" ).prop( "disabled", true );
            }
            cb && cb();
        },

        _deselectAllImages: function()
        {
            var self = this;
            // TODO: faltaría hacerlo con los folder también
            for( var i = this._selectedObjects.imageViews.length -1; i >= 0; --i )
            {
                var imageView = this._selectedObjects.imageViews[ i ];
                var $checkbox = imageView.$el.find( ".selectImgCb" );
                $checkbox.prop( 'checked', false );
                self.imageDeselected( imageView );
            }
        },

    });

    return ImageManagerView;

});
