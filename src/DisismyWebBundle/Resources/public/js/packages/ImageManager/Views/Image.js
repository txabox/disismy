"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'globals',
    'text!../Templates/imageViewTmpl.html',
    'contextMenu',
    'jquery-hammerjs'
], function ( $, _, Backbone, globals, imageViewTmpl )
{
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var ImageView = Backbone.View.extend({
        tagName:    "li",
        className:  "draggable IS_REMOVABLE IS_EDITABLE IS_SELECTABLE",
        // id:         'li_' + this.model.get( 'encodedId' ),


        attributes: function ()
        {
            return {
              id : "li_" + this.model.get( 'encodedId' )
            };
        },


        template: _.template( imageViewTmpl ),

        initialize: function( options )
        {
            var self = this;

            this.imagesColView = options.parent;
            this.imageManagerView = this.imagesColView.imageManagerView;

            this.$html = $( 'html' );
            this._isTouchEvent = false;
            this._isLongTap = false;

            self._views = [];
            $( 'body' ).addClass( 'imageCss' );

            $.contextMenu({
                selector: '#li_' + self.model.get( 'encodedId' ),
                trigger: 'none',
                scope: this,
                callback: function( key, options )
                {
                    switch( key )
                    {
                        case "edit":
                            options.scope._editImage();
                            break;
                        case "delete":
                            options.scope._removeImage();
                            break;
                        case "select":
                            options.scope._imageSelectedDeselected();
                            break;
                        case "quit":
                            break;
                        default:
                            break;
                    }
                },
                items:
                {
                    "edit": {name: Translator.trans( 'edit' ), icon: "fa-edit"},
                    "delete": {name: Translator.trans( 'delete' ), icon: "fa-trash"},
                    "select": {name: Translator.trans( 'select' ), icon: "fa-hand-pointer-o"},
                    "sep1": "---------",
                    "quit": {name: Translator.trans( 'quit' ), icon: function()
                    {
                        return 'context-menu-icon context-menu-icon-quit';
                    }}
                }
            });


            self.listenTo( self.model, 'change', self.render );

        },

        render: function()
        {
            // this.attributes( this.model.toJSON() )
            this.$el.html( this.template({
                model: this.model.toJSON(),
                'globals': globals
            }) );


            var hammerOptions = {
            };
            this.$el.hammer( hammerOptions );


            var singleTap = new Hammer.Tap({ event: 'tap' });
            var doubleTap = new Hammer.Tap({event: 'doubletap', taps: 2 });


            this.$el.data( "hammer" ).add([ doubleTap, singleTap ]);

            doubleTap.recognizeWith( singleTap );
            singleTap.requireFailure([ doubleTap ]);



            return this;

        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });



            $( 'body' ).removeClass( 'imageCss' );

            this.$el.empty();
            this.undelegateEvents();

        },

        events:
        {
            // touch events:
            "touchstart .gamma-description": "onTouchStart",
            "touchmove .gamma-description": "onTouchMove",
            "touchend .gamma-description": "onTouchEnd",

            // mouse events
            "mouseover .gamma-description": "onMouseOver",
            "mousemove .gamma-description": "onMouseMove",
            // onPress
            "mousedown .gamma-description": "onMouseDown",
            "mouseup .gamma-description": "onMouseUp",
            // onRelease
            "click .gamma-description" : "onClick",

            // hammerjs events:
            // TODO: no entiendo x q no se puede hacer
            // "pan .gamma-description": "onPan",
            // con los eventos de hammerjs
            "pan": "onPan",
            "tap": "singleTap",
            "doubletap": "doubleTap",
            "press": "onLongTap",

            'click .removeImg': "_removeImage",
            'click .editImg': "_editImage",
            'click .selectImg': "_imageSelectedDeselected",
            'click .selectImgCb': "_imageSelectedDeselected",
        },

        _removeImage: function( e )
        {
            this.imageManagerView.showDeleteImageModal( [ this ] );
        },

        _editImage: function( e )
        {
            console.log( "clicked: edit " + this.model.get( 'encodedId' ) );
        },

        _imageSelectedDeselected: function( e )
        {
            var $checkbox = this.$el.find( ".selectImgCb" );

            // if there is no e it is called from the view
            if( ! e )
            {
                if( $checkbox.is( ":checked" ) )
                {
                    $checkbox.prop( 'checked', false );
                    this.imageManagerView.imageDeselected( this );
                }
                else
                {
                    $checkbox.prop( 'checked', true );
                    this.imageManagerView.imageSelected( this );
                }
            }
            // e is an event
            else
            {
                // se le llama tanto al presionar sobre el span como sobre el cb
                // para que no se llame dos veces cuando presionas sobre el span
                // y sobre el checkbox, paramos la propagacion del evento
                e.stopPropagation();

                // se ha presionado sobre el checkbox, ya ha cambiado el estado
                if( e.currentTarget.type === "checkbox" )
                {
                    $checkbox.is( ":checked" ) ? this.imageManagerView.imageSelected( this ) : this.imageManagerView.imageDeselected( this.model );
                }
                // se ha presionado sobre el span, no ha cambiado el estado aún
                else
                {
                    if( $checkbox.is( ":checked" ) )
                    {
                        $checkbox.prop( 'checked', false );
                        this.imageManagerView.imageDeselected( this );
                    }
                    else
                    {
                        $checkbox.prop( 'checked', true );
                        this.imageManagerView.imageSelected( this );
                    }
                }
            }

        },

        onTouchStart: function( e )
        {
            // e.preventDefault();
            // e.stopPropagation();

            if( ! this.$html.hasClass( 'touch' ) ) return false;

            this._isTouchEvent = true;
            console.log( 'onTouchStart.', this.model.get( 'encodedId' ) );
        },

        onTouchMove: function( e )
        {

            if( ! this.$html.hasClass( 'touch' ) ) return false;

            // e.preventDefault();
            // e.stopPropagation();
            console.log( 'onTouchMove.', this.model.get( 'encodedId' ) );
        },

        onTouchEnd: function( e )
        {

            if( ! this.$html.hasClass( 'touch' ) ) return false;

            // e.preventDefault();
            // e.stopPropagation();
            this._isTouchEvent = false;
            console.log( 'onTouchEnd.', this.model.get( 'encodedId' ) );
        },



        onMouseOver: function( e )
        {

            if( this.$html.hasClass( 'touch' ) ) return false;

            // e.preventDefault();
            // e.stopPropagation();
            // console.log( 'onMouseOver.', this.model.get( 'encodedId' ) );
        },

        onMouseMove: function( e )
        {

            if( this.$html.hasClass( 'touch' ) ) return false;

            // e.preventDefault();
            // e.stopPropagation();
            // console.log( 'onMouseMove.', this.model.get( 'encodedId' ) );
        },

        onMouseDown: function( e )
        {
            if( this.$html.hasClass( 'touch' ) ) return false;

            // e.preventDefault();
            // e.stopPropagation();

            switch( e.which )
            {
                case 1:
                    console.log( 'onLeftMouseDown.', this.model.get( 'encodedId' ) );
                    break;
                case 2:
                    console.log( 'onMiddleMouseDown.', this.model.get( 'encodedId' ) );
                    break;
                case 3:
                    console.log( 'onRightMouseDown.', this.model.get( 'encodedId' ) );
                    this._onRightClickOrLongTap( e );
                    break;
                default:
                    console.log( 'You have a strange Mouse!', this.model.get( 'encodedId' ) );
                    break;
            }
        },

        // lo vamos a manejar a traves del singleTap
        onMouseUp: function( e )
        {

            if( this.$html.hasClass( 'touch' ) ) return false;

            // e.preventDefault();
            // e.stopPropagation();
            if( this._isLongTap )
            {
                // el singleTap lo captura hammer.js
                // pero no el longTap desde el mouse. Tenemos que hacerlo así:
                console.log( 'onMouseLongTap', this.model.get( 'encodedId' ) );
                this._isLongTap = false;
            }
        },

        // lo vamos a manejar a traves del singleTap
        onClick: function( e )
        {

            if( this.$html.hasClass( 'touch' ) ) return false;

            // e.preventDefault();
            // e.stopPropagation();
            // console.log( "onClick", this.model.get( 'encodedId' ) );
        },



        /*************************** hammer.js ***************************/
        onPan: function( e )
        {
            // e.preventDefault();
            // e.stopPropagation();
            // console.log( "onPan", this.model.get( 'encodedId' ) );
        },

        // cuando se hace release del boton
        singleTap: function( e )
        {
            // e.preventDefault();
            // e.stopPropagation();
            console.log( "singleTap", this.model.get( 'encodedId' ) );
            this._onLeftMouseOrSingleTap( e );
        },

        // long tap pressed
        onLongTap: function( e )
        {
            this._isLongTap = true;
            // solo si es tactil
            if( this._isTouchEvent )
            {
                // e.preventDefault();
                // e.stopPropagation();
                console.log( 'onLongTap.', this.model.get( 'encodedId' ) );
                this._onRightClickOrLongTap( e );
                this._isLongTap = false;
            }
        },

        doubleTap: function( e )
        {
            // e.preventDefault();
            // e.stopPropagation();
            console.log( "doubleTap", this.model.get( 'encodedId' ) );
        },

        _onLeftMouseOrSingleTap: function( e )
        {

        },

        _onRightClickOrLongTap: function( e )
        {

            var xPos = ( e.pageX || e.gesture.pointers[ 0 ].pageX ) + 5;
            var yPos = ( e.pageY || e.gesture.pointers[ 0 ].pageY ) + 8;

            this.$el.contextMenu( { x: xPos, y: yPos } );
        },

    });



    return ImageView;

});
