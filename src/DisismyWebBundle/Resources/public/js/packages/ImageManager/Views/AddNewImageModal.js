"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'globals',
    'text!../Templates/addNewImageModalTmpl.html'
], function ( $, _, Backbone, globals, addNewImageModalTmpl )
{
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var AddNewImageModalView =  Backbone.View.extend({
        tagName:    'div',
        className:  'modal fade',
        id:         'addNewImageModal',
        template:   _.template( addNewImageModalTmpl ),

        attributes: function()
        {
            return {
                tabindex : -1,
                role: "dialog",
                "aria-labelledby": "addNewImageModalLabel",
                "aria-hidden": true
            };
        },

        initialize: function( options )
        {
            var self = this;
            this.parent = options.parent;
            this.languages = [];
            self._views = [];
            $( 'body' ).addClass( 'addNewImageModalCss' );


            this.getLanguages( function()
            {
                self._onInitialized( options.cb );
            });

        },

        _onInitialized: function( parentViewCb )
        {
            var self = this;

            // y llamamos al cb del parent
            parentViewCb && parentViewCb( self );
        },

        events:
        {
            "shown.bs.modal": "onShown",
            "hidden.bs.modal": "onHidden",
            "click #showMoreOptions": "showMoreOptions",
            "click #hideMoreOptions": "hideMoreOptions",
            "click #addNewImgBtn": "submitForm",
            "change #image_imageFile_file": "_fileSelected"
        },

        _fileSelected: function( e )
        {

            $( '#val' ).text( "" );
            for( var i=0; i < e.target.files.length; ++i )
            {
                var file = e.target.files[ i ];
                console.log( "file: ", file );
                $( '#val' ).prepend( file.name.replace( /C:\\fakepath\\/i, '' ) + "<br />" );
            }

            // alert( "Txabox: " + e.target.value.replace(/C:\\fakepath\\/i, ''))

        },

        render: function()
        {
            var self = this;

            this.$el.html(
                this.template(
                    {
                        "languages": this.languages,
                        'globals': globals,
                    })
                );

            if( ! $( '#addNewImageModal' ).length )
            {
                $( 'body' ).append( this.el );
            }
            else
            {
                console.error( "Yo creo que aquí no tendría que llegar nunca." );
                alert( "Error al renderizar el AddNewImageModal." );
            }

            self.$el.find( ".loadingDIV" ).addClass( 'hidden' );
            self.$el.find( "#addImgForm" ).removeClass( 'hidden' );

            if( this.languages.length )
            {
                var $loadingDIV = self.$el.find( '.loadingDIV' );
                var $addImgForm = self.$el.find( '#addImgForm' );

                $loadingDIV.slideUp( 100, function()
                {
                    $addImgForm.slideDown( 300 );
                });
            }

            this._$addImgForm       = this.$el.find( '#addImgForm' );
            this._$imgExtraOptions  = this.$el.find( '#imgExtraOptions' );
            this._$showMoreOptions  = this.$el.find( '#showMoreOptions' );
            this._$hideMoreOptions  = this.$el.find( '#hideMoreOptions' );
            this._$inputFile        = this.$el.find( 'input[type=file]' );

            return this;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });



            $( 'body' ).removeClass( 'addNewImageModalCss' );

            this.$el.empty();
            this.undelegateEvents();

            // en este caso, como queremos que el modal esté al final del body
            // ( no en el template de este view porque queda más guarro ),
            // lo borramos también completamente y se creará de nuevo
            // en el próximo render.
            this.$el.remove();

        },

        getLanguages: function( cb )
        {
            var self = this;

            $.ajax({
                url: Disismy.Symfony.BASE_URL + "/languages/",
                method: "GET"
            })
            .done( function( data )
            {
                if( data.isOk )
                {
                    // console.log( "Data saved: ", data );
                    self.languages = data.data.languages;
                    cb();
                }
                else
                {
                    $.notify( "Data not saved: " + data.data.msg + ". Please, try again later." );
                }
            })
            .fail( function( data )
            {
                $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
            });
        },

        onShown: function()
        {
        },

        onHidden: function()
        {
            $( 'body' ).addClass( 'modal-open' );
        },

        showMoreOptions: function()
        {
            var self = this;

            this._$imgExtraOptions.slideDown( 300, function()
            {
                self._$showMoreOptions.addClass( 'hidden' );
                self._$hideMoreOptions.removeClass( 'hidden' );
            });
        },

        hideMoreOptions: function()
        {
            var self = this;

            this._$hideMoreOptions.addClass( 'hidden' );
            this._$showMoreOptions.removeClass( 'hidden' );
            this._$imgExtraOptions.slideUp( 300, function()
            {
            });
        },

        submitForm: function( e )
        {

            e.preventDefault();

            var self = this;
            var addImgForm = this._$addImgForm[ 0 ]; // You need to use standart javascript object here
            var addImgFormData = new FormData( addImgForm );

            /*

            https://css-tricks.com/drag-and-drop-file-uploading/

            if( droppedFiles )
            {
                $.each( droppedFiles, function(i, file) {
                    addImgFormData.append( $input.attr( 'name' ), file );
                });
            }
            */
            if( this._$inputFile[ 0 ].files[ 0 ]  )
            {
                addImgFormData.append( this._$inputFile.attr( 'name' ), this._$inputFile[ 0 ].files[ 0 ] );
                // Submit data via AJAX to the form's action path.
                $.ajax({
                    url : Disismy.Symfony.BASE_URL + '/add/image/in/folder/' + self.parent.curFolder.encodedId + '/',
                    type: self._$addImgForm.attr( 'method' ),
                    data : addImgFormData,
                    contentType: false,  // tell jQuery not to set contentType
                    processData: false,  // tell jQuery not to process the data
                    enctype: 'multipart/form-data'
                })
                .done( function( data )
                {
                    if( data.isOk )
                    {
                        // console.log( "Data saved: ", data );
                        // var $newLI = createNewImageLi( data.data.image );
                        // Gamma.add( $newLI );
                        self.parent.add( data.data.image );
                        self.$el.modal( 'hide' );
                    }
                    else
                    {
                        $.notify( "Data not saved: " + data.data.msg + ". Please, try again later." );
                    }
                })
                .fail( function( data )
                {
                    $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
                });
            }
            else
            {
                console.warn( "No file selected" );
            }
        },

    });

    return AddNewImageModalView;

});
