"use strict";

define( [
    'jquery',
    'backbone',
    '../Views/Image',
    '../libs/gammaGallery/modernizr.custom.70736',
    '../libs/gammaGallery/jquery.masonry',
    // '../libs/gammaGallery/jquery.history',
    '../libs/gammaGallery/js-url',
    '../libs/gammaGallery/jquerypp.custom',
    '../libs/gammaGallery/gamma',
], function ( $, Backbone, ImageView )
{

    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var ImagesCollectionView = Backbone.View.extend({
        tagName:    'ul',
        className:  'gamma-gallery',

        initialize: function( options )
        {
            var self = this;

            this.imageManagerView = options.parent;


            self._views = [];
            $( 'body' ).addClass( 'imagesCollectionCss' );

            this.listenTo( this.imageManagerView.collection, "add", this.addOne );

            self._onInitialized( options.cb );

        },

        _onInitialized: function( parentViewCb )
        {
            var self = this;
            // y llamamos al cb del parent
            parentViewCb && parentViewCb( self );
        },

        addOne: function( image )
        {
            var self = this;
            // creating a new child views
            var imageView = new ImageView({
                model: image,
                parent: this,
            });
            self._views.push( imageView );

            // append to the root element
            this.$el.append( imageView.render().el );

            // le decimos a Gamma que hay un nuevo li
            Gamma.add( imageView.$el );
        },

        render: function()
        {

            // this.collection.each( this.addOne, this );
            if( ! $( ".gamma-gallery" ).length )
            {
                $( "#gamma-container" ).append( this.$el );
            }

            return this;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });



            $( 'body' ).removeClass( 'imagesCollectionCss' );

            this.$el.empty();
            this.undelegateEvents();

        }

    });

    return ImagesCollectionView;

});
