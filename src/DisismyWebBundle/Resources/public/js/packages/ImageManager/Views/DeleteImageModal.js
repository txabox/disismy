"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'text!../Templates/deleteImageModalTmpl.html'
], function ( $, _, Backbone, deleteImageModalTmpl )
{
    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var DeleteImageModal = Backbone.View.extend({
        tagName:    'div',
        className:  'modal centeredModal fade',
        id:         'deleteImageModal',
        template:   _.template( deleteImageModalTmpl ),

        attributes: function()
        {
            return {
                tabindex : -1,
                role: "dialog",
                "aria-labelledby": "deleteImageModalLabel",
                "aria-hidden": true
            };
        },

        initialize: function( options )
        {
            var self = this;
            self.imageManagerView = options.parent;
            self._views = [];
            self._imageViewsToDelete = [];
            $( 'body' ).addClass( 'deleteImageModalCss' );

            self._onInitialized( options.cb );

        },

        show: function( imageViewsToDelete )
        {
            this._imageViewsToDelete = imageViewsToDelete;
            this.$el.modal();
            this._addImagesToDeleteNamesList();
        },

        _addImagesToDeleteNamesList: function( cb_ )
        {
            var imageNamesList = "<ul>";
            _.each( this._imageViewsToDelete, function( imageView )
            {
                imageNamesList += "<li><strong>" + imageView.model.get( 'name' ) + "</strong></li>";
            });
            imageNamesList += "</ul>";
            this.$el.find( "#imagesToDeleteNamesList" ).html( imageNamesList );

            cb_ && cb_();
        },

        _onInitialized: function( parentViewCb )
        {
            var self = this;

            // y llamamos al cb del parent
            parentViewCb && parentViewCb( self );
        },

        events:
        {
            "shown.bs.modal": "_onShown",
            "hidden.bs.modal": "_onHidden",
            "click #deleteImgBtn": "deleteImages",
        },

        render: function()
        {
            var self = this;

            self.$el.html( this.template({}) );

            if( ! $( '#deleteImageModal' ).length )
            {
                $( 'body' ).append( self.el );
            }
            else
            {
                console.error( "Yo creo que aquí no tendría que llegar nunca." );
                alert( "Error al renderizar el DeleteImageModal." );
            }

            self.$el.find( ".loadingDIV" ).addClass( 'hidden' );
            self.$el.find( ".content" ).removeClass( 'hidden' );

            self._$deleteImgBtn = this.$el.find( "#deleteImgBtn" );

            return this;
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });



            $( 'body' ).removeClass( 'deleteImageModalCss' );

            this.$el.empty();
            this.undelegateEvents();

            // en este caso, como queremos que el modal esté al final del body
            // ( no en el template de este view porque queda más guarro ),
            // lo borramos también completamente y se creará de nuevo
            // en el próximo render.
            this.$el.remove();

        },



        _onShown: function()
        {
        },

        _onHidden: function()
        {
            $( 'body' ).addClass( 'modal-open' );
        },

        deleteImages: function()
        {
            var self = this;

            _.each( self._imageViewsToDelete, function( imageView )
            {
                // cuando haces destroy, hay una petición al servidor, se borra
                // este elemento y también se invoca un remove en la colección
                // en el frontend
                imageView.model.destroy({
                    wait: true,
                    success: function( model, response )
                    {
                        if( response.isOk )
                        {
                            // console.log( "Data saved: ", response );
                            Gamma.remove( $( "#li_" + response.data.encodedImageId ) );
                            self.imageManagerView.imageDeselected( model );
                            self.$el.modal( 'hide' );
                        }
                        else
                        {
                            $.notify( "Data not saved: " + response.data.msg + ". Please, try again later." );
                        }
                    },
                    error: function( model, response )
                    {
                        $.notify( "Data not saved: Unknown error. Please, check your browser console error log and try again later." );
                    }
                });

            });
        },


    });


    return DeleteImageModal;

});
