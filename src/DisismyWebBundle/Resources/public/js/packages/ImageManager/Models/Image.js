"use strict";

define( [
    'jquery',
    'backbone'
], function ( $, Backbone )
{
    /*
    ejemplo de variables y funciones privadas
    var privateVar = "privateVar";
    function privateFunction(arg)
    {
        console.log("Esto es una funcion privada cuyo argumento es: ", arg);
    }
    */
    var ImageModel = Backbone.Model.extend({
            defaults:{
                "id"        : -1,
                "encodedId" : -1,
                "name"      : "",
                "alt"       : "",
                "desc"      : ""
            },

            initialize: function()
            {
                // console.log( "A new ImageModel has been initialized", this );
                this.on( 'change:encodedId', this.setId );
                this.set({ id: this.get( "encodedId" ) });
            },

            setId: function()
            {
                this.set({ id: this.get( "encodedId" ) });
            },

            // para sacar el mensaje de error
            // myImage.on('invalid', function (model, error) {
            // console.log(error);
            //});
            validate: function( attributes )
            {
                if ( attributes.encodedId < 0 )
                {
                    return 'Invalid encodedId.';
                }
            },

            render: function()
            {
            }

        });

    return ImageModel;

});
