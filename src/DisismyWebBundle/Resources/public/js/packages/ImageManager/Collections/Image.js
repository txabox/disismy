"use strict";

define( [
    'jquery',
    'backbone',
    '../Models/Image'
], function ( $, Backbone, ImageModel )
{

    var ImagesCollection =  Backbone.Collection.extend({
            url: '/images/',
            model: ImageModel
        });


    return ImagesCollection;

});
