"use strict";

define( [
    './Views/SharingMod',
], function ( SharingModView )
{
    var SharingMod = function( options )
    {
        return new SharingModView( options );
    }

    return SharingMod;

});
