"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'globals',
    'text!./../Templates/sharingModTmpl.html',
    'socialShareKit',
    'whatsapp'
], function ( $, _, Backbone, globals, sharingModTmpl, SocialShareKit )
{

    var parentContainerId;

    var SharingModView =  Backbone.View.extend({
        template:   _.template( sharingModTmpl ),
        name:       'SharingModView',
        initialize: function( options )
        {
            var self = this;
            self._views = [];
            self._parent = options.parent;

            _.bindAll( this, 'render', 'postRender' );

            $( 'body' ).addClass( 'sharingModCss' );

            self.panoView = options.panoView;
            parentContainerId = options.parentContainerId;

            options.cb( self );

        },

        render: function()
        {

            var self = this;

            if( this.$el.html() === "" )
            {
                // hacer esto permite separar la logica del init al render
                // y poder meter un div contenedor en el template padre.
                self.setElement( $( parentContainerId ) );
            }


            self.$el.html(
                self.template({
                    'pano': self.panoView.model
                })
            );

            return this;
        },

        postRender: function()
        {
            var self = this;
            // una vez que el dom está creado inicializamos SocialShareKit
            $( "meta[property='og\\:url']" ).attr( "content", window.location.href );
            $( "meta[property='og\\:title']" ).attr( "content", "my new title" );
            $( "meta[property='og\\:description']" ).attr( "content", "my new title" );
            // TODO: Calcular el path del thumbnail correctamente
            $( "meta[property='og\\:image\\:url']" ).attr( "content", "https://disismy.com/rawImage/1890721554/" );
            $( "meta[property='og\\:image\\:secure_url']" ).attr( "content", "https://disismy.com/rawImage/1890721554/" );
            $( "meta[property='og\\:image\\:type']" ).attr( "content", "image/jpeg" );
            $( "meta[property='og\\:image\\:width']" ).attr( "content", "700" );
            $( "meta[property='og\\:image\\:height']" ).attr( "content", "400" );


            SocialShareKit.init();

            // TODO: Por ahora lo dejo aqui, pero tienes que ser capaz de pasar
            // esto al apartado de events
            $( "#copyPanoLink" )[ 0 ].addEventListener( globals.touchOrClick, function()
            {
                globals.copyToClipboard( $( "#panoLinkTA" ), $( this ), textAreaCopied );
            }, false );

            $( "#copyEmbedPanoLinkTA" )[ 0 ].addEventListener( globals.touchOrClick, function()
            {
                globals.copyToClipboard( $( "#embedPanoLinkTA" ), $( this ), textAreaCopied );
            }, false );

            function textAreaCopied( buttonEl, isCopied )
            {
                if( isCopied )
                {
                    buttonEl.html( Disismy.Symfony.extra.copiedTxt );
                    // buttonEl.attr( "disabled", "disabled" );
                    setTimeout(
                        function()
                        {
                            // buttonEl.removeAttr( "disabled" );
                            buttonEl.html( Disismy.Symfony.extra.copyTxt );
                        }, 2000 );
                }
            }
        },

        unload: function()
        {
            var self = this;

            _.each( self._views, function( view )
            {
                view.unload();
            });

            $( 'body' ).removeClass( 'sharingModCss' );

            this.$el.empty();

            this.undelegateEvents();
        },

        events:
        {
            "click textarea": function( e )
            {
                var $target = $( e.currentTarget );
                $target.focus();
                $target.select();
            },
        },
    });

    return SharingModView;

});
