"use strict";
define( [ 'jquery', 'three', 'globals' ], function ( $, THREE, globals )
{
    var StaticCamera = function( pano_, initialPanoNodeLon_, initialPanoNodeLat_ )
    {
        var that = this;
        var pano = pano_;
        var prevLon = 0, lon = 0, prevLat = 0, lat = 0;
        var minZoomFOV        = 30;
        var maxZoomFOV        = 90;
        var nextZoomFOV       = 75;
        var camSpeed          = 0;
        this.target = new THREE.Vector3( 0, 0, 0 );

        this.camera = new THREE.PerspectiveCamera(
            nextZoomFOV, // fov
            pano.getWindowInnerWidth() / pano.getWindowInnerHeight(), // aspect
            1, // near
            501 ); // far

        this.camera.target = this.target;


        this.setNewCamSpeedFromInnerW = function( innerW_ )
        {
            var maxSpeed = 0.5;
            var minSpeed = 0.2;
            var maxSpeedAtWidth = 0;
            var minSpeedAtWidth = 1000;

            var newCamSpeed = maxSpeed - ( ( maxSpeed - minSpeed ) / minSpeedAtWidth ) * innerW_;

            newCamSpeed = newCamSpeed > maxSpeed ? maxSpeed : newCamSpeed;
            newCamSpeed = newCamSpeed < minSpeed ? minSpeed : newCamSpeed;

            camSpeed = newCamSpeed;
            return camSpeed;

        }
        this.setNewCamSpeedFromInnerW( pano.getWindowInnerWidth() );

        this.getCamSpeed = function()
        {
            return camSpeed;
        }



        this.getPrevLon = function()
        {
            return prevLon;
        }

        this.getLon = function()
        {
            return lon;
        }

        this.setLon = function( newLon )
        {
            prevLon = lon;
            lon = newLon;
        }

        this.getPrevLat = function()
        {
            return prevLat;
        }

        this.getLat = function()
        {
            return lat;
        }

        this.setLat = function( newLat )
        {
            prevLat = lat;
            lat = Math.max( - 85, Math.min( 85, newLat ) );
        }




        this.decelerate = function()
        {
            var decelerateScalar = 0.85; // en cada iteración bajamos el 15%
            var speedVect = new THREE.Vector2( that.getLon() - that.getPrevLon(), that.getLat() - that.getPrevLat() );
            var umbral = 0.05;

            // console.error( ">>> EMPEZAMOS: prevLon: ", that.getPrevLon(), ", lon: ", that.getLon(), ", prevLat: ", that.getPrevLat(), ", lat: ", that.getLat(), ", speedVect: ", speedVect );

            var intervalHandle = setInterval( function()
            {
                if( Math.abs( speedVect.x ) <= umbral
                    && Math.abs( speedVect.y ) <= umbral )
                {
                    // console.error( "<<< FINALIZAMOS: speedVect: ", speedVect );
                    clearInterval( intervalHandle );
                }
                else
                {
                    speedVect.x *= decelerateScalar;
                    speedVect.y *= decelerateScalar;
                    that.setLon( that.getLon() + speedVect.x );
                    that.setLat( that.getLat() + speedVect.y );
                }
            }, 10 ); // milliseconds
        }



        this.setLon( initialPanoNodeLon_ );
        this.setLat( initialPanoNodeLat_ );


        this.zoom = function( wheelDeltaY_ )
        {
            nextZoomFOV = that.camera.fov - wheelDeltaY_ * 0.1;
            if( nextZoomFOV < minZoomFOV ) nextZoomFOV = minZoomFOV;
            if( nextZoomFOV > maxZoomFOV ) nextZoomFOV = maxZoomFOV;

            if( globals.tweener.isPlaying() )
            {
                globals.tweener.stop();
            }
            globals.tweener.tween({
                from: { 'x': that.camera.fov },
                to:       { 'x': nextZoomFOV },
                duration: 300,
                easing: 'easeOutQuart',
                step: function( state )
                {
                    that.camera.fov = state.x;
                    that.camera.updateProjectionMatrix();
                },
                finish: function( state )
                {
                    that.camera.updateProjectionMatrix();
                }
            });

        }



        function updateTarget()
        {
            var targetPos = pano.getPointFromLonAndLat( that.getLon(), that.getLat() );

            that.target.x = targetPos.x;
            that.target.y = targetPos.y;
            that.target.z = targetPos.z;
        }

        this.update = function( isInGyroscopeMode, hasInteractedAlready )
        {
            // aqui updateamos simplemente los valores de prevLon y prevLat en el caso
            // de que el usuario esté interactuando (ha hecho press pero aún no ha hecho
            // release ) pero no se esté moviendo
            if( pano.isUserInteracting )
            {
                // si ya tienen el mismo valor no hace falta lanzar el intervalo
                if( that.getPrevLon() === that.getLon() && that.getPrevLat() === that.getLat() )
                {
                    // console.error( "1. prevs ya updateads por lo que no hacemos nada" );
                }
                // como su valor es distinto, y estan interactuando, hay que updatear los valores
                // para prevLon y prevLat... los de lon y lat se modifican en el mousemove, pero si
                // resulta que el usuario estaba moviendo pero se queda un rato parado, los prev no se
                // updatean.
                else
                {
                    // lo detectamos, pero con un pequeño margen de 10 milliseconds porque si no
                    // a veces no decelera debido a que se cambia esto antes de que empiece el decelerado
                    // q, en definitiva, depende de cuándo hagamos release en el mouse ( o touch end ).
                    setTimeout(function()
                    {
                        // si ya tienen el mismo valor no hace falta lanzar el intervalo
                        if( that.getPrevLon() === that.getLon() && that.getPrevLat() === that.getLat() )
                        {
                            // console.error( "2. prevs ya updateads por lo que no hacemos nada" );
                        }
                        else
                        {
                            // console.error( "Updateamos los prevs" );
                            // aunque parezca una bobada, es xa que se updateen los prevLon y prevLat
                            // sin tener que exponer sus setters
                            that.setLon( that.getLon() );
                            that.setLat( that.getLat() );
                        }
                    }, 300 ); // milliseconds


                }

            }

            updateTarget();

            if( isInGyroscopeMode )
            {

            }
            else
            {
                if( pano.isUserInteracting === false && ! hasInteractedAlready )
                {
                    that.setLon( that.getLon() + 0.06 );
                }

                that.camera.lookAt( that.target );

            }
        }


    }

    return StaticCamera;

});
