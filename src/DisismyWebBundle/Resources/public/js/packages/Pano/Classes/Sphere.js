"use strict";
define( [
    'three'
], function( THREE )
{
    var Sphere = function()
    {

        var that = this;

        this.RADIUS = 500;


        this.material = new THREE.MeshBasicMaterial();
        var geometry = new THREE.SphereGeometry( this.RADIUS, 60, 40 );
        // xa q la textura se vea desde dentro:
        geometry.scale( - 1, 1, 1 );

        this.mesh = new THREE.Mesh( geometry, this.material );
        this.mesh.tag = "Sphere";
        this.mesh.name = "Sphere";

        this.setTexture = function( texture )
        {
            that.material.map = texture;
            that.material.needsUpdate = true;
        }

        return this;

    }

    return Sphere;

});
