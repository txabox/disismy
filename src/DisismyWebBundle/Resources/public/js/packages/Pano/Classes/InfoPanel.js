"use strict";
define( [ 'jquery', 'globals' ], function ( $, globals )
{
    var InfoPanel = function( pano_, isEmbeded_ )
    {
        var that = this;

        var pano = pano_;
        var $el = $( "#infoPanel" );
        var panoInfoBtn = $( "#infoPanel>.container>button" );

        if( isEmbeded_ ) $el.hide();

        globals.preventEventPropagation( $el, [ 'mousewheel', 'MozMousePixelScroll' ] );


        panoInfoBtn.bind( 'touchstart click', function()
        {
            that.hide();
            pano.setIsZoomable( true );
        });


        this.show = function( cb )
        {
            // en embeded mode no mostramos la pantalla inicial
            if( isEmbeded_ )
            {
                cb && cb();
            }
            else
            {
                $el.fadeIn( 300, function()
                {
                    cb && cb();
                });
            }
        }

        this.hide = function( cb )
        {
            $el.fadeOut( 150, function()
            {
                pano.onInitialInfoPanelHidden();
                cb && cb();
            });
        }

        return this;

    }


    return InfoPanel;

});
