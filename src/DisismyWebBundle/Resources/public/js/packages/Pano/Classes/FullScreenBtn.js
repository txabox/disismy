"use strict";
define( [ 'jquery', 'globals' ], function ( $, globals )
{
    var FullScreenBtn = function( pano_, isEmbeded_ )
    {
        var that = this;

        var pano = pano_;

        var $el = $( "#fullscreenBtn" );

        if( isEmbeded_ ) $el.hide();


        globals.preventEventPropagation( $el, [ 'mousewheel', 'MozMousePixelScroll' ] );
        $el[ 0 ].addEventListener( globals.touchOrClick, pano.toggleFullScreen, false );

        return this;

    }

    return FullScreenBtn;

});
