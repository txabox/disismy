"use strict";
define( [ 'jquery', 'globals' ], function ( $, globals )
{
    var Notifications = function( pano_ )
    {
        var that = this;
        var pano = pano_;
        var WAITING_TIME = 2200;

        var jQueryEl = $( "#panoView>.notifications" );
        globals.preventEventPropagation( jQueryEl );

        this.show = function( msg )
        {
            jQueryEl.html( msg )
            .delay( 300 )
            .animate( {top: 0}, 300, function()
            {
                $( this )
                .delay( WAITING_TIME )
                .animate( {top: -55 }, 300 );
            });
        }

        function hide()
        {
            jQueryEl.animate( {top: -55 }, 150 );
        }

        this.showSuccess = function()
        {

        }

        this.showInfo = function()
        {

        }

        this.showWarning = function()
        {

        }

        this.showDanger = function()
        {

        }

        return this;
    }


    return Notifications;

});
