"use strict";
define( [
    'jquery',
    'three'
], function ( $, THREE )
{
    var Button = function( pano_, Camera_, btnJSON )
    {
    	var that = this;

    	var pano = pano_;
        var Camera = Camera_;

    	this.id = btnJSON ? btnJSON.id : -1;
    	this.name = btnJSON ? btnJSON.name : -1;
    	this.target = btnJSON ? btnJSON.target : -1;
        this.isHovered = false;
        var lon = btnJSON ? btnJSON.p.lon : -1;
        var lat = btnJSON ? btnJSON.p.lat : -1;
        var posVect = pano.getPointFromLonAndLat( lon, lat );


    	this.transform = new THREE.Object3D();
    	this.transform.position.x = posVect.x;
    	this.transform.position.y = posVect.y;
    	this.transform.position.z = posVect.z;

    	this.transform.owner = this;


        var circleGeo = new THREE.CircleGeometry(
            30, // radius
            32, // segments
            0, // thetaStart
            Math.PI * 2 ); // thetaLength
        var circleText = new THREE.TextureLoader().load( Disismy.Symfony.BASE_URL + "/rawImage/1809966608/" );
        var circleMat = new THREE.MeshBasicMaterial( {map: circleText, color: 0xffffff} );
        this.circleMesh = new THREE.Mesh( circleGeo, circleMat );
        this.circleMesh.position.set( 0, 0, 3.1 );
        // this.circleMesh.rotateX( Math.PI / 2 );



        // el transform es xq es el objeto que se mete en el scene
        // y el circleMesh porque es contra lo que colisiona el raycast
    	this.transform.tag = this.circleMesh.tag = "Button";



        this.transform.add( this.circleMesh );



    	this.setColor = function( newColor )
    	{
    		this.circleMesh.material.color.setHex( newColor );
    	}

        this.onHover = function()
        {
            this.setColor( 0xa0a0a0 );
            this.isHovered = true;
        }

        this.onHoverOut = function()
        {
            this.setColor( 0xffffff );
            this.isHovered = false;
        }

    	this.onClick = function()
    	{
    		// Disismy.Symfony.DEBUG && console.log( "Boton clickado! id [ " + that.id + " ], name[ " + that.name + " ], target[ " + that.target + " ]" );
    		this.setColor( 0x99ca3c );
            pano.loadPanoNode( that.target, that.id );
    	}

    	this.update = function()
    	{
    		this.transform.lookAt( Camera.camera.position );
    	}

    	this.setColor( 0xFFFFFF );

    	return this;
    }


    return Button;

});
