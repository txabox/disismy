"use strict";
define( [ 'three' ], function ( THREE )
{
    var Nadir = function( pano_, Camera_, isEmbeded_ )
    {
    	var that = this;

    	var pano = pano_;
        var Camera = Camera_;
        var isEmbeded = isEmbeded_;


        that.name = "nadir";
        that.isHovered = false;
        var lon = 200;
        var lat = -85;
        var radius = 75;
        var posVect = pano.getPointFromLonAndLat( lon, lat );
        var IMG_ROTATION_OFFSET = 1.30;


    	that.transform = new THREE.Object3D();
    	that.transform.position.x = posVect.x;
    	that.transform.position.y = posVect.y;
    	that.transform.position.z = posVect.z;

    	that.transform.owner = this;


        var circleGeo = new THREE.CircleGeometry(
            radius, // radius
            32, // segments
            0, // thetaStart
            Math.PI * 2 ); // thetaLength
        var circleText = new THREE.TextureLoader().load( Disismy.Symfony.BASE_URL + "/rawImage/1415924901/" );
        var circleMat = new THREE.MeshBasicMaterial( { map: circleText, color: 0xffffff } );
        that.circleMesh = new THREE.Mesh( circleGeo, circleMat );
        that.circleMesh.position.set( 0, 0, radius / 10 );
        // that.circleMesh.rotateZ( Math.PI / 2 );



        // el transform es xq es el objeto que se mete en el scene
        // y el circleMesh porque es contra lo que colisiona el raycast
    	that.transform.tag = that.circleMesh.tag = "Nadir";



        that.transform.add( that.circleMesh );



    	that.setColor = function( newColor )
    	{
    		that.circleMesh.material.color.setHex( newColor );
    	}

        that.onHover = function()
        {
            if( isEmbeded )
            {
                that.setColor( 0xa0a0a0 );
                that.isHovered = true;
            }
        }

        that.onHoverOut = function()
        {
            if( isEmbeded )
            {
                that.setColor( 0xffffff );
                that.isHovered = false;
            }
        }

        that.onClick = function()
        {
            if( isEmbeded )
            {
                window.open( Disismy.Symfony.BASE_URL + '/' + pano.getObject().type.name + '/' + pano.getObject().encodedId, '_blank' );
            }
        }


        that.setColor( 0xFFFFFF );



        this.update = function()
        {
            that.transform.lookAt( Camera.camera.position );
            that.circleMesh.rotation.z = Camera.camera.rotation.y - IMG_ROTATION_OFFSET;
            // console.error( "nadir.rotation.z: ", that.circleMesh.rotation.z );
        }

        return this;
    }

    return Nadir;

});
