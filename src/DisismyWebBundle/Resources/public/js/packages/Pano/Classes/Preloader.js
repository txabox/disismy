// 'bootstrap' does not return an object. Must appear at the end
define([ 'jquery', 'globals' ], function( $, globals )
{
    "use strict";
    var Preloader = function( elemIdentifier )
    {
        // Disismy.Symfony.DEBUG && console.log( "PRELOADER: new Preloader. elemIdentifier: ", elemIdentifier );
        var that = this;
        var container;
        var isBckgImgLoaded, isFrontImgLoaded, areBothImgsLoaded = false;
        var imgW, imgH, imgW_DIV2, imgH_DIV2;
        var preloaderBckg, preloaderTitle, preloaderCanvas, ctx;
        var containerId         = "disismyPreloader";
        var preloaderBckgId     = "preloaderBckg";
        var preloaderTitleId    = "preloaderTitle";
        var preloaderCanvasId   = "preloaderCanvas";
        var isLoaded = false;
        var isShowing = false;
        var isHiding = false;
        var bckgImg, frontImg;

        function load( cb )
        {
            // Disismy.Symfony.DEBUG && console.log( "PRELOADER. preloaderLoad" );

            /**************************************************************************/
            /* 1º Cremamos el contenedor disismyPreloader xa evitar problemas de      */
            /* namespaces                                                             */
            /**************************************************************************/
            container = createElementIfNotExists( elemIdentifier, containerId, setContainerProperties );

            /**************************************************************************/
            /* 2º Cremamos el preloaderBckg                                           */
            /**************************************************************************/
            preloaderBckg = createElementIfNotExists( "#" + containerId, preloaderBckgId, setPreloaderBckgProperties );


            /**************************************************************************/
            /* 3º Cremamos el preloaderTitle                                          */
            /**************************************************************************/
            preloaderTitle = createElementIfNotExists( "#" + containerId, preloaderTitleId, setPreloaderTitleProperties, "p" );
            preloaderTitle.html( "Loading..." );


            /**************************************************************************/
            /* 4º Cremamos las imagenes del preloader                                 */
            /**************************************************************************/
            bckgImg = document.createElement( 'IMG' );
            frontImg = document.createElement( 'IMG' );

            // Specify the src to load the image
            bckgImg.src = Disismy.Symfony.BASE_URL + "/rawImage/18617295/";
            frontImg.src = Disismy.Symfony.BASE_URL + "/rawImage/613895610/";

            bckgImg.onload = function ()
            {
                isBckgImgLoaded = true;
                if( isFrontImgLoaded )
                {
                    bothImgsLoaded();
                }
            }

            bckgImg.onerror = function()
            {
                console.error( "Error cargando bckgImg" );
            }

            frontImg.onload = function ()
            {
                isFrontImgLoaded = true;
                if( isBckgImgLoaded )
                {
                    bothImgsLoaded();
                }
            }
            frontImg.onerror = function()
            {
                console.error( "Error cargando frontImg" );
            }



            function bothImgsLoaded()
            {

                areBothImgsLoaded = true;


                imgW = bckgImg.width;
                imgH = bckgImg.height;
                imgW_DIV2 = imgW / 2;
                imgH_DIV2 = imgH / 2;


                /**************************************************************************/
                /* 5º Una vez cargadas ambas imagens, cremamos preloaderCanvas,           */
                /* el canvas que contiene la animacion de las imagenes.                   */
                /**************************************************************************/
                preloaderCanvas = createElementIfNotExists( "#" + containerId, preloaderCanvasId, setPreloaderCanvasProperties, "canvas" );
                ctx = preloaderCanvas[ 0 ].getContext( "2d" );


                /**************************************************************************/
                /* 6º Devolvemos que el preloader está cargado                            */
                /**************************************************************************/
                isLoaded = true;
                // Disismy.Symfony.DEBUG && console.log( "PRELOADER. preloaderLoaded" );
                cb();


            }


            function createElementIfNotExists( parentContainerId_, elemId_, setStylesFn_, newElType_ )
            {

                var newElement;
                var newElType = ( typeof newElType_ === 'undefined' ) ? "div" : newElType_;

                if( $( parentContainerId_ + ">#" + elemId_ ).length )
                {
                    // console.log( "como #preloaderBckg SÍ existía, simplemente lo asignamos" );
                    // simplemente asignamos su valor
                    newElement = $( parentContainerId_ + " #" + elemId_ );
                }
                // si no existía previamente
                else
                {
                    // console.log( "como #preloaderBckg NO existía, lo creamos y asignamos" );
                    // generamos el div que sale por detras
                    $( parentContainerId_ ).append( "<" + newElType + " id='" + elemId_ + "'></" + newElType + ">" );
                    newElement = $( parentContainerId_ + " #" + elemId_ );
                    setStylesFn_( newElement );
                }

                return newElement;

            }



        }





        this.show = function( cb_ )
        {

            var cb = cb_;

            if( ! isLoaded )
            {
                // Disismy.Symfony.DEBUG && console.log( "PRELOADER: preloader.show(). Como no estaba cargado, lo cargamos" );
                load( function()
                {
                    // Disismy.Symfony.DEBUG && console.log( "PRELOADER: preloader.show(). El preloader ya está cargado, nos disponemos a mostrarlo." );
                    that.show( cb );
                });
            }
            // como ya está cargado simplemente lo enseñamos
            else
            {
                // hay q llamar a update para que se pinte
                this.update( 0 );
                // si no están visibles
                if( ! preloaderBckg.is( ":visible" ) ||
                    ! preloaderCanvas.is( ":visible" ) )
                {
                    // ni están en proceso de ser visibles
                    if( ! isShowing )
                    {
                        // Disismy.Symfony.DEBUG && console.log( "PRELOADER: preloader.show()" );
                        // el isShowing bloquea el isHiding
                        isHiding = false;
                        isShowing = true;
                        // console.log( "preloader.show()" );


                        preloaderBckg.fadeIn( preloaderBckg.is( ":visible" ) ? 0 : 300, function()
                        {
                            // Disismy.Symfony.DEBUG && console.log( "PRELOADER. mostramos preloaderBckg" );
                            preloaderTitle.fadeOut( preloaderTitle.is( ":visible" ) ? 0 : 100, function()
                            {
                                // Disismy.Symfony.DEBUG && console.log( "PRELOADER. ocultamos preloaderTitle" );
                                $( "#preloaderCanvas" ).fadeIn( 300, function()
                                {
                                    // Disismy.Symfony.DEBUG && console.log( "PRELOADER. mostramos preloaderCanvas" );
                                    isShowing = false;
                                    // Disismy.Symfony.DEBUG && console.log( "PRELOADER. preloaderShown" );
                                    cb && cb();
                                });

                            });
                        });

                    }
                }

            }


        }

        this.hide = function( cb_ )
        {

            var cb = cb_;

            if( preloaderBckg.is( ":visible" ) ||
                preloaderCanvas.is( ":visible" ) )
            {
                if( ! isHiding )
                {
                    // Disismy.Symfony.DEBUG && console.log( "PRELOADER: preloader.hide()" );
                    isHiding = true;
                    // console.log( "preloader.hide()" );

                    preloaderBckg.fadeOut( 100 );
                    preloaderCanvas.fadeOut( 100, function()
                    {
                        isHiding = false;
                        // Disismy.Symfony.DEBUG && console.log( "PRELOADER. preloaderHidden" );
                        cb && cb();
                    } );
                }
            }
        }


        this.update = function( curPercentile )
        {

            if( ! areBothImgsLoaded )
            {
                return;
            }



            ctx.clearRect( 0, 0, imgW, imgH);


            // Save the state, so we can undo the clipping
            ctx.save();

            ctx.drawImage( bckgImg, 0, 0 );




            ctx.beginPath();

            // console.error( "entramos aqui: ", curPercentile, imgH - Math.floor( imgH * curPercentile ) )

            // Clip a rectangular area
            ctx.rect( 0, imgH - Math.floor( imgH * curPercentile ), imgW, imgH );
            // ctx.stroke();



            // Clip to the current path
            ctx.clip();


            ctx.drawImage( frontImg, 0, 0 );




            // Undo the clipping
            ctx.restore();



        }



        function setContainerProperties( element )
        {}

        function setPreloaderBckgProperties( element )
        {
            element.css({
                "z-index": container.css( "z-index" ),
                "height": "100%",
                "width": "100%",
                "position": "absolute",
                "background-color": "rgba(0, 0, 0, 0.9)",
                "top": "0",
                "left": "0",
                "cursor": "auto",
                "display": "inline"
            });

            globals.preventEventPropagation( element, [ 'mousewheel', 'MozMousePixelScroll' ]  );

        }

        function setPreloaderTitleProperties( element )
        {
            element.css({
                "color": "white",
                "position": "absolute",
                "top": "50%",
                "left": "0",
                "text-align": "center",
                "width": "100%",
                "cursor": "auto"
            });
        }

        function setPreloaderCanvasProperties( element )
        {
            element.css({
                "z-index": $( "#preloaderBckg" ).css( "z-index" ),
                "position": "absolute",
                "width": imgW,
                "height": imgH,
                "left": "-webkit-calc(50% - " + imgW_DIV2 + "px)",
                "left": "-moz-calc(50% - " + imgW_DIV2 + "px)",
                "left": "calc(50% - " + imgW_DIV2 + "px)",
                "top": "-webkit-calc(50% - " + imgH_DIV2 + "px)",
                "top": "-moz-calc(50% - " + imgH_DIV2 + "px)",
                "top": "calc(50% - " + imgH_DIV2 + "px)",
                "cursor": "auto",
                "display": "none"
            });

            element[ 0 ].width = imgW;
            element[ 0 ].height = imgH;

            globals.preventEventPropagation( element, [ 'mousewheel', 'MozMousePixelScroll' ]  );

        }








        return this;

    }

    return Preloader;

});
