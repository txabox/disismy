"use strict";
define( [ 'jquery', 'globals' ], function ( $, globals )
{
    var ShowMoreLessBtn = function( isEmbeded_ )
    {
        var that = this;

        this.jQueryEl = $( "#showMoreLessBtn" );
        var showInfoEl = $( '#showInfo' );
        var showPanoEl = $( '#showPano' );

        if( isEmbeded_ ) this.jQueryEl.hide();

        var isMovingBecauseOfBtn = false;

        globals.preventEventPropagation( this.jQueryEl, [ 'mousewheel', 'MozMousePixelScroll' ] );

        this.jQueryEl[ 0 ].addEventListener( globals.touchOrClick, onShowMoreOrLessClick, false );


        function onShowMoreOrLessClick( e )
        {

            isMovingBecauseOfBtn = true;
            if( showInfoEl.is( ':visible' ) )
            {

                $( 'html, body' ).animate(
                    {
                        scrollTop: that.jQueryEl.offset().top - 60
                    },
                    1000,
                    function()
                    {
                        showPanoButton();
                        isMovingBecauseOfBtn = false;
                    });
            }
            else
            {
                $( 'html, body' ).animate(
                    { scrollTop: 0 },
                    1000,
                    function()
                    {
                        showInfoButton();
                        isMovingBecauseOfBtn = false;
                    });
            }
        }



        function showInfoButton()
        {
            if( ! showInfoEl.is( ':visible' ) )
            {
                showInfoEl.css( 'display', 'inline' );
                showPanoEl.css( 'display', 'none' );
            }
        }

        function showPanoButton()
        {
            if( ! showPanoEl.is( ':visible' ) )
            {
                showInfoEl.css( 'display', 'none' );
                showPanoEl.css( 'display', 'inline' );
            }
        }


        function onWindowScroll()
        {
            if( ! isMovingBecauseOfBtn )
            {
                if( $( window ).scrollTop() == 0 )
                {
                    showInfoButton();
                }
                else
                {
                    showPanoButton();
                }
            }
        }

        this.show = function( cb )
        {
            // en embeded mode no mostramos la pantalla inicial
            if( isEmbeded_ )
            {
                cb && cb();
            }
            else
            {
                that.jQueryEl.fadeIn( 300, function()
                {
                    cb && cb();
                });
            }
        }
        this.hide = function( cb )
        {
            that.jQueryEl.fadeOut( 150, function()
            {
                if( cb ) cb();
            });
        }

        this.isVisible = function()
        {
            return that.jQueryEl.is( ":visible" );
        }


        window.addEventListener( "scroll", onWindowScroll );

        return this;

    }


    return ShowMoreLessBtn;

});
