"use strict";
define( [ 'jquery' ], function ( $ )
{
    var Debugger = function( pano_, Camera_, buttonsMgr_ )
    {

        var that = this;
        var pano = pano_;
        var Camera = Camera_;


        var fooPointer, fooTarget;

        if( Disismy.Symfony.ENV === "dev" )
        {
            fooPointer = buttonsMgr_.createNewBtn();
            fooPointer.transform.tag = fooPointer.circleMesh.tag = "Pointer";
            fooPointer.setColor( 0xFFFF00 );
            fooPointer.circleMesh.scale.set( 0.2, 0.2, 0.2 );


            fooTarget = buttonsMgr_.createNewBtn();
            fooTarget.transform.tag = fooTarget.circleMesh.tag = "Target";
            fooTarget.setColor( 0xFF00FF );
            fooTarget.circleMesh.scale.set( 0.2, 0.2, 0.2 );
        }

        this.updateFooPointerPos = function( x, y, z )
        {
            if( fooPointer )
            {
                fooPointer.transform.position.x = x;
                fooPointer.transform.position.y = y;
                fooPointer.transform.position.z = z;
            }
        }

        function updateFooTargetPos()
        {
            if( fooTarget )
            {
                fooTarget.transform.position.x = Camera.target.x;
                fooTarget.transform.position.y = Camera.target.y;
                fooTarget.transform.position.z = Camera.target.z;
            }
        }

        this.echo = function( msg )
        {
            if( Disismy.Symfony.ENV === "dev" )
            {
                $( "#debugTxt" ).html( msg );
            }
        }

        this.update = function()
        {
            updateFooTargetPos();
        }

        return this;

    }


    return Debugger;

});
