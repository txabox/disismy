"use strict";
define( [ 'jquery', 'globals' ], function ( $, globals )
{
    var DeviceController = function( pano_, isEmbeded_ )
    {
        var that = this;
        var wasGyroscopeDetected = false;
        this.isInGyroscopeMode = false;

        var pano = pano_;

        var jQueryEl = $( "#deviceControlBtn" );
        var gyroscopeSpan = $( "#gyroscopeCtrlSpan" );
        var touchSpan = $( "#touchCtrlSpan" );


        globals.preventEventPropagation( jQueryEl );
        jQueryEl[ 0 ].addEventListener( globals.touchOrClick, onClick, false );

        function onClick( e )
        {
            // pasamos de gyroscope a touch
            if( that.isInGyroscopeMode )
            {
                touchSpan.hide();
                gyroscopeSpan.show();

                pano.onGyroscopeToTouch();

                that.isInGyroscopeMode = false;
            }
            // pasamos de touch a gyroscope
            else
            {
                gyroscopeSpan.hide();
                touchSpan.show();
                that.isInGyroscopeMode = true;
            }
        }




        if( window.DeviceOrientationEvent )
        {
            window.addEventListener( "deviceorientation", function( event )
            {
                onGyroscopeDetected( [ event.beta, event.gamma ] );
            }, true );
        }
        else if( window.DeviceMotionEvent )
        {
            window.addEventListener( 'devicemotion', function( event )
            {
                onGyroscopeDetected( [ event.acceleration.x * 2, event.acceleration.y * 2 ] );
            }, true);
        }
        else
        {
            window.addEventListener( "MozOrientation", function( event )
            {
                onGyroscopeDetected( [ event.x * 50, event.y * 50 ] );
            }, true );
        }

        function onGyroscopeDetected( data )
        {
            if( ! wasGyroscopeDetected &&
                data[ 0 ] &&
                data[ 1 ] )
            {
                gyroscopeSpan.show( 1, function(){
                    jQueryEl.fadeIn( 300 );
                });
                /*
                hasta que no mejoremos el gyroscopeMode, se queda en false
                aunque el dispositivo tenga gyroscope.
                touchSpan.show( 1, function(){
                    jQueryEl.fadeIn( 300 );
                });
                that.isInGyroscopeMode = true;
                */

            }
            // tenga o no tenga ya hemos realizado la detección, para que no sigua ejecutandose
            wasGyroscopeDetected = true;

        }

        return this;

    }


    return DeviceController;

});
