"use strict";

define( [
    'jquery',
    'three',
    './Button'
], function ( $, THREE, Button )
{
    var ButtonsManager = function( pano_, Camera_, isEmbeded_ )
    {
        var that = this;
    	var buttonsList = [];
    	var pano = pano_;
        var Camera = Camera_;
        var raycaster = new THREE.Raycaster();
    	// obj que asocia el id con el boton
    	this.buttons = {};
        var prevHoveredBtn = undefined;




        this.createNewBtn = function( btnJSON )
        {
            buttonsList.push( new Button( pano, Camera, btnJSON ) );

            var newBtn = buttonsList[ buttonsList.length - 1 ];
            if( btnJSON )
            {
                that.buttons[ "" + btnJSON.id ] = newBtn;
            }

            pano.getScene().add( newBtn.transform );
            return newBtn;

        }

        this.createNodeButtons = function( nodeData, cb )
        {

            // Disismy.Symfony.DEBUG && console.log( "6. createNodeButtons", nodeData );

            for( var i = 0; i < nodeData.buttons.length; ++i )
            {
                // // Disismy.Symfony.DEBUG && console.log( nodeData.buttons[ i ] );
                this.createNewBtn( nodeData.buttons[ i ] );
            }

            cb();
        }

        this.removeNodeButtons = function()
        {
            // Disismy.Symfony.DEBUG && console.log( "1. removeNodeButtons" );

            that.buttons = {};

            for( var i = pano.getScene().children.length - 1; i >= 0; --i )
            {
                var obj = pano.getScene().children[ i ];
                if( obj.tag && obj.tag === "Button" )
                {
                    pano.getScene().remove( obj );
                }
            }
        }

        this.checkHover = function( mousePos )
        {

            var interesectedObj = undefined;
            raycaster.setFromCamera( mousePos, Camera.camera );

            var intersects = raycaster.intersectObjects(
                pano.getScene().children,
                true // recursively
                );

            if( intersects.length > 1 ) // uno es la esfera
            {
                // solo nos interesa el primer elemento ( el más cercano a la camera )
                var object = intersects[ 0 ].object;

                if(
                    object.tag === "Button" ||
                    // solo hacemos hover en el Nadir si está embebido
                    ( object.tag === "Nadir" && isEmbeded_ ) )
                {
                    // el obj en este caso es el mesh del transform
                    var newBtn = object.parent.owner;

                    // CASO 1. No había ningún btn hovereado
                    if( ! prevHoveredBtn )
                    {
                        // por lo que hovereamos el nuevo
                        // console.error( "Solo debería entrar una vez en onHover" );
                        newBtn.onHover();
                        prevHoveredBtn = newBtn;
                        $( "#" + pano.canvasElId ).removeClass( "grabCursor" ).addClass( "pointerCursor" );
                    }
                    // CASO 2. El boton previamente hovereado es el mismo que el nuevo
                    else if( prevHoveredBtn.id === newBtn.id )
                    {
                        // por lo que no hacemos nada
                    }
                    // CASO 3. El botón previamente hovereado es distinto que el nuevo
                    else
                    {
                        // console.error( "Solo debería entrar una vez en onHover" );
                        newBtn.onHover();
                        prevHoveredBtn = newBtn;
                    }

                    interesectedObj = object;
                }
            }
            else
            {
                if( prevHoveredBtn )
                {
                    // console.error( "Solo debería entrar una vez en onHoverOut" );
                    prevHoveredBtn.onHoverOut();
                    prevHoveredBtn = undefined;

                    $( "#" + pano.canvasElId ).removeClass( "pointerCursor" ).addClass( "grabCursor" );

                }
            }

            return interesectedObj;

        }

    	this.update = function()
    	{
    		for( var i = 0; i < buttonsList.length; ++i )
    		{
    			buttonsList[ i ].update();
    		}
    	}

    	return this;
    }

    return ButtonsManager;

});
