"use strict";

define( [
    'jquery',
    'backbone',
    '../Models/Pano',
], function ( $, Backbone, PanoModel )
{

    var PanosCollection =  Backbone.Collection.extend({
            url: '/panos/',
            model: PanoModel
        });


    return PanosCollection;

});
