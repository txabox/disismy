"use strict";

define( [
    'jquery',
    'three',
    'underscore',
    'backbone',
    'globals',
    'threeWebGLDetector',
    '../Classes/Preloader',
    '../Classes/FullScreenBtn',
    '../Classes/DeviceController',
    '../Classes/StaticCamera',
    '../Classes/ButtonsManager',
    '../Classes/InfoPanel',
    '../Classes/Sphere',
    '../Classes/Nadir',
    '../Classes/Notifications',
    '../Classes/ShowMoreLessBtn',
    '../Classes/Debugger',
    '../Classes/Button',
    'text!../Templates/panoTmpl.html',
    '../Collections/Pano',
    '../Models/Pano',
    '../Classes/Image',
    'threeCanvasRenderer', 'threeProjector',
    'threeDeviceOrientationControls',
], function ( $, THREE, _, Backbone, globals, Detector, Preloader, FullScreenBtn, DeviceController, StaticCamera, ButtonsManager, InfoPanel, Sphere, Nadir, Notifications, ShowMoreLessBtn, Debugger, Button, panoTmpl, PanosCollection, PanoModel )
{
    // https://github.com/mrdoob/three.js/blob/master/examples/webgl_panorama_equirectangular.html
    var $parentEl, isEmbeded, fullscreenBtn, deviceController, Camera, scene,
    renderer, controls, sphere, showMoreLessBtn, nadir, notifications,
    curPanoNode, curRoom, isAnimatedAlready, myDebugger, buttonsMgr,
    prevWindowInnerW, prevWindowInnerH, textureLoader, onPointerDownPointerX,
    onPointerDownPointerY, onPointerDownLon, onPointerDownLat, raycaster,
    mousePos, touchPos, preloader, INITIAL_REFEREE_BTN, infoPanel, isZoomable;

    var managers = [];

    var texture_placeholder,
    hasInteractedAlready = false,
    onMouseDownMouseX = 0, onMouseDownMouseY = 0,
    // giro izda-dcha
    onMouseDownLon = 0,
    // giro subir-bajar
    onMouseDownLat = 0;

    // los views tienen acceso al Model así:
    // 'this.model'
    // porque se le pasa cuando se crea como parametro
    var PanoView =  Backbone.View.extend({
        tagName:            'div',
        id:                 'panoView',
        template:           _.template( panoTmpl ),
        canvasElId:         'panoCanvas',
        isUserInteracting:  false,
        name:               'PanoView',

        initialize: function( options )
        {
            /**************************************************************/
            // 1º Definimos las variables básicas:
            /**************************************************************/
            var self = this;

            // private methods:
            _.bindAll( this, '_getPano', '_animate', '_update', '_checkResize',
            '_onWindowResize' );
            // public methods:
            _.bindAll( this, 'getPointFromLonAndLat', 'getWindowInnerWidth',
            'getWindowInnerHeight', 'toggleFullScreen',
            'loadPanoNode', 'onInitialInfoPanelHidden', 'onGyroscopeToTouch' );

            preloader = new Preloader( "#" + self.id );
            INITIAL_REFEREE_BTN = 0;

            this.collection = new PanosCollection;

            $parentEl = $( options.parentContainerId );
            isEmbeded = options.isEmbeded;
            self._parent = options.parent;
            isZoomable = isEmbeded ? true : false;

            isAnimatedAlready = false;

            raycaster = new THREE.Raycaster();
            mousePos = new THREE.Vector2();
            touchPos = new THREE.Vector2();


            this._getPano( options.encodedPanoId, function()
            {
                // initVars:

                self.render();



                fullscreenBtn = new FullScreenBtn( self, isEmbeded );
                deviceController = new DeviceController( self, isEmbeded );


                Camera = new StaticCamera( self, self.model.get( "initialPanoNodeLon" ), self.model.get( "initialPanoNodeLat" ) );

                scene = new THREE.Scene();

                controls = new THREE.DeviceOrientationControls( Camera.camera );


                buttonsMgr = new ButtonsManager( self, Camera, isEmbeded );
                managers.push( buttonsMgr );

                // instantiate a loader
                textureLoader = new THREE.TextureLoader();

                renderer = Detector.webgl ? new THREE.WebGLRenderer( { alpha: true } ) : new THREE.CanvasRenderer();

                renderer.setPixelRatio( window.devicePixelRatio );
                renderer.setSize( self.getWindowInnerWidth(), self.getWindowInnerHeight() );

                renderer.domElement.id = self.canvasElId;

                self.el.appendChild( renderer.domElement );


                infoPanel = new InfoPanel( self, isEmbeded );


                sphere = new Sphere();
                scene.add( sphere.mesh );

                nadir = new Nadir( self, Camera, isEmbeded );
                scene.add( nadir.transform );

                notifications = new Notifications( self );

                showMoreLessBtn = new ShowMoreLessBtn( isEmbeded );


                myDebugger = new Debugger( self, Camera, buttonsMgr );






                /**************************************************************/
                // 2º Cargar el json con los datos del Node
                /**************************************************************/
                self.loadPanoNode( self.model.get( 'initialPanoNodeEncodedId' ), INITIAL_REFEREE_BTN );


                options.cb && options.cb( self );
            });
        },

        _getPano: function( encodedPanoId, cb )
        {
            var self = this;
            this.model = new PanoModel({ 'id': encodedPanoId });
            this.model
                .fetch()
                .done( function( data )
                {
                    cb();
                })
                .fail( function( xhr, err )
                {
                    $.notify( "Data not retrieved: Unknown error. Please, check your browser console error log and try again later." );
                });
        },

        render: function()
        {
            var self = this;

            self.$el.append(
                self.template({
                    "pano": this.model,
                })
            );

            self.el.style.height = ( self.getWindowInnerHeight() - $( '.navbar-header' ).height() ) + "px";

            // solo lo añadimos si $( '#panoView' ) no existía previamente.
            if ( ! $( '#' + self.id ).length )
            {
                $parentEl.prepend( this.el );
            }

            return this;
        },

        postRender: function()
        {
        },

        unload: function()
        {
            var self = this;

            // TODO: Aquí hay que borrar de la memoria todo lo que se haya creado
            // mediante un new
            preloader = null, raycaster = null, mousePos = null, touchPos = null,
            fullscreenBtn = null, deviceController = null, Camera = null,
            scene = null, controls = null, buttonsMgr = null, textureLoader = null,
            renderer = null, infoPanel = null, sphere = null, nadir = null,
            notifications = null, showMoreLessBtn = null, myDebugger = null,
            self.collection = null, self.model = null;

            window.cancelAnimationFrame( Disismy.Pano.requestAnimationFrame );
            Disismy.Pano.requestAnimationFrame = null;
            Disismy.Pano.view = null;

            self.$el.empty();

            this.undelegateEvents();
        },

        events:
        {
            "mousedown" : "_onDocumentMouseDown",
            "mousemove": "_onDocumentMouseMove",
            "mouseup": "_onDocumentMouseUp",
            "mousewheel": "_onDocumentMouseWheel",
            "MozMousePixelScroll": "_onDocumentMouseWheel",
            "contextmenu": function( e ){ globals.preventPropagation( e ); },
            "touchstart": "_onDocumentTouchStart",
            "touchmove": "_onDocumentTouchMove",
            "touchend": "_onDocumentTouchEnd",
        },

        _onDocumentMouseDown: function( event )
        {
            var self = this;
            event.preventDefault();

            self.isUserInteracting = hasInteractedAlready = true;

            onPointerDownPointerX = event.clientX;
            onPointerDownPointerY = event.clientY;

            onPointerDownLon = Camera.getLon();
            onPointerDownLat = Camera.getLat();


            self._updateMousePos( event );
            self._checkRaycast( true );


        },

        _onDocumentMouseMove: function( event )
        {
            var self = this;

            if ( self.isUserInteracting === true )
            {
                Camera.setLon( ( onPointerDownPointerX - event.clientX ) * Camera.getCamSpeed() + onPointerDownLon );
                Camera.setLat( ( event.clientY - onPointerDownPointerY ) * Camera.getCamSpeed() + onPointerDownLat );
            }

            self._updateMousePos( event );
            buttonsMgr.checkHover( mousePos );

        },

        _onDocumentMouseUp: function( e )
        {

            var self = this;

            Camera.decelerate();
            self.isUserInteracting = false;

        },

        _onDocumentMouseWheel: function( event )
        {
            var self = this;
            if( isZoomable )
            {
                globals.preventPropagation( event );
                Camera.zoom( event.originalEvent.wheelDeltaY );
            }

        },

        _onDocumentTouchStart: function( event )
        {
            var self = this;

            var touches = ( event.touches || event.originalEvent.touches );

            if ( touches.length == 1 )
            {
                // Disismy.Symfony.DEBUG && console.log( "onDocumentTouchStart: [ " + touches[ 0 ].pageX + ", " + touches[ 0 ].pageY + " ]" );

                globals.preventPropagation( event );

                self.isUserInteracting = hasInteractedAlready = true;

                onPointerDownPointerX = touches[ 0 ].pageX;
                onPointerDownPointerY = touches[ 0 ].pageY;

                onPointerDownLon = Camera.getLon();
                onPointerDownLat = Camera.getLat();

                self._updateTouchPos( touches[ 0 ] );
                self._checkRaycast( false );

            }

        },

        _onDocumentTouchMove: function( event )
        {
            var self = this;

            var touches = ( event.touches || event.originalEvent.touches );

            // esto es para que no se pueda arrastrar en moviles y no salga la url bar
            // que hace que la experiencia
            event.preventDefault();

            if ( touches.length == 1 )
            {
                // Disismy.Symfony.DEBUG && console.log( "onDocumentTouchMove: [ " + touches[ 0 ].pageX + ", " + touches[ 0 ].pageY + " ]" );
                globals.preventPropagation( event );

                Camera.setLon( ( onPointerDownPointerX - touches[0].pageX ) * Camera.getCamSpeed() + onPointerDownLon );
                Camera.setLat( ( touches[0].pageY - onPointerDownPointerY ) * Camera.getCamSpeed() + onPointerDownLat );

                // updateTouchPos( touches[ 0 ] );
                // checkRaycast( false );

            }
            else if( touches.length == 2 )
            {
                globals.preventPropagation( event );
                // Disismy.Symfony.DEBUG && console.log( "Empezamos a hacer zoom" );
            }

        },

        _onDocumentTouchEnd: function( event )
        {
            var self = this;

            globals.preventPropagation( event );
            Camera.decelerate();
            self.isUserInteracting = false;

        },

        _updateMousePos: function( e )
        {
            var self = this;

            mousePos.x = ( ( e.clientX - self.$el.offset().left + $( document ).scrollLeft() ) / self.$el.width() ) * 2 - 1;
            mousePos.y = - ( ( e.clientY - self.$el.offset().top + $( document ).scrollTop() ) / self.$el.height() ) * 2 + 1;

        },

        _updateTouchPos: function( finger )
        {
            var self = this;
            // touchPos.x = ( finger.pageX / window.innerWidth ) * 2 - 1;
            // touchPos.y = - ( finger.pageY / window.innerHeight ) * 2 + 1
            // no tengo ni la menor idea de por qué no hay que tener en cuenta el scrollLeft y el scrollTop
            touchPos.x = ( ( finger.pageX - self.$el.offset().left /* + $( document ).scrollLeft() */ ) / self.$el.width() ) * 2 - 1;
            touchPos.y = - ( ( finger.pageY  - self.$el.offset().top /* + $( document ).scrollTop() */ ) / self.$el.height() ) * 2 + 1;

            // // Disismy.Symfony.DEBUG && console.log( "finger.pageY: ", finger.pageY, ", self.$el.offset().top: ", self.$el.offset().top, ", $( document ).scrollTop(): ", $( document ).scrollTop(), ", self.$el.height(): ", self.$el.height() )
            // $( "#debugTxt" ).html( "event.client: [ " + finger.pageX + ", " +  finger.pageY + " ]<br />touchPos: [ " + touchPos.x + ", " +  touchPos.y + " ]" );
        },

        _checkRaycast: function( isMouse )
        {
            var self = this;
            // update the picking ray with the camera and mousePos position
            if( isMouse )
            {
                raycaster.setFromCamera( mousePos, Camera.camera );
            }
            else
            {
                raycaster.setFromCamera( touchPos, Camera.camera );
            }

            // calculate objects intersecting the picking ray
            var intersects = raycaster.intersectObjects(
                scene.children,
                true // recursively
                );

            for ( var i = 0; i < intersects.length; ++i )
            {
                var intersection = intersects[ i ];
                var intersectionPoint = intersection.point;
                var collidingObj = intersects[ i ].object;

                myDebugger.updateFooPointerPos( intersectionPoint.x, intersectionPoint.y, intersectionPoint.z );



                if( collidingObj.tag === "Sphere" )
                {
                    /*
                    console.error( "Button pos on Sphere: ", self._getLonAndLatFromPoint(
                        intersection.point.x,
                        intersection.point.y,
                        intersection.point.z
                    ));
                    */
                }
                else if( collidingObj.tag === "Button" || collidingObj.tag === "Nadir" )
                {
                    // el obj en este caso es el mesh del transform
                    collidingObj.parent.owner.onClick();
                }

            }
        },

        _animate: function()
        {

            Disismy.Pano.requestAnimationFrame = requestAnimationFrame( this._animate );
            this._update();

        },

        _update: function()
        {

            var self = this;

            self._checkResize();

            if( deviceController.isInGyroscopeMode )
            {
                controls.update();
            }
            else
            {


            }


            myDebugger.update();

            nadir.update();

            for( var i = 0; i < managers.length; ++i )
            {
                managers[ i ].update();
            }

            Camera.update( deviceController.isInGyroscopeMode, hasInteractedAlready );

            myDebugger.echo( "lon: [ " + Camera.getLon() + " ]<br />lat: [ " + Camera.getLat() + " ]"  );

            renderer.render( scene, Camera.camera );






        },

        _checkResize: function()
        {
            var self = this;

            var innerW = self.getWindowInnerWidth();
            var innerH = self.getWindowInnerHeight();

            if( innerW != prevWindowInnerW ||
                innerH != prevWindowInnerH )
            {
                // // Disismy.Symfony.DEBUG && console.log( "Hay que hacer resize!" );
                self._onWindowResize();
            }

            prevWindowInnerW = innerW;
            prevWindowInnerH = innerH;

        },

        _onWindowResize: function()
        {

            var self = this;

            var scrollTop = $( document ).scrollTop();
            // // Disismy.Symfony.DEBUG && console.log( "scroll al entrar en onWindowResize, $( document ).scrollTop(): " + scrollTop );

            var innerW = self.getWindowInnerWidth();
            var innerH = self.getWindowInnerHeight();
            // // Disismy.Symfony.DEBUG && console.log( "onWindowResize: [ " + innerW + ", " + innerH + " ]" );

            Camera.camera.aspect = innerW / innerH;
            Camera.camera.updateProjectionMatrix();

            self.el.style.height = innerH + "px";
            // self.el.style.width = innerW + "px";

            renderer.setSize( innerW, innerH );


            // Como el usuario puede salir de fullscreen mediante metodos del propio
            // browser ( no a traves del boton que habilitamos para ello )
            // no tenemos manera de saber con certeza en los eventos del botón por lo
            // que hace falta hacer esto:
            if( globals.isInFullScreen() && showMoreLessBtn.isVisible() )
            {
                // // Disismy.Symfony.DEBUG && console.log( "Estamos en fullscreen y showmoreless estaba visible. Esto solo debe ocurrir una vez." )
                $( "#shrinkSpan" ).removeClass( "hidden" ).show();
                $( "#enlargeSpan" ).hide();
                showMoreLessBtn.hide();
            }
            else if( ! globals.isInFullScreen() && ! showMoreLessBtn.isVisible() )
            {
                // // Disismy.Symfony.DEBUG && console.log( "no estamos en fullscreen y showmoreless no estaba visible. Esto solo debe ocurrir una vez.")
                $( "#shrinkSpan" ).hide();
                $( "#enlargeSpan" ).show();
                showMoreLessBtn.show();
            }






            if( scrollTop === 0 )
            {
                // Disismy.Symfony.DEBUG && console.log( "Como estaba en cero, una vez hecho el cambio lo scrolleamos a 0" );
                $( document ).scrollTop( 0 );
                // Disismy.Symfony.DEBUG && console.log( "Tras resituar scroll, $( document ).scrollTop(): " + $( document ).scrollTop() );
            }
            else
            {
                // Disismy.Symfony.DEBUG && console.log( "No es cero... esto no debería ocurrir" );
            }


            Camera.setNewCamSpeedFromInnerW( innerW );

        },

        _getLonAndLatFromPoint: function( x, y, z )
        {
            var newLat, newLon;
            // si quisieramos hacer el inverso ( hayar phi teniendo target.y ):
            // como Math.cos( phi ) = target.y / sphere.RADIUS
            // phi = arcocoseno( target.y / sphere.RADIUS )
            var newPhi = Math.acos( y / sphere.RADIUS );
            // como tb sabemos que
            // phi = THREE.Math.degToRad( 90 - lat );
            newLat = 90 - THREE.Math.radToDeg( newPhi );

            // teniendo en cuenta que
            // x = sphere.RADIUS * Math.sin( phi ) * Math.cos( theta );
            // despejamos theta:
            // Math.cos( theta ) = x / ( SHPERE_RADIUS * Math.sin( phi ) )
            var newTheta = Math.acos( x / ( sphere.RADIUS * Math.sin( newPhi ) ) );

            // y como sabemos que
            // theta = THREE.Math.degToRad( lon );
            // TODO: Comprobar q esto está bien, aunque parece que funciona
            // TODO: la verdad es que no estoy muy seguro
            if( y >= 0 )
            {
                // x+, z+
                if( x >= 0 && z >= 0 )
                {
                    // x: 464.6272942119987
                    // y: 154.4214784673948
                    // z: 100.86411778453994
                    // console.warn( "CASO 1: y+, x+, z+. ESTA BIEN" );
                    newLon = THREE.Math.radToDeg( newTheta );
                }
                // x+, z-
                else if( x >= 0 && z < 0 )
                {
                    // x: 209.5274272409281
                    // y: 140.20185928145727
                    // z: -430.72912900548806
                    // console.warn( "CASO 2: y+, x+, z-. ESTA BIEN" );
                    newLon = - THREE.Math.radToDeg( newTheta );
                }
                // x-, z-
                else if( x < 0 && z < 0 )
                {
                    // console.warn( "CASO 3: y+, x-, z-. ESTA BIEN" );
                    // x: -412.37277622763463
                    // y: 34.63506818951073
                    // z: -279.28756267575073
                    newLon = - THREE.Math.radToDeg( newTheta );
                }
                // x-, z+
                else if( x < 0 && z >= 0 )
                {
                    // x: -81.5798507154943
                    // y: 105.38191797775427
                    // z: 480.9305304586501
                    // console.warn( "CASO 4: y+, x-, z+. ESTA BIEN" );
                    newLon = THREE.Math.radToDeg( newTheta );
                }
                else
                {
                    alert( "Aqui no puede llegar" );
                }

            }
            else
            {
                // x+, z+
                if( x >= 0 && z >= 0 )
                {
                    // console.warn( "CASO 5: y-, x+, z+. ESTA BIEN" );
                    newLon = THREE.Math.radToDeg( newTheta );
                }
                // x+, z-
                else if( x >= 0 && z < 0 )
                {
                    // console.warn( "CASO 6: y-, x+, z-. ESTA BIEN" );
                    newLon = - THREE.Math.radToDeg( newTheta );
                }
                // x-, z-
                else if( x < 0 && z < 0 )
                {
                    // console.warn( "CASO 7: y-, x-, z-. ESTA BIEN" );
                    newLon = - THREE.Math.radToDeg( newTheta );
                }
                // x-, z+
                else if( x < 0 && z >= 0 )
                {
                    // console.warn( "CASO 8: y-, x-, z+. ESTA BIEN" );
                    newLon = THREE.Math.radToDeg( newTheta );
                }
                else
                {
                    alert( "Aqui no puede llegar" );
                }
            }

            return { "lat": newLat, "lon": newLon };
        },


        /************************ PUBLIC METHODS: ************************/
        getPointFromLonAndLat: function( lon_, lat_ )
        {
            var self = this;

            var point = new THREE.Vector3();
            var newLat = Math.max( - 85, Math.min( 85, lat_ ) );
            var newPhi = THREE.Math.degToRad( 90 - newLat );
            var newTheta = THREE.Math.degToRad( lon_ );

            point.x = sphere.RADIUS * Math.sin( newPhi ) * Math.cos( newTheta );
            point.y = sphere.RADIUS * Math.cos( newPhi );
            point.z = sphere.RADIUS * Math.sin( newPhi ) * Math.sin( newTheta );

            return point;
        },

        getWindowInnerWidth: function()
        {
            var self = this;
            var newInnerWidth = self.$el.width();

            // si es la primera vez, se inicializa prevWindowInnerW
            if( typeof prevWindowInnerW === 'undefined' )
            {
                prevWindowInnerW = newInnerWidth;
            }

            return newInnerWidth; //window.innerWidth;
        },

        getWindowInnerHeight: function()
        {
            var self = this;
            var newInnerHeight = window.innerHeight - self.$el.offset().top;
            if( typeof prevWindowInnerH === 'undefined' )
            {
                prevWindowInnerH = newInnerHeight;
            }
            return newInnerHeight;
        },

        getScene: function()
        {
            return scene;
        },

        getObject: function()
        {
            return object;
        },

        setIsZoomable: function( _isZoomable )
        {
            isZoomable = _isZoomable;
        },

        toggleFullScreen: function()
        {
            var self = this;

            if( ! globals.isInFullScreen() )
            {

                $( "#enlargeSpan" ).hide();
                showMoreLessBtn.hide();
                $( "#shrinkSpan" ).removeClass( "hidden" ).show();

                // Supports most browsers and their versions.
                if( self.el.requestFullscreen ) {
                    self.el.requestFullscreen();
                  } else if(self.el.mozRequestFullScreen) {
                    self.el.mozRequestFullScreen();
                  } else if(self.el.webkitRequestFullscreen) {
                    self.el.webkitRequestFullscreen();
                  } else if(self.el.msRequestFullscreen) {
                    self.el.msRequestFullscreen();
                  }
            }
            else
            {

                $( "#shrinkSpan" ).hide();
                $( "#enlargeSpan" ).show();
                showMoreLessBtn.show();

                if( document.exitFullscreen )
                {
                    document.exitFullscreen();
                }
                else if( document.msExitFullscreen )
                {
                    document.msExitFullscreen();
                }
                else if( document.mozCancelFullScreen )
                {
                    document.mozCancelFullScreen();
                }
                else if( document.webkitExitFullscreen )
                {
                    document.webkitExitFullscreen();
                }
            }
        },

        loadPanoNode: function( panoNodeId, refereeButtonId )
        {
            var self = this;
            preloader.show( function()
            {

                unloadPanoNode();

                // Disismy.Symfony.DEBUG && console.log( "2. loadPanoNode", Disismy.Symfony.BASE_URL + "/panoNode/" + panoNodeId + "/" + refereeButtonId + "/" );
                $.post( Disismy.Symfony.BASE_URL + "/panoNode/" + panoNodeId + "/" + refereeButtonId + "/" )
                .done( function( data_ )
                {
                    if( data_.isOk )
                    {
                        curPanoNode = data_.data.panoNode;
                        curRoom = curPanoNode.room;

                        // Disismy.Symfony.DEBUG && console.log( "3. panoNodeLoaded. curPanoNode: ", curPanoNode, ", curRoom: ", curRoom );

                        loadPanoNodeTexture();
                    }
                    else
                    {
                        alert( "Error grabbing pano Node" );
                    }
                })
                .fail( function( xhr, textStatus, errorThrown )
                {
                    console.error( "Error grabbing pano Node. Error: " + xhr.responseText );
                });


                function unloadPanoNode()
                {
                    // Disismy.Symfony.DEBUG && console.log( "0. unloadPanoNode" );
                    self.isUserInteracting = false;

                    buttonsMgr.removeNodeButtons();
                    // console.error( "Txabox Sphere object: ", scene.getObjectByName( 'Sphere' ) );
                }

                function loadPanoNodeTexture()
                {
                    // Disismy.Symfony.DEBUG && console.log( "4. loadPanoNodeTexture" );

                    var textureImg = new Image();
                    textureImg.load(
                        Disismy.Symfony.BASE_URL + '/rawImage/' + curPanoNode.texture.id + '/',
                        undefined,
                        onImgProgress );

                    textureImg.onload = function()
                    {

                        // Disismy.Symfony.DEBUG && console.log( "5. panoNodeTextureLoaded" );

                        var texture = new THREE.Texture( texture_placeholder );
                        texture.image = this;
                        texture.needsUpdate = true;

                        sphere.setTexture( texture );

                        buttonsMgr.createNodeButtons( curPanoNode, function()
                        {
                            // Disismy.Symfony.DEBUG && console.log( "7. nodeButtonsCreated" );
                            hasInteractedAlready = false;
                            if( ! isAnimatedAlready )
                            {
                                self._animate();
                                isAnimatedAlready = true;
                            }
                            var isInitialNode = ( refereeButtonId === INITIAL_REFEREE_BTN );
                            if( isInitialNode )
                            {
                                infoPanel.show(
                                    preloader.hide()
                                );
                            }
                            else
                            {
                                var refereePairedBtn = buttonsMgr.buttons[ curPanoNode.refereePairedButtonId ];

                                var tempVect = self._getLonAndLatFromPoint(
                                    refereePairedBtn.transform.position.x,
                                    refereePairedBtn.transform.position.y,
                                    refereePairedBtn.transform.position.z );

                                Camera.setLon( tempVect.lon + 180 );
                                Camera.setLat( 0 );
                                var notificationsText = ( curRoom && curRoom.builtSqMeters && curRoom.builtSqMeters != 0 ) ? curPanoNode.name + '. ' + parseInt( curRoom.builtSqMeters ) + 'm<sup>2</sup>.' : curPanoNode.name + '. ';
                                preloader.hide(
                                    notifications.show( notificationsText )
                                );
                            }
                        });
                    }

                    function onImgProgress( xhr )
                    {
                        preloader.update( xhr.loaded / xhr.total );
                    }



                }
            });



        },

        onInitialInfoPanelHidden: function()
        {
            var self = this;

            var notificationsText = ( curRoom && curRoom.builtSqMeters && curRoom.builtSqMeters != 0 ) ? curPanoNode.name + '. ' + parseInt( curRoom.builtSqMeters ) + 'm<sup>2</sup>.' : curPanoNode.name + '. ';
            notifications.show( notificationsText )
        },



        onGyroscopeToTouch: function()
        {
            var self = this;

            // hayamos el targetPos aunque no lo usemos, xq luego cuando pasas
            // a pointerControls necesitamos este dato
            var centerScreenPos = { "x": 0, "y": 0 };
            centerScreenPos.x = ( ( ( self.$el.width() * 0.5 ) - self.$el.offset().left + $( document ).scrollLeft() ) / self.$el.width() ) * 2 - 1;
            centerScreenPos.y = - ( ( ( self.$el.height() * 0.5 ) - self.$el.offset().top + $( document ).scrollTop() ) / self.$el.height() ) * 2 + 1;

            raycaster.setFromCamera( centerScreenPos, Camera.camera );

            // calculate objects intersecting the picking ray
            var intersects = raycaster.intersectObjects(
                scene.children,
                true // recursively
                );

            for ( var i = 0; i < intersects.length; i++ )
            {
                var intersection = intersects[ i ];
                var intersectionPoint = intersection.point;
                var collidingObj = intersects[ i ].object;

                if( collidingObj.tag === "Sphere" )
                {
                    // alert( "Tocas la sphera! [ " + intersectionPoint.x + ", " + intersectionPoint.y + ", " + intersectionPoint.z + " ]" );

                    Camera.target.x = intersectionPoint.x;
                    Camera.target.y = intersectionPoint.y;
                    Camera.target.z = intersectionPoint.z;


                    var tempVect = self._getLonAndLatFromPoint(
                        intersectionPoint.x,
                        intersectionPoint.y,
                        intersectionPoint.z );

                    Camera.setLon( tempVect.lon );
                    Camera.setLat( tempVect.lat );

                }

            }
        },


        /*
        este no se usa, es solo para debuguear
        createNButtons: function( num )
        {
            var self = this;
            for( var i = 0; i < num; ++i )
            {
                var btnJSON = {
                    "id": "rand_" + i,
                    "name": "rand_" + i,
                    "target": -1,
                    "p": {
                        "lon": globals.randomIntFromInterval( 0, 360 ),
                        "lat": globals.randomIntFromInterval( -85, 85 )
                    }};
                buttonsMgr.createNewBtn( btnJSON );
            }
        },
        */

    });

    return PanoView;

});
