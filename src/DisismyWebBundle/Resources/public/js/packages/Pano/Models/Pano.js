"use strict";

define( [
    'jquery',
    'underscore',
    'backbone'
], function ( $, _, Backbone )
{
    /*
    ejemplo de variables y funciones privadas
    var privateVar = "privateVar";
    function privateFunction(arg)
    {
        console.log("Esto es una funcion privada cuyo argumento es: ", arg);
    }
    */

    function setIdFromEncodedId( encodedId, model )
    {
        model.set({ 'id': encodedId });
    }

    function setEncodedIdFromId( id, model )
    {
        model.set({ 'encodedId': id });
    }

    var PanoModel = Backbone.Model.extend({
            defaults:{
                "id"        : -1,
                "encodedId" : -1,
                "name"      : "",
                "alt"       : "",
                "desc"      : ""
            },

            url: function()
            {
                var self = this;
                return "pano/" + self.get( 'encodedId' );
            },

            parse: function( response, options )
            {
                return response.data.pano;
            },

            initialize: function( options )
            {
                var self = this;

                if( options.id )
                {
                    setEncodedIdFromId( options.id, self );
                }
                else if( options.encodedId )
                {
                    setIdFromEncodedId( options.encodedId, self );
                }

                this.on( 'change:encodedId', function(){
                    setIdFromEncodedId( self.get( 'encodedId' ), self );
                 });
            },

            setId: function()
            {
                this.set({ id: this.get( "encodedId" ) });
            },

            // para sacar el mensaje de error
            // myPano.on('invalid', function (model, error) {
            // console.log(error);
            //});
            validate: function( attributes )
            {
                if ( attributes.encodedId < 0 )
                {
                    return 'Invalid encodedId.';
                }
            },

            render: function()
            {
            }

        });

    return PanoModel;

});
