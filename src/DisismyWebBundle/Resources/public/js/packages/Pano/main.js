"use strict";

define( [
    './Views/Pano',
], function ( PanoView )
{
    var Pano = function( options )
    {
        return new PanoView( options );
    }

    return Pano;

});
