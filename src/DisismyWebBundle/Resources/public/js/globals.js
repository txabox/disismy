"use strict";
define( [ 'jquery', 'tweenable' ], function( $, Tweenable )
{

    var Globals = function()
    {

        var that = this;
        this.touchOrClick = ( 'ontouchstart' in window ) ? 'touchstart' : 'click';

        this.HALF_PI = Math.PI / 2;

        this.tweener = new Tweenable();

        this.isFullscreenable = function()
        {
            return (
                document.fullscreenEnabled ||
                document.webkitFullscreenEnabled ||
                document.mozFullScreenEnabled ||
                document.msFullscreenEnabled
            );
        }


        this.isInFullScreen = function()
        {
            var isInFullScreen = false;

            if( ! document.fullscreenElement &&    // alternative standard method
                ! document.mozFullScreenElement &&
                ! document.webkitFullscreenElement &&
                ! document.msFullscreenElement )
            {
                isInFullScreen = false;
            }
            else
            {
                isInFullScreen = true;
            }

            return isInFullScreen; // ( window.innerWidth == screen.width && window.innerHeight == screen.height );
        }

        this.preventEventPropagation = function( element, skippableEventsArray )
        {
            var events = [ 'mousedown', 'mousemove', 'mouseup', 'mousewheel',
            'MozMousePixelScroll', 'contextmenu', 'touchstart', 'touchmove',
            'touchend' ]

            for( var i = 0; i < events.length; ++i )
            {
                var event = events[ i ];
                // si no está en los eventos a mantener
                if( $.inArray( event, skippableEventsArray ) === -1 )
                {
                    // evitamos que se propague
                    if( ! element[ 0 ] )
                    {
                        debugger;
                    }
                    element[ 0 ].addEventListener( event, that.preventPropagation, false );
                }
            }

        }

        this.preventPropagation = function( event )
        {
            event.preventDefault();
            event.stopPropagation();
        }

        this.randomIntFromInterval = function( min, max )
    	{
    	    return Math.floor( Math.random() * ( max - min + 1 ) + min );
    	}

        // http://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
        this.copyToClipboard = function( textAreaEl, buttonEl, cb )
        {
            var successful = false;
            textAreaEl.select();

            try
            {
                successful = document.execCommand( 'copy' );
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log( 'Copying text command was ' + msg );
            }
            catch( err )
            {
                alert( 'Oops, unable to copy' );
            }

            cb && cb( buttonEl, successful );

        }

        return this;

    }

    return new Globals();

});
