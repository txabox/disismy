<?php

namespace DisismyWebBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bridge\Monolog\Logger;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use DisismyWebBundle\Services\GlobalsService;
use Doctrine\ORM\EntityManager;

use DisismyWebBundle\Entity\Folder;
use DisismyWebBundle\Entity\StoragePlan;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class UserRegisteredListener implements EventSubscriberInterface
{
    private $router;
    private $logger;
    private $globals;
    private $container;
    private $em;
    private $fs;

    public function __construct( UrlGeneratorInterface $router, ContainerInterface $container, EntityManager $entityManager, Logger $logger, GlobalsService $globals )
    {
        $this->router = $router;
        $this->container = $container;
        $this->em = $entityManager;
        $this->logger = $logger;
        $this->globals = $globals;
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_INITIALIZE  => 'onRegistrationInitialize',
            FOSUserEvents::REGISTRATION_SUCCESS     => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_FAILURE     => 'onRegistrationFailure',
            FOSUserEvents::REGISTRATION_COMPLETED   => 'onRegistrationCompleted',
        );
    }

    public function onRegistrationInitialize(UserEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getUser();
        // $this->logger->info( 'Txabox1 onRegistrationInitialize: ' , array( '$event' => $event ) );
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        // $user = $event->getUser();
        // $this->logger->info( 'Txabox1 onRegistrationSuccess: ' , array( '$event' => $event ) );
    }

    public function onRegistrationFailure(FormEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        // $user = $event->getUser();
        // $this->logger->info( 'Txabox1 onRegistrationFailure: ' , array( '$event' => $event ) );
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getUser();
        $encodedUserId = $this->globals->optimus->encode( $user->getId() );

        $userFolderPath =  $this->container->get( 'kernel' )->getRootDir() . "/../src/DisismyWebBundle/Resources/public/images/Users/$encodedUserId/";

        $rootFolder = new Folder();
        $rootFolder->setName( "root" );
        $rootFolder->setOwner( $user );

        $this->em->persist( $rootFolder );

        $storagePlan = $this->em
        ->getRepository( 'DisismyWebBundle:StoragePlan' )
        ->findOneBy( array( 'name_en' => 'BASIC' ) );

        $user->setRootFolder( $rootFolder );
        $user->setStoragePlan( $storagePlan );
        $this->em->persist( $user );
        $this->em->flush();

        $user->setEncodedId( $this->globals->optimus->encode( $user->getId() ) );
        $this->em->persist( $user );
        $this->em->flush();

        $rootFolderPath = "$userFolderPath". $this->globals->optimus->encode( $rootFolder->getId() ) . "/";



        $this->fs->mkdir( $userFolderPath, 0755 );
        $this->fs->mkdir( $rootFolderPath, 0755 );
        // $this->logger->info( 'Txabox1 onRegistrationCompleted: Usuario registrado satisfactoriamente. Creamos una nueva carpeta raíz para este usuario en : ' . $rootFolderPath );
    }
}
