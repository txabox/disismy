<?php

namespace DisismyWebBundle\Controller;

require_once __DIR__.'/../../../vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Jenssegers\Optimus\Optimus;

class PanosController extends Controller
{

    /**
     * @Route( "/panos/", name="getPanos" )
     * @Route( "/{_locale}/panos/", name="localized_getPanos" )
     * @Method( { "GET" } )
     */
    public function getPanosAction( Request $request )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'GET' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        return $response->setData( array(
                "code" => 1,
                "isOk" => true,
                'data' => array(
                    "msg" => "devolviendo todos los panos",
                )
            ));


    }

    /**
     * @Route( "/pano/{encodedPanoId}/", name="getPano" )
     * @Route( "/{_locale}/pano/{encodedPanoId}/", name="localized_getPano" )
     * @Method( { "GET" } )
     */
    public function getPanoAction( Request $request, $encodedPanoId )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'GET' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        $pano = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Pano' )
        ->findOneById( $globals->optimus->decode( $encodedPanoId )  );


        if( ! $pano )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "encodedPanoId" => $encodedPanoId,
                        "msg" => 'No pano found for panoId ' . $encodedPanoId
                    )
                ));
        }

        // TODO: Chequear si es publico y de no serlo chequear permisos del que lo mira.

        return $response->setData( array(
                "code" => 1,
                "isOk" => true,
                'data' => array(
                    "pano" => $pano->toArrayWithObject( $globals, $locale ),
                )
            ));


    }


    /**
     * @Route( "/panoNode/{encodedPanoNodeId}/{encodedRefereeButtonId}/", name="getPanoNode" )
     * @Route( "/{_locale}/panoNode/{encodedPanoNodeId}/{encodedRefereeButtonId}/", name="localized_getPanoNode" )
     * @Method({"POST","GET"})
     */
    public function getPanoNodeAction( Request $request, $encodedPanoNodeId, $encodedRefereeButtonId )
    {
        // encodedRefereeButtonId es el id del botón que has presionado para conseguir este panoNode

        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();

        $panoNodeId = $globals->optimus->decode( $encodedPanoNodeId );
        $refereeButtonId = $globals->optimus->decode( $encodedRefereeButtonId );



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );

        if( ! $globals->IS_DEBUG_MODE )
        {
            if( ! $this->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => "access_denied"
                ));
                return $response;
                //throw $this->createAccessDeniedException();
            }
        }



        $panoNode = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:PanoNode' )
        ->findOneById( $panoNodeId );





        if( ! $panoNode )
        {
            throw $this->createNotFoundException(
                'No panoNode found in that index [ ' . $encodedPanoNodeId . ' ] for that pano ' . $pano->getId()
            );
        }
        else
        {
            // existe panoNode:
            $room = $panoNode->getRoom();
            $roomArray;
            if( $room )
            {
                $roomArray = array(
                    "name" => $room->getName( $locale ),
                    "desc" => $room->getDescription( $locale ),
                    "utilSqMeters" => $room->getUtilSqMeters(),
                    "builtSqMeters" => $room->getBuiltSqMeters()
                );
            }




            $panoNodeArray = array(
                        "id" => intval( $encodedPanoNodeId ),
                        "name" => $panoNode->getName( $locale ),
                        "desc" => $panoNode->getDescription( $locale ),
                        "texture" => array(
                            "id" => $globals->optimus->encode( $panoNode->getTexture()->getId() ),
                            "ext" =>  $panoNode->getTexture()->getExtension()
                        ),
                        "room" => $roomArray
                    );
            $buttons = $panoNode->getButtons();
            $buttonsArray = [];

            if( count( $buttons ) <= 0 )
            {
                // este panoNode no tiene ningun boton
                /*
                throw $this->createNotFoundException(
                    'No buttons found for panoNode: ' . $panoNode->getId()
                );
                */
            }
            else
            {
                foreach ( $buttons as $button )
                {
                    array_push( $buttonsArray, array(
                        "id" => $globals->optimus->encode( $button->getId() ),
                        "name" => $button->getTargetPanoNode()->getRoom()->getName( $locale ),
                        "target" => $globals->optimus->encode( $button->getTargetPanoNode()->getId() ),
                        "p" => array(
                            "lon" => $button->getLon(),
                            "lat" => $button->getLat() ) ) );
                    // dump( $button->getZScale() );
                }
            }

            $panoNodeArray[ "buttons" ] = $buttonsArray;

        }




        if( $encodedRefereeButtonId != 0 )
        {
            $refereeButton = $this->getDoctrine()
            ->getRepository( 'DisismyWebBundle:Button' )
            ->findOneById( $refereeButtonId );





            if( ! $refereeButton )
            {
                throw $this->createNotFoundException(
                    'No refereeButton found in that index [ ' . $refereeButtonId . ' ] '
                );
            }
            else
            {
                if( $refereeButton->getPairedButton() )
                {
                    $panoNodeArray[ "refereePairedButtonId" ] = $globals->optimus->encode( $refereeButton->getPairedButton()->getId() );
                }
                else
                {
                    $panoNodeArray[ "refereePairedButtonId" ] = 0;
                }
            }
        }
        else
        {
            $panoNodeArray[ "refereePairedButtonId" ] = 0;
        }

        // $jsonContent = $serializer->serialize( $buttons, 'json');
        // echo $jsonContent;

        $response->setData( array(
                "code" => 1,
                "isOk" => true,
                'data' => array(
                    "panoNode" => $panoNodeArray
                )
            ));

        return $response;

    }







    /**
     * @Route( "/pano/edit/{encodedPanoId}/", name="editPano" )
     * @Route( "/{_locale}/pano/edit/{encodedPanoId}/", name="localized_editPano" )
     * @Method( "GET" )
     */
    public function editPanoAction( Request $request, $encodedPanoId )
    {
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();

        return $this->render( 'DisismyWebBundle:Panos:edit.html.twig', array(
            // ...
        ));
    }

}
