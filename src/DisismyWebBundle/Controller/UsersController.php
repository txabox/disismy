<?php

namespace DisismyWebBundle\Controller;

require_once __DIR__.'/../../../vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Jenssegers\Optimus\Optimus;

use Doctrine\Common\Collections\ArrayCollection;

class UsersController extends Controller
{

    /**
     * @Route( "/user/{encodedUserId}/", name="getUser" )
     * @Route( "/{_locale}/user/{encodedUserId}/", name="localized_getUser" )
     * @Method( "GET" )
     */
    public function getUserAction( Request $request, $encodedUserId )
    {
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $curUser = $this->getUser();

        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'GET' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );

        // se puede coger un usuario por su encodedId
        if( is_numeric( $encodedUserId ) )
        {
            $userId = $globals->optimus->decode( $encodedUserId );

            $user = $this->getDoctrine()
            ->getRepository( 'DisismyWebBundle:User' )
            ->findOneById( $userId );

        }
        // se puede coger un usuario por su nombre.
        else
        {

            $userName = $encodedUserId;

            $user = $this->getDoctrine()
            ->getRepository( 'DisismyWebBundle:User' )
            ->findOneByUsername( $userName );

        }

        if( ! $user )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "no_user_found_with_that_id"
                    )
                ));
        }


        // si es curUser, lo mostramos aunque sea privado
        $isCurUser = $curUser && $curUser->getId() === $user->getId();


        if( ! $isCurUser &&
            ! $user->getIsPublic() )
        {
            return $response->setData( array(
                    "code" => 1,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "this_user_is_no_public"
                    )
                ));
        }


        return $response->setData( array(
                "code" => 2,
                "isOk" => true,
                'data' => array(
                    "user" => $user->toArray( $globals, $locale ),
                    "isCurUser" => $isCurUser
                )
            ));

    }

    /**
     * @Route( "/get/public/users/names/", name="getPublicUsersNames" )
     * @Route( "/{_locale}/get/public/users/names/", name="localized_getPublicUsersNames" )
     * @Method( { "GET" } )
     */
    public function getPublicUsersNamesAction( Request $request )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $curUser = $this->getUser();



        $names = array();
        $term = trim( strip_tags( $request->get( 'term' ) ) );

        $users = $this->getDoctrine()
                    ->getRepository( 'DisismyWebBundle:User' )
                    ->findAllPublicUsersWithNamaLike( $term );


        foreach( $users as $user )
        {
            // el propio usuario no sale
            if( $curUser->getId() != $user->getId() )
            {
                $names[] = $user->getUsername();
            }
        }

        $response = new JsonResponse();
        $response->setData( $names );

        return $response;
    }


}
