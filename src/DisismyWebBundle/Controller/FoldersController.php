<?php

namespace DisismyWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FoldersController extends Controller
{
    /**
     * @Route( "/rootFolder/", name="getRootFolder" )
     * @Route( "/{_locale}/rootFolder/", name="localized_getRootFolder" )
     * @Method( { "POST" } )
     */
    public function getRootFolderAction( Request $request )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'POST' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        $tempResponse = $globals->isUserLogged();
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser = $tempResponse[ "data" ][ "curUser" ];

        return $this->forward('DisismyWebBundle:Folders:getFolder', array(
            'encodedFolderId'  => $globals->optimus->encode( $curUser->getRootFolder()->getId() ),
        ));
    }


    /**
     * @Route( "/folder/{encodedFolderId}/", name="getFolder" )
     * @Route( "/{_locale}/folder/{encodedFolderId}/", name="localized_getFolder" )
     * @Method( { "POST" } )
     */
    public function getFolderAction( Request $request, $encodedFolderId )
    {
        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'POST' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - el folder exista
        //  - el usuario sea el owner del folder
        $tempResponse = $globals->isUserLoggedAndOwnsThisFolder( $encodedFolderId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "encodedFolderId" => $encodedFolderId,
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser = $tempResponse[ "data" ][ "curUser" ];
        $folder   = $tempResponse[ "data" ][ "folder" ];


        return $response->setData( array(
                "code" => 1,
                "isOk" => true,
                'data' => array(
                    "folder" => $folder->toArray( $globals, $locale ),
                    "msg" => "success_retreiving_folder"
                )
            ));
    }

}
