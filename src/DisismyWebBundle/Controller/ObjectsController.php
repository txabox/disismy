<?php

namespace DisismyWebBundle\Controller;

require_once __DIR__.'/../../../vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Jenssegers\Optimus\Optimus;

use Doctrine\Common\Collections\ArrayCollection;

use DisismyWebBundle\Form\ObjectType;
use DisismyWebBundle\Form\HouseType;

class ObjectsController extends Controller
{

    /**
     * @Route( "/object/{encodedObjectId}/", name="getObject" )
     * @Route( "/{_locale}/object/{encodedObjectId}/", name="localized_getObject" )
     * @Method( "GET" )
     */
    public function getObjectAction( Request $request, $encodedObjectId )
    {
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $curUser = $this->getUser();

        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'GET' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        $objectId = $globals->optimus->decode( $encodedObjectId );




        $object = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Object' )
        ->findOneById( $objectId );



        if( ! $object )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "no_object_found"
                    )
                ));
        }


        // typed object es la info más concreta del tipo de objeto house, hotel,
        // restaurant, promotion, etc
        $typedObject = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Object' )
        ->getTypedObjectData( $object->getId(), $object->getType()->getName() );


        if( ! $typedObject )
        {
            return $response->setData( array(
                    "code" => 1,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "no_typedObject_found_for_this_object"
                    )
                ));
        }


        $panos = $object->getPanos();
        $panosArray = [];

        foreach( $panos as $pano )
        {
            array_push( $panosArray, $pano->toArrayWithObject( $globals, $locale ) );
        }

        $objectArray = $object->toArray( $globals, $locale, $curUser );
        $objectArray[ "typedObject" ] = $typedObject->toArray( $globals, $locale );
        $objectArray[ "panos" ] = $panosArray;

        return $response->setData( array(
                "code" => 2,
                "isOk" => true,
                'data' => array(
                    "object"        => $objectArray
                )
            ));

    }

    /**
     * @Route( "/object/{encodedObjectId}/", name="patchObject" )
     * @Route( "/{_locale}/object/{encodedObjectId}/", name="localized_patchObject" )
     * @Method( "PATCH" )
     */
    public function patchObjectAction( Request $request, $encodedObjectId )
    {
        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $curUser = $this->getUser();
        $logger = $this->get( 'logger' );

        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'PATCH' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );


        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - el objeto exista
        //  - el usuario sea el owner del objeto
        $tempResponse = $globals->isUserLoggedAndOwnsThisObject( $encodedObjectId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }


        $params = json_decode( $request->getContent(), true );


        $curUser    = $tempResponse[ "data" ][ "curUser" ];
        $object     = $tempResponse[ "data" ][ "object" ];

        foreach( $params as $key => $value )
        {

            $method = "set" . ucfirst( $key );

            // mediante este metodo cambiamos los setters que dependen de un
            // booleano, un integer o un string
            if( method_exists( $object, $method ) )
            {
                $object->$method( $value );
            }
            // aqui modificamos los métodos que necesitan una entidad externa o
            // son un poco más complejos
            else
            {
                // comprobamos a ver si es porque es un método que depende del locale
                // tipo setName_en | setName_es:
                if( substr( $method, -3, 1 ) === "_" )
                {
                    $methodNameWithoutLocale = substr( $method, 0, strlen ( $method ) - 3 );
                    $locale = substr( $method, -2 );
                    // echo "true. locale: $locale, methodNameWithoutLocale: $methodNameWithoutLocale";
                    $object->$methodNameWithoutLocale( $value, $locale );
                }
                // en realidad el método se llama setThumbnail y necesita
                // una entidad de tipo Image
                else if( $method == "setThumbId" )
                {
                    $newThumb = $this->getDoctrine()
                    ->getRepository( 'DisismyWebBundle:Image' )
                    ->findOneById( $globals->optimus->decode( $value ) );

                    $object->setThumbnail( $newThumb );
                }
                else
                {
                    return $response->setData( array(
                            "code" => 1,
                            "isOk" => false,
                            'data' => array(
                                "msg" => "objects don't have $method method."
                            )
                        ));
                }
            }


        }



        $em->persist( $object );
        $em->flush();

        return $this->getObjectAction( $request, $encodedObjectId );

    }



    /**
     * @Route( "/objects/", name="objects" )
     * @Route( "/{_locale}/objects/", name="localized_objects" )
     * @Method( "GET" )
     */
    public function objectsAction( Request $request )
    {
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $curUser = $this->getUser();

        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'GET' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );


        $ownerEncodedId = $request->query->get( 'ownerEncodedId' );
        if( ! $ownerEncodedId )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "no_object_owner_provided"
                    )
                ));
        }

        $ownerId = $globals->optimus->decode( $ownerEncodedId );

        $isOwnerCurUser = $curUser && ( $curUser->getId() === $ownerId );


        $objects = array();

        $owner = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:User' )
        ->findOneById( $ownerId );


        if( ! $owner )
        {
            return $response->setData( array(
                    "code" => 1,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "invalid_object_owner"
                    )
                ));
        }


        $ownerObjects = $owner->getObjects();

        if( ! $ownerObjects )
        {
            return $response->setData( array(
                    "code" => 2,
                    "isOk" => true,
                    'data' => array(
                        "msg" => "this_owner_doesnt_have_any_objects"
                    )
                ));
        }

        foreach( $ownerObjects as $object )
        {
            // si el dueño de la galería es curUser puede verlos todos
            if( $isOwnerCurUser ||
            // o si el dueño no es curUser pero es un objeto público, también puede verlo
                $object->getIsPublic() ||
                // o si es un objeto privado pero curUser es un viewer asociado a
                // ese objeto
                $curUser && $object->getViewers()->contains( $curUser )
                )
            {
                array_push( $objects, $object->toArray( $globals, $locale ) );
            }
        }

        return $response->setData( array(
                "code" => 3,
                "isOk" => true,
                'data' => array(
                    'owner' => array(
                        'encodedId' => $owner->getEncodedId(),
                        'username' => $owner->getUsername()
                    ),
                    'objects' => $objects,
                    'isOwnerCurUser' => $isOwnerCurUser
                )
            ));

    }



    /**
     * @Route( "/edit/{objectType}/{encodedObjectId}/", name="editObject", requirements={ "objectType": "%objectTypes%" } )
     * @Route( "/{_locale}/edit/{objectType}/{encodedObjectId}/", name="localized_editObject", requirements={ "objectType": "%objectTypes%" } )
     * @Method( {"POST","GET"} )
     */
     /*
    public function editObjectAction( Request $request, $objectType, $encodedObjectId )
    {

        if( ! $this->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
        {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $curUser = $this->getUser();



        $disismyLanguages = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Language' )
        ->findAll();

        $typedObject = null;


        $objectId = $globals->optimus->decode( $encodedObjectId );

        $object = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Object' )
        ->findOneById( $objectId );

        if( ! $object )
        {
            throw $this->createNotFoundException(
                'No object found for object ' . $encodedObjectId
            );
        }

        $objectThumbnail = $object->getThumbnail();


        if( $object->getType()->getName() != $objectType )
        {
            throw $this->createNotFoundException(
                "El tipo del objeto no concuerda con el objectType dado."
            );
        }


        $form = $this->createForm(
            ObjectType::class,
            $object,
            array(
                'action' => $this->generateUrl( 'editObject', array( 'objectType' => "house", 'encodedObjectId' => $encodedObjectId ) ),
                'method' => 'POST',
                'curUser' => $curUser,
        ));
        switch( $object->getType()->getName() )
        {
            case 'house':
                // $originalRooms = new ArrayCollection();
                $house = $object->getHouse();

                $typedObject = $house;
                $form->add( 'house', HouseType::class );
                break;

            default:
                # code...
                break;
        }

        $form->handleRequest( $request );

        if( $form->isSubmitted() )
        {
            if( $form->isValid() )
            {

                // TODO: ESTO ES TEMPORAL, SOLO HASTA QUE MANEJES BIEN LOS THUMBNAILS EN EL FRONTEND
                $object->setThumbnail( $objectThumbnail );

                $em->persist( $object );
                $em->persist( $house );

                $em->flush();

                // redirect back to some edit page
                // return $this->redirectToRoute( 'house_edit', array( 'id' => $id ) );

            }
            else
            {
                // die( "form not valid" );
            }
        }
        else
        {
            // es la 1ª vez que entras aqui, el formulario aun no se ha enviado
        }

        $objectLanguages = $object->getLanguages();
        $objectPanos = $object->getPanos();
        $objectViewers = $object->getViewers();

        return $this->render( 'DisismyWebBundle:Objects:edit.html.twig', array(
            "objectType"        => $objectType,
            "typedObject"       => $typedObject, // los datos de la casa, el hotel o lo que sea
            "form"              => $form->createView(),
            "encodedObjectId"   => $encodedObjectId,
            "disismyLanguages"  => $disismyLanguages,
            "objectLanguages"   => $objectLanguages,
            "objectViewers"     => $objectViewers,
            "objectPanos"       => $objectPanos,
        ));
    }
    */










    /**************************************************************************/
    /*****************************      API      ******************************/
    /**************************************************************************/



    /**
     * @Route( "/patch/object/{encodedObjectId}/{property}/{isActive}/", name="patchBoolean", requirements={ "property": "%objectBoolProperties%" } )
     * @Route( "/{_locale}/patch/object/{encodedObjectId}/{property}/{isActive}/", name="localized_patchBoolean", requirements={ "property": "%objectBoolProperties%" } )
     * @Method( { "PATCH" } )
     */
    public function patchBooleanAction( Request $request, $encodedObjectId, $property, $isActive )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'PATCH' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - el objeto exista
        //  - el usuario sea el owner del objeto
        $tempResponse = $globals->isUserLoggedAndOwnsThisObject( $encodedObjectId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser    = $tempResponse[ "data" ][ "curUser" ];
        $object     = $tempResponse[ "data" ][ "object" ];



        $isActive_  = ( $isActive === 'true' );



        // 2. Cambiamos el valor de property
        switch( $property )
        {
            case 'isPublic':
                $object->setIsPublic( $isActive_ );
                break;
            case 'isOnSale':
                $object->setIsOnSale( $isActive_ );
                break;
            case 'isOnRent':
                $object->setIsOnRent( $isActive_ );
                break;
            default:
                return $response->setData( array(
                        "code" => 2,
                        "isOk" => false,
                        'data' => array(
                            "msg" => "not_suported_property"
                        )
                ));
                break;
        }
        $em->persist( $object );
        $em->flush();

        return $response->setData( array(
                "code" => 3,
                "isOk" => true,
                'data' => array(
                    "msg" => $property . "_patched"
                )
        ));

    }



    private function addViewer( $object, $encodedIdOrUsername, $locale )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );

        // 1º seleccionamos el viewer bien por el encodedEntityId o
        // por el encodedEntityName
        if( is_numeric ( $encodedIdOrUsername ) )
        {

            $viewerId = $globals->optimus->decode( $encodedIdOrUsername );

            if( $viewerId === $globals->getUser()->getId() )
            {
                return array(
                        "code" => 1,
                        "isOk" => false,
                        'data' => array(
                            "msg" => "cur_user_cant_be_viewer"
                        )
                    );
            }

            $viewer = $this->getDoctrine()
            ->getRepository( 'DisismyWebBundle:User' )
            ->findOneById( $viewerId );

        }
        else
        {
            if( $encodedIdOrUsername === $this->getUser()->getUsername() )
            {
                return array(
                        "code" => 2,
                        "isOk" => false,
                        'data' => array(
                            "msg" => "cur_user_cant_be_viewer"
                        )
                    );
            }

            $viewer = $this->getDoctrine()
            ->getRepository( 'DisismyWebBundle:User' )
            ->findOneByUsername( $encodedIdOrUsername );

        }

        if( ! $viewer )
        {
            return array(
                    "code" => 3,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "no_viewer_found_for_that_entityValue"
                    )
                );
        }



        // si existia previamente no hay que hacer nada
        if( $object->getViewers()->contains( $viewer ) )
        {
            return array(
                    "code" => 4,
                    "isOk" => true,
                    'data' => array(
                        "msg" => "viewer_already_existed"
                    )
                );
        }
        // finalmente añadimos ese viewer al objeto si es que no existía
        // previamente
        else
        {
            $object->addViewer( $viewer );
            $em->persist( $object );
            $em->flush();

            return array(
                    "code" => 5,
                    "isOk" => true,
                    'data' => array(
                        "msg" => "viewer_added",
                        "viewer" => $viewer->toArray( $globals, $locale )
                    )
                );
        }

    }



    private function removeViewer( $object, $encodedViewerId )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );



        $viewerId = $globals->optimus->decode( $encodedViewerId );

        $viewer = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:User' )
        ->findOneById( $viewerId );

        if( ! $viewer )
        {
            return array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => 'viewer_not_found'
                    )
                );
        }


        // Si contenia el viewer, lo borramos
        if( $object->getViewers()->contains( $viewer ) )
        {
            $object->removeViewer( $viewer );
            $em->persist( $object );
            $em->flush();
        }

        return array(
                "code" => 3,
                "isOk" => true,
                'data' => array(
                    "msg" => 'viewer_removed'
                )
            );
    }



    private function addLanguage( $object, $encodedLanguageId, $locale )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );



        // 1. Seleccionamos el language
        $languageId = $globals->optimus->decode( $encodedLanguageId );

        $language = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Language' )
        ->findOneById( $languageId );

        if( ! $language )
        {
            return array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => 'language_not_found'
                    )
                );
        }



        // si existia previamente no hay que hacer nada
        if( $object->getLanguages()->contains( $language ) )
        {
            return array(
                    "code" => 1,
                    "isOk" => true,
                    'data' => array(
                        "msg" => "language_already_existed"
                    )
                );
        }
        // finalmente añadimos ese language al objeto si es que no existía
        // previamente
        else
        {
            $object->addLanguage( $language );
            $em->persist( $object );
            $em->flush();

            return array(
                    "code" => 2,
                    "isOk" => true,
                    'data' => array(
                        "msg" => 'language_added'
                    )
                );
        }

    }



    private function removeLanguage( $object, $encodedLanguageId )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );



        // Seleccionamos el language
        $languageId = $globals->optimus->decode( $encodedLanguageId );

        $language = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Language' )
        ->findOneById( $languageId );

        if( ! $language )
        {
            return array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => 'language_not_found'
                    )
                );
        }


        $languages = $object->getLanguages();
        // Siempre tiene que tener al menos un language...
        if( count( $languages ) === 1 )
        {
            return array(
                    "code" => 2,
                    "isOk" => false,
                    'data' => array(
                        "msg" => 'last_language_cannot_be_removed'
                    )
                );
        }



        // Si contenia el language, lo borramos
        if( $languages->contains( $language ) )
        {
            $object->removeLanguage( $language );
            $em->persist( $object );
            $em->flush();
        }

        return array(
                "code" => 3,
                "isOk" => true,
                'data' => array(
                    "msg" => 'language_removed'
                )
            );
    }



    /**
     * @Route( "/add/object/{encodedObjectId}/{entityType}/{entityValue}/", name="addNewEntity", requirements={ "entityType": "%objectEntityTypes%" } )
     * @Route( "/{_locale}/add/object/{encodedObjectId}/{entityType}/{entityValue}/", name="localized_addNewEntity", requirements={ "entityType": "%objectEntityTypes%" } )
     * @Method( { "PATCH" } )
     */
    public function addNewEntityAction( Request $request, $encodedObjectId, $entityType, $entityValue )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'PATCH' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - el objeto exista
        //  - el usuario sea el owner del objeto
        $tempResponse = $globals->isUserLoggedAndOwnsThisObject( $encodedObjectId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser    = $tempResponse[ "data" ][ "curUser" ];
        $object     = $tempResponse[ "data" ][ "object" ];


        $method = "add" . ucfirst( $entityType );



        if( method_exists( $this, $method ) )
        {
            // die( "El metodo $method existe" );
            $tempResponse = $this->$method( $object, $entityValue, $locale );
        }
        else
        {
            $tempResponse[ "isOk" ] = false;
            $tempResponse[ "data" ][ "msg" ] = "invalid entity type";
        }


        return $response->setData( array(
                "code" => $tempResponse[ "code" ],
                "isOk" => $tempResponse[ "isOk" ],
                'data' => $tempResponse[ "data" ]
            ));

    }



    /**
     * @Route( "/remove/object/{encodedObjectId}/{entityType}/{entityValue}/", name="removeEntity", requirements={ "entityType": "%objectEntityTypes%" } )
     * @Route( "/{_locale}/remove/object/{encodedObjectId}/{entityType}/{entityValue}/", name="localized_removeEntity", requirements={ "entityType": "%objectEntityTypes%" } )
     * @Method( { "PATCH" } )
     */
    public function removeEntityAction( Request $request, $encodedObjectId, $entityType, $entityValue )
    {

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'PATCH' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - el objeto exista
        //  - el usuario sea el owner del objeto
        $tempResponse = $globals->isUserLoggedAndOwnsThisObject( $encodedObjectId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser    = $tempResponse[ "data" ][ "curUser" ];
        $object     = $tempResponse[ "data" ][ "object" ];



        $method = "remove" . ucfirst( $entityType );

        if( method_exists( $this, $method ) )
        {
            // die( "El metodo $method existe" );
            $tempResponse = $this->$method( $object, $entityValue );
        }
        else
        {
            $tempResponse[ "isOk" ] = false;
            $tempResponse[ "data" ][ "msg" ] = "invalid entity type";
        }


        return $response->setData( array(
                "code" => 1,
                "isOk" => $tempResponse[ "isOk" ],
                'data' => $tempResponse[ "data" ]
            ));

    }

}
