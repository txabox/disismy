<?php

namespace DisismyWebBundle\Controller;

require_once __DIR__.'/../../../vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Jenssegers\Optimus\Optimus;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use DisismyWebBundle\Entity\Image;

// forms
use DisismyWebBundle\Form\ImageType;

class ImagesController extends Controller
{

    private function getFolderPathToImageRecursively( $globals, $curFolder, &$folderIdsArray )
    {
        if( $curFolder->getParent() )
        {
            // lo añadimos al inicio del array, dado que el path será a la inversa
            array_unshift( $folderIdsArray, $globals->optimus->encode( $curFolder->getParent()->getId() ) );
            $this->getFolderPathToImageRecursively( $globals, $curFolder->getParent(), $folderIdsArray );
        }
        else
        {
            // echo "no parent";
        }
    }
    private function getFolderPathToImage( $globals, $initialFolder )
    {
        $folderIdsArray = array( $globals->optimus->encode( $initialFolder->getId() ) );

        $this->getFolderPathToImageRecursively( $globals, $initialFolder, $folderIdsArray );

        return implode( "/", $folderIdsArray ) . "/";
    }

    /**
     * @Route( "/rawImage/{encodedImageId}/", name="getRawImage" )
     * @Route( "/{_locale}/rawImage/{encodedImageId}/", name="localized_getRawImage" )
     * @Method( "GET" )
     */
    public function getRawImageAction( $encodedImageId )
    {

        $globals = $this->get( 'globals' );
        $imageId = $globals->optimus->decode( $encodedImageId );

        $image = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Image' )
        ->findOneById( $imageId );

        if( ! $image )
        {
            throw $this->createNotFoundException(
                'No image found for id ' . $encodedImageId
            );
        }



        $owner = $image->getOwner();

        if( ! $owner )
        {
            throw $this->createNotFoundException(
                'No owner found for image  ' . $encodedImageId
            );
        }

        $ownerEncodedId = $owner->getEncodedId();

        $folder = $image->getFolder();


        $imageExt = $image->getExtension();

        $fileName = "$encodedImageId.$imageExt";
        $filePath = $globals->imagesPath . "$ownerEncodedId/" . $this->getFolderPathToImage( $globals, $folder ) . "$fileName";

        $file = file_get_contents( $filePath );
        $fileSize = filesize( $filePath );

        $headers = array(
            'Content-Type'     => "image/$imageExt",
            'Content-Disposition' => 'inline; filename="' . $fileName . '"',
            'Content-Length' => $fileSize,
            'Access-Control-Allow-Origin' => 'https://www.disismy.com',
            'Cache-Control' => 'no-cache, must-revalidate', // quitar cache // HTTP/1.1
            'Expires' => 'Sat, 26 Jul 1997 05:00:00 GMT' // quitar cache // Fecha en el pasado
            );

        return new Response( $file, 200, $headers );

    }


    /**
     * @Route( "/folder/{encodedFolderId}/images/", name="getFolderImages" )
     * @Route( "/{_locale}/folder/{encodedFolderId}/images/", name="localized_getFolderImages" )
     * @Method( { "POST" } )
     */
    public function getFolderImagesAction( Request $request, $encodedFolderId )
    {
        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'POST' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - el folder exista
        //  - el usuario sea el owner del folder
        $tempResponse = $globals->isUserLoggedAndOwnsThisFolder( $encodedFolderId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "encodedFolderId" => $encodedFolderId,
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser = $tempResponse[ "data" ][ "curUser" ];
        $folder   = $tempResponse[ "data" ][ "folder" ];

        $folderImagesArray = array();
        foreach( $folder->getImages() as $image )
        {
            array_push( $folderImagesArray, $image->toArray( $globals, $locale ) );
        }



        return $response->setData( array(
                "code" => 1,
                "isOk" => true,
                'data' => array(
                    "encodedFolderId" => $encodedFolderId,
                    "images" => $folderImagesArray,
                    "msg" => "success_retreiving_folder_images"
                )
            ));


    }


    /**
     * @Route( "/add/image/in/folder/{encodedFolderId}/", name="addImageInFolder" )
     * @Route( "/{_locale}/add/image/in/folder/{encodedFolderId}/", name="localized_addImageInFolder" )
     * @Method( { "POST" } )
     */
    public function addImageInFolderAction( Request $request, $encodedFolderId )
    {

        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'POST' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $logger = $this->get( 'logger' );
        $fs = new Filesystem();



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - el folder exista
        //  - el usuario sea el owner del folder
        $tempResponse = $globals->isUserLoggedAndOwnsThisFolder( $encodedFolderId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "encodedFolderId" => $encodedFolderId,
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser = $tempResponse[ "data" ][ "curUser" ];
        $folder   = $tempResponse[ "data" ][ "folder" ];



        $newImage = new Image();

        $form = $this->createForm(
            ImageType::class,
            $newImage,
            array(
                'method'    => 'POST',
                'curUser'   => $curUser,
        ));



        $form->handleRequest( $request );



        // 1º Validamos que el formulario esté correcto
        if( is_null( $newImage->getImageFile() ) )
        {
            return $response->setData( array(
                    "code" => 1,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "image_is_null"
                    )
                ));
        }



        $imageSize = $newImage->getImageFile()->getSize();
        $consumedBytes = $curUser->getConsumedBytes();
        // si excedemos los datos del plan contratado
        if( $imageSize + $consumedBytes > $curUser->getStoragePlan()->getBytes() )
        {
            return $response->setData( array(
                    "code" => 2,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "storage_plan_excedeed"
                    )
                ));
        }

        $imageExt = $newImage->getImageFile()->getClientOriginalExtension();
        $imagePathFrom = $this->getParameter( 'kernel.root_dir' ) . "/../src/DisismyWebBundle/Resources/public/images/Users/";
        $imagePathTo = $this->getParameter( 'kernel.root_dir' ) . "/../src/DisismyWebBundle/Resources/public/images/Users/" . $curUser->getEncodedId() . "/" . $encodedFolderId . "/";

        $newImage
            ->setOwner( $curUser )
            ->setExtension( $imageExt );



        $em->persist( $newImage );
        $em->flush();

        // aumentamos el consumo de bytes a nuestro usuario
        $curUser->setConsumedBytes( $curUser->getConsumedBytes() + $imageSize );


        // esto se sabe una vez se hace flush ( cosas del bundle VichFileType )
        $clientOriginalName = $newImage->getImageName();
        $encodedNewName = $globals->optimus->encode( $newImage->getId() ) . "." . $imageExt;



        $newImage->setImageName( $form->get( 'name' )->getData() );
        $newImage->setFolder( $folder );
        $em->persist( $newImage );
        $em->flush();

        try
        {
            $fs->rename(
                $imagePathFrom . "" . $clientOriginalName,
                $imagePathTo . $encodedNewName );
        }
        catch( IOExceptionInterface $e )
        {
            return $response->setData( array(
                    "code" => 3,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "An error occurred while renaming your file at " . $e->getPath()
                    )
                ));
        }

        return $response->setData( array(
                "code" => 4,
                "isOk" => true,
                'data' => array(
                    "msg" => "image_saved",
                    "image" => $newImage->toArray( $globals, $locale )
                )
            ));

    }






    /**
     * @Route( "/images/{encodedImageId}", name="removeImage" )
     * @Route( "/{_locale}/images/{encodedImageId}", name="localized_removeImage" )
     * @Method( { "DELETE" } )
     */
    public function removeImageAction( Request $request, $encodedImageId )
    {
        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $fs = new Filesystem();



        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'DELETE' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );



        // 1º Cotejamos que:
        //  - el usuario esté logueado
        //  - la imagen exista
        //  - el usuario sea el owner de la imagen
        $tempResponse = $globals->isUserLoggedAndOwnsThisImage( $encodedImageId );
        // si alguna de las 3 cosas a cotejar está mal:
        if( ! $tempResponse[ "isOk" ] )
        {
            return $response->setData( array(
                    "code" => 0,
                    "isOk" => false,
                    'data' => array(
                        "encodedImageId" => $encodedImageId,
                        "msg" => $tempResponse[ "data" ][ "msg" ]
                    )
                ));
        }
        $curUser = $tempResponse[ "data" ][ "curUser" ];
        $image   = $tempResponse[ "data" ][ "image" ];
        $encodedImageFileName = $globals->optimus->encode( $image->getId() ) . "." . $image->getExtension();
        $folder  = $image->getFolder();

        try
        {
            $imagePath = $this->getParameter( 'kernel.root_dir' ) . "/../src/DisismyWebBundle/Resources/public/images/Users/" . $curUser->getEncodedId() . "/" . $this->getFolderPathToImage( $globals, $folder ) . $encodedImageFileName;

            if( $fs->exists( $imagePath ) )
            {
                $fs->remove( $imagePath );
            }
        }
        catch( IOExceptionInterface $e )
        {
            return $response->setData( array(
                    "code" => 1,
                    "isOk" => false,
                    'data' => array(
                        "encodedImageId" => $encodedImageId,
                        "msg" => "error_deleting_image"
                    )
                ));
        }

        $em->remove( $image );
        $em->flush();

        return $response->setData( array(
                "code" => 2,
                "isOk" => true,
                'data' => array(
                    "encodedImageId" => $encodedImageId,
                    "msg" => "image_deleted"
                )
            ));


    }


}
