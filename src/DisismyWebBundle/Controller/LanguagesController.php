<?php

namespace DisismyWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LanguagesController extends Controller
{
    /**
     * @Route("/getLanguages/")
     */
     /**
      * @Route( "/languages/", name="getLanguages" )
      * @Route( "/{_locale}/languages/", name="localized_getLanguages" )
      * @Method( "GET" )
      */
    public function getLanguagesAction( Request $request )
    {

        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'GET' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );

        $em = $this->getDoctrine()->getManager();
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $logger = $this->get( 'logger' );

        $disismyLanguages = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Language' )
        ->findAll();

        $disismyLanguagesArray = array();
        foreach( $disismyLanguages as $language )
        {
            array_push( $disismyLanguagesArray, $language->toArray( $globals, $locale ) );
        }

        return $response->setData( array(
                "code"      => 1,
                "isOk"      => true,
                'data'      => array(
                "languages" => $disismyLanguagesArray,
                )
            ));
    }

}
