<?php

namespace DisismyWebBundle\Controller;

require_once __DIR__.'/../../../vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Jenssegers\Optimus\Optimus;

class DefaultController extends Controller
{

    /**
     * @Route( "/", name="index" )
     * @Route( "/{_locale}/", name="localized_index" )
     */
    public function indexAction( Request $request )
    {
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();

        $demoPanoId = 1;
        $encodedPanoId = $globals->optimus->encode( $demoPanoId  );

        $sampleGalleryOwnerId = 1;
        $encodedGalleryOwnerId = $globals->optimus->encode( $sampleGalleryOwnerId  );


        $pano = $this->getDoctrine()
        ->getRepository( 'DisismyWebBundle:Pano' )
        ->findOneById( $demoPanoId  );


        if( ! $pano )
        {
            throw $this->createNotFoundException(
                'No pano found for demoPanoId ' . $encodedPanoId
            );
        }

        $renderPanoExample = array(
            'objectType' => 'house',
            'encodedObjectId' => $globals->optimus->encode( 1 ),
            'encodedPanoId' => $globals->optimus->encode( 3 )
        );

        return $this->render( 'DisismyWebBundle:Default:index.html.twig', array(
            "encodedGalleryOwnerId" => $encodedGalleryOwnerId,
            "pano"                  => $pano->toArrayWithObject( $globals, $locale ),
            "isPanoInFullScreen"    => "false",
            "renderPanoExample"     => $renderPanoExample
        ));

    }

    /**
     * @Route( "/admin/", name="admin" )
     * @Route( "/{_locale}/admin/", name="localized_admin" )
     */
    public function adminAction()
    {
        return new Response( '<html><body>Admin page!</body></html>' );
    }

    /**
     * @Route( "/prueba/", name="prueba" )
     * @Route( "/{_locale}/prueba/", name="localized_prueba" )
     */
    public function pruebaAction()
    {
        return $this->render( 'DisismyWebBundle:Default:prueba.html.twig', array());
    }

    /**
     * @Route( "/isLogged/", name="isLogged" )
     * @Route( "/{_locale}/isLogged/", name="localized_isLogged" )
     * @Method( "GET" )
     */
    public function isLoggedAction( Request $request )
    {
        $globals = $this->get( 'globals' );
        $locale = $request->getLocale();
        $curUser = $this->getUser();

        $response = new JsonResponse();
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->headers->set( 'Access-Control-Allow-Origin', 'https://www.disismy.com' );
        $response->headers->set( 'Access-Control-Allow-Methods', 'GET' );
        $response->setEncodingOptions( JSON_UNESCAPED_UNICODE );

        if( $curUser )
        {
            return $response->setData( array(
                    "code" => 1,
                    "isOk" => true,
                    'data' => array(
                        "msg" => "logged"
                    )
                ));
        }
        else
        {
            return $response->setData( array(
                    "code" => 1,
                    "isOk" => false,
                    'data' => array(
                        "msg" => "no_logged"
                    )
                ));
        }

    }

    /**
     * @Route( "/phpinfo/", name="phpInfo" )
     * @Route( "/{_locale}/phpinfo/", name="localized_phpInfo" )
     */
    public function phpInfoAction()
    {
        phpinfo();
        return new Response('<html><body>phpinfo page!</body></html>');
    }

    /**
     * @Route( "/optimus/encode/{num}/", name="encode_optimus" )
     * @Route( "/{_locale}/encode/optimus/{num}/", name="localized_encode_optimus" )
     */
    public function optimusEncodeAction( $num )
    {

        $globals = $this->get( 'globals' );

        return new Response('<html><body>' . "Optimus: " . $globals->optimus->encode( $num ) . '</body></html>');
    }

    /**
     * @Route( "/optimus/decode/{num}/", name="decode_optimus" )
     * @Route( "/{_locale}/decode/optimus/{num}/", name="localized_decode_optimus" )
     */
    public function optimusDecodeAction( $num )
    {
        $globals = $this->get( 'globals' );

        return new Response('<html><body>' . "Optimus: " . $globals->optimus->decode( $num ) . '</body></html>');
    }


}
